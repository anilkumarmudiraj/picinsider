/**
 * Created by Shahrukh Khan on 5/16/2015.
 */

$("#contact_us").validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    },
    phone: {
    	required: true,
    	maxlength: 10,
    	minlength: 10
    }
  },
  submitHandler: function(form) {
    var name = document.getElementById('name').value;
	var email = document.getElementById('email').value;
	var phone = document.getElementById('phone').value;
	var comment = document.getElementById('comment').value;
	var connect = document.getElementById('connect').value;
	var to_id = document.getElementById('to_id').value;
	var to_name = document.getElementById('to_name').value;
	var token = document.getElementById('token').value;

	$.post('/sendMessage', {
		'name': name,
		'email': email,
		'phone': phone,
		'comment': comment,
		'connect': connect,
		'to_id': to_id,
		'to_name': to_name,
		'_token': token
	}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			alert('Message sent successfully ');
		}
		else {
			alert('Something went wrong');
		}
	});
  }
});

// $('#sendMessage').click(function() {
	
// });

$('#write-review').click(function() {

	var subject = document.getElementById('review_subject').value;
	var review_user_name = document.getElementById('review_user_name').value;
	var review_review = document.getElementById('review_review').value;
	var photographer_id = document.getElementById('review_photographer_id').value;
	var stars = $("input[name='score']").val();
	var token = document.getElementById('review_token').value;

	if (subject.length <= 3) {
		alert('Subject is too short');
	}
	else if (review_user_name < 3) {
		alert('Very short name, I doubt that is not real');
	}
	else if (review_review.length < 10) {
		alert('Minimum review length is 10 characters');
	}
	else {
		$.post('/post-review', {
			'subject': subject,
			'review__user_name': review_user_name,
			'review_review': review_review,
			'photographer_id': photographer_id,
			'stars': stars,
			'_token': token
		}, function(data) {

			var parsed = JSON.parse(data);

			if (parsed == 'success') {
				$('#review_response').html('Thank you for your feedback');
				$('#exampleModal').modal('toggle');
				$(".modal-backdrop").remove();
				alert('Thank you for your feedback');
			}
			else {
				alert('Something went wrong');
			}

		});
	}

});

$('.likeButton').click(function() {
	var like = $(this).data('review_id');
	var liker = document.getElementById('liker_id').value;
	var token = document.getElementById('token').value;

	$.post('/like-review', {'like': like, 'liker_id': liker, '_token': token}, function(data) {

		var parsed = JSON.parse(data);

		if (parsed == 'success') {
			// $('.rating').html('Thank you for rating this review');
			alert('Thank you for rating this review');
			$("#total_likes").text(parseInt($("#total_likes").text())+1);
		}
		else {
			alert('Something went wrong');
		}
	});
});

$('.dislikeButton').click(function() {
	var dislike = $(this).data('review_id');
	var token = document.getElementById('token').value;

	$.post('/dislike-review', {'dislike': dislike, '_token': token}, function(data) {

		var parsed = JSON.parse(data);

		if (parsed == 'success') {
			$('.rating').html('Thank you for rating this review');
		}
		else {
			alert('Something went wrong');
		}
	});
});

$(".deactivate").click(function() {
	var item_id = $(this).data('deactivate_id');
	console.log('Deactivate slide' + item_id)
	var token = document.getElementById('csrftoken').value;
	$.post('/administrator/deactivateSlider', {'slider_id': item_id, '_token': token}, function(data) {

		var parsed = JSON.parse(data);

		if (parsed == 'success') {
			location.reload(true);
		}
		else {
			alert('Something went wrong');
		}
	});
});


$(".activate").click(function() {
	var item_id = $(this).data('activate_id');
	var token = document.getElementById('csrftoken').value;
	$.post('/administrator/activateSlider', {'slider_id': item_id, '_token': token}, function(data) {

		var parsed = JSON.parse(data);

		if (parsed == 'success') {
			location.reload(true);
		}
		else {
			alert('Something went wrong');
		}
	});
});


$(".delete").click(function() {

	var item_id = $(this).data('delete_id');
	var token = document.getElementById('csrftoken').value;

	$.post('/administrator/deleteSlider', {'slider_id': item_id, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		console.log(parsed);
	});
});


$("#updatePassword").click(function() {

	var current_password = document.getElementById('current_password').value;
	var new_password = document.getElementById('new_password').value;
	var confirm_password = document.getElementById('confirm_password').value;
	var token = document.getElementById('csrftoken').value;

	if(new_password = confirm_password)
		$('#message').html("New and Confirm password doesn't match");

	$.post('/modalupdatePassword', {
		'current_password': current_password,
		'new_password': new_password,
		'_token': token
	}, function(data) {
		var parsed = JSON.parse(data);
		console.log(parsed);

		if(parsed == 0){
			$('#message').html("The password you have supplied does not match with the one in our records");
		}
		else{
			$('#message').html("Congrats! your password updated successfully. Logging you out now");
			setTimeout(function() {
				window.location = '/logout';
			}, 3000);
		}

		// $('#message').append(parsed);
	});
});

$('.reportReviewButton').click(function() {
								   var review_id = $(this).data('review_flag_id');
								   var token = document.getElementById('token').value;

								   $.post('/flagReview', {'review_id': review_id, '_token': token}, function(data) {


									   var parsed = JSON.parse(data);
									   console.log(parsed);

									   $('#message').append(parsed);

								   });
							   }
);


$('#deleteUser').click(function() {
	console.log('button clicked');
	var deleting = document.getElementById('modaldeleteuserid').value;
	var token = document.getElementById('token').value;
	$.post('/administrator/deleteUser', {'deleting': deleting, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			alert('User deleted successfully');
			location.reload(true);
		}
	});
});

$('#deleteFlag').click(function() {

	var deleting = document.getElementById('modaldeleteflagid').value;
	var token = document.getElementById('token').value;

	$.post('/administrator/deleteFlag', {'deleting': deleting, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			alert('Flag deleted successfully');
			location.reload(true);
		}
	});
});

$('#deleteReview').click(function() {
	var deleting = document.getElementById('modaldeletereviewid').value;
	var token = document.getElementById('token').value;

	$.post('/administrator/deleteReview', {'deleting': deleting, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			alert('Review deleted successfully');
			location.reload(true);
		}
	});
});

$(":checkbox[name='category[]']").change(function() {
	if ($(":checkbox[name='category[]']:checked").length == 2) {
		$(':checkbox:not(:checked)').prop('disabled', true);
	} else {
		$(':checkbox:not(:checked)').prop('disabled', false);
	}
});

$(".gig-invite").click(function() {
	var gig_id = document.getElementById('gig_id').value;
	var token = document.getElementById('token').value;
	var photographer_id = document.getElementById('photographer_id').value;

	$.post('/inviteToGig', {'gig_id': gig_id, 'photographer_id': photographer_id, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			$('.inviteButton span').html("Invitation sent");
			$('#inviteModal').modal('toggle');
			$('.modal-backdrop').fadeOut('slow');
		}

	});
});


$("#modalLogin").click(function() {
	var email = document.getElementById('modalEmail').value;
	var token = document.getElementById('token').value;
	var password = document.getElementById('modalPassword').value;

	$.post('/modalLogin', {'email': email, 'password': password, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			location.reload(true);
		}
		else {
			$('#modalMessage').html('Invalid credentials. If you do not already have an account please create one');
		}

	});
});


$('.phone').focus(function() {
	console.log("touch phone");
	var string = $(this).html();
	$(this).html(string.substring(0, 3) + '-' + string.substring(3, 6) + '-' + string.substring(6, 10))
});


$('.featured').click(function() {
	var changeto = document.getElementById('status').value;
	var photographer_id = $(this).val();
	var token = document.getElementById('token').value;

	console.log('pid ' + photographer_id + ' change ' + changeto);
	$.post('/administrator/changeStatus', {
		'photographer_id': photographer_id,
		'changeto': changeto,
		'_token': token
	}, function(data) {
		var parsed = JSON.parse(data);
		console.log(parsed);
	});
});

$("#showeditbox").click(function() {
	var review = $('#review').text();
	var reviewSubject = $('#reviewSubject').text();
	var review_id = document.getElementById("review_id").value;
	var divid = document.getElementById('review');
	var reviewDiv = document.getElementById('reviewSubject');
	var editButton = document.getElementById('editButton');
	editButton.style.display = 'none';
	divid.style.display = 'none';
	reviewDiv.style.display = 'none';

	var html = '<div class="form-group"><input type="text" style="color:#000;" class="form-control" id="reviewSubjectBox" value="' + reviewSubject + '"> <br /><textarea class="form-control" rows="4" id="editedReview" name="editedReview">' + review + '</textarea><input type="hidden" value="' + review_id + '" id="review_id"><br /><button class="btn btn-success" id="saveChanges">Save changes</button></div>';
	$("#editbox").append(html);
});


$(document).on('click', '#saveChanges', function() {
	var review_id = $('#review_id').val();
	var reviewSubject = document.getElementById('reviewSubjectBox').value;
	var body = $('#editedReview').val();
	var token = $('#token').val();

	console.log(reviewSubject);
	$.post('/saveEditedReview', {'review_id': review_id, 'body': body, 'review_subject': reviewSubject, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed.status == 'success') {
			var editbox = document.getElementById('editbox');
			editbox.style.display = 'none';
			var review = parsed.review;
			var subject = parsed.subject;
			var reviewbox = document.getElementById('review');
			var editButton = document.getElementById('editButton');
			var subjectBox = document.getElementById('reviewSubject');
			subjectBox.style.display = 'block';
			editButton.style.display = 'block';
			reviewbox.style.display = 'block';
			$('#review').replaceWith(review);
			$('#subjectBox').replaceWith(subject);
		}
		else {
			console.log('fail');
		}

	});
});

$('.typeahead').typeahead({
							  hint: true,
							  highlight: true,
							  minLength: 3,
							  limit: 8
						  }, {
							  source: function(q, cb) {
								  return $.ajax({
													dataType: 'json',
													type: 'get',
													url: 'http://gd.geobytes.com/AutoCompleteCity?callback=?&q=' + q,
													chache: false,
													success: function(data) {
														var result = [];
														$.each(data, function(index, val) {
															result.push({
																			value: val
																		});
														});
														cb(result);
													}
												});
							  }
						  });


$(document).on('click', '#addMore', function() {
	console.log('button clicked');
	var html = "<div class='btn btn-o btn-default btn-file bc10'><i class='glyphicon glyphicon-folder-open'></i>&nbsp;Browse Images<input type='file' id='1431954496675' data-browse-label='Browse Images' data-browse-class='btn btn-o btn-default' accept='image/jpeg,image/png' data-show-remove='false' data-show-caption='false' data-show-upload='false' multiple='true' name='image' class='file'></div>";
	$("#browsePlace").append(html);
});
