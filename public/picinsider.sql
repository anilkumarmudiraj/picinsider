-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: picinsider
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slide` text COLLATE utf8_unicode_ci NOT NULL,
  `onslider` text COLLATE utf8_unicode_ci NOT NULL,
  `heading1` text COLLATE utf8_unicode_ci NOT NULL,
  `para1` text COLLATE utf8_unicode_ci NOT NULL,
  `person1_photo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person1_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person1_position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person2_photo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person2_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person2_position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person3_photo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person3_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person3_position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person4_photo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person4_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person4_position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person5_photo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person5_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person5_position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person6_photo` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person6_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `person6_position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `heading2` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `para2` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abouts`
--

LOCK TABLES `abouts` WRITE;
/*!40000 ALTER TABLE `abouts` DISABLE KEYS */;
INSERT INTO `abouts` VALUES (1,'/images/about-banner.jpg','Photography is the way of feeling, of touching, of loving. What you have caught on film is captured forever and ever ever','Having been through some of life’s biggest milestone, we understand the need to capture the perfect moment. Something','<p><strong>Today</strong>&nbsp;there are more than 30000 photographers around the world. Each day there are more than 30000 pictures being shot. As human, we long to remember and document the small and not so small moment in life. Picinsider was conceived with the idea of bringing together photographers passionate about their art with consumers interested in their art.</p>\r\n','/images/about1.jpg','Richard','Co-founder','/images/about2.jpg','Esther','Co-Founder','/images/about3.jpg','Robert','CFO','','','','','','','','','','<p>About our <strong>People</strong></p>\r\n','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: rgb(85, 85, 85); font-family: Conv_Montserrat-Light; font-size: 14px; line-height: 20px;\">Picinsider growing team is build to scale along with thousand of new customers every day. As a privately owned, founder led company, we&rsquo;re constantly looking to build and innovate. We invest in engineering, research, customer support and great design. If you&rsquo;re interested in joining the Picinsiderteam, feel free to reach out to us at hello@picinsider.com</p>\r\n\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: rgb(85, 85, 85); font-family: Conv_Montserrat-Light; font-size: 14px; line-height: 20px;\">Finally don&rsquo;t forget to follow us on Facebook, Instagram, and also read our blog for additional insights.</p>\r\n','picinsider','picinsider','picinsider','picinsider@picinsider.com','2015-05-27 02:30:00','2015-05-30 00:47:55');
/*!40000 ALTER TABLE `abouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blogs_user_id_foreign` (`user_id`),
  CONSTRAINT `blogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (1,'Testing blog','<p>Before you go&nbsp;</p>\r\n','/images/blogs/MThXePpsOYXP.jpg','gaurav',26,'2015-06-10 17:37:29','2015-06-10 17:37:29'),(2,'testing picinsider','<p>new content</p>\r\n','/images/blogs/PHOxckxidnGx.jpg','gaurav',26,'2015-06-11 17:06:09','2015-06-16 14:18:27'),(3,'test','<p>new image content</p>\r\n','/images/blogs/AJusfBze1f7S.jpg','gaurav',26,'2015-06-16 12:02:51','2015-06-16 14:16:54');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Engagement','2015-05-10 22:30:00','0000-00-00 00:00:00'),(2,'Events','2015-05-10 22:30:00','0000-00-00 00:00:00'),(3,'Wedding','2015-05-10 22:30:00','0000-00-00 00:00:00'),(4,'New Borns','2015-05-10 22:30:00','0000-00-00 00:00:00'),(5,'Lifestyle','2015-05-10 22:30:00','0000-00-00 00:00:00'),(6,'Real Estate','2015-05-10 22:30:00','0000-00-00 00:00:00'),(7,'Commercial','2015-05-10 22:30:00','0000-00-00 00:00:00'),(8,'Others','2015-05-10 22:30:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Toronto','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Montreal','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'New York','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Buffalo','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Boston','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Columbus','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Dallas','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Vancouver','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Seattle','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Los Angeles','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'US','United States','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'CA','Canada','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'AF','Afghanistan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'AL','Albania','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'DZ','Algeria','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'DS','American Samoa','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'AD','Andorra','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'AO','Angola','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'AI','Anguilla','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'AQ','Antarctica','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'AG','Antigua and/or Barbuda','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'AR','Argentina','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'AM','Armenia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'AW','Aruba','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'AU','Australia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'AT','Austria','0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,'AZ','Azerbaijan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'BS','Bahamas','0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,'BH','Bahrain','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'BD','Bangladesh','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'BB','Barbados','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'BY','Belarus','0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'BE','Belgium','0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'BZ','Belize','0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,'BJ','Benin','0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'BM','Bermuda','0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'BT','Bhutan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,'BO','Bolivia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'BA','Bosnia and Herzegovina','0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'BW','Botswana','0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,'BV','Bouvet Island','0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'BR','Brazil','0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,'IO','British lndian Ocean Territory','0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,'BN','Brunei Darussalam','0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,'BG','Bulgaria','0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'BF','Burkina Faso','0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,'BI','Burundi','0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,'KH','Cambodia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'CM','Cameroon','0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,'CV','Cape Verde','0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,'KY','Cayman Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(42,'CF','Central African Republic','0000-00-00 00:00:00','0000-00-00 00:00:00'),(43,'TD','Chad','0000-00-00 00:00:00','0000-00-00 00:00:00'),(44,'CL','Chile','0000-00-00 00:00:00','0000-00-00 00:00:00'),(45,'CN','China','0000-00-00 00:00:00','0000-00-00 00:00:00'),(46,'CX','Christmas Island','0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,'CC','Cocos (Keeling) Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(48,'CO','Colombia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(49,'KM','Comoros','0000-00-00 00:00:00','0000-00-00 00:00:00'),(50,'CG','Congo','0000-00-00 00:00:00','0000-00-00 00:00:00'),(51,'CK','Cook Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(52,'CR','Costa Rica','0000-00-00 00:00:00','0000-00-00 00:00:00'),(53,'HR','Croatia (Hrvatska)','0000-00-00 00:00:00','0000-00-00 00:00:00'),(54,'CU','Cuba','0000-00-00 00:00:00','0000-00-00 00:00:00'),(55,'CY','Cyprus','0000-00-00 00:00:00','0000-00-00 00:00:00'),(56,'CZ','Czech Republic','0000-00-00 00:00:00','0000-00-00 00:00:00'),(57,'DK','Denmark','0000-00-00 00:00:00','0000-00-00 00:00:00'),(58,'DJ','Djibouti','0000-00-00 00:00:00','0000-00-00 00:00:00'),(59,'DM','Dominica','0000-00-00 00:00:00','0000-00-00 00:00:00'),(60,'DO','Dominican Republic','0000-00-00 00:00:00','0000-00-00 00:00:00'),(61,'TP','East Timor','0000-00-00 00:00:00','0000-00-00 00:00:00'),(62,'EC','Ecuador','0000-00-00 00:00:00','0000-00-00 00:00:00'),(63,'EG','Egypt','0000-00-00 00:00:00','0000-00-00 00:00:00'),(64,'SV','El Salvador','0000-00-00 00:00:00','0000-00-00 00:00:00'),(65,'GQ','Equatorial Guinea','0000-00-00 00:00:00','0000-00-00 00:00:00'),(66,'ER','Eritrea','0000-00-00 00:00:00','0000-00-00 00:00:00'),(67,'EE','Estonia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(68,'ET','Ethiopia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(69,'FK','Falkland Islands (Malvinas)','0000-00-00 00:00:00','0000-00-00 00:00:00'),(70,'FO','Faroe Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(71,'FJ','Fiji','0000-00-00 00:00:00','0000-00-00 00:00:00'),(72,'FI','Finland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(73,'FR','France','0000-00-00 00:00:00','0000-00-00 00:00:00'),(74,'FX','France, Metropolitan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(75,'GF','French Guiana','0000-00-00 00:00:00','0000-00-00 00:00:00'),(76,'PF','French Polynesia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(77,'TF','French Southern Territories','0000-00-00 00:00:00','0000-00-00 00:00:00'),(78,'GA','Gabon','0000-00-00 00:00:00','0000-00-00 00:00:00'),(79,'GM','Gambia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(80,'GE','Georgia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(81,'DE','Germany','0000-00-00 00:00:00','0000-00-00 00:00:00'),(82,'GH','Ghana','0000-00-00 00:00:00','0000-00-00 00:00:00'),(83,'GI','Gibraltar','0000-00-00 00:00:00','0000-00-00 00:00:00'),(84,'GR','Greece','0000-00-00 00:00:00','0000-00-00 00:00:00'),(85,'GL','Greenland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(86,'GD','Grenada','0000-00-00 00:00:00','0000-00-00 00:00:00'),(87,'GP','Guadeloupe','0000-00-00 00:00:00','0000-00-00 00:00:00'),(88,'GU','Guam','0000-00-00 00:00:00','0000-00-00 00:00:00'),(89,'GT','Guatemala','0000-00-00 00:00:00','0000-00-00 00:00:00'),(90,'GN','Guinea','0000-00-00 00:00:00','0000-00-00 00:00:00'),(91,'GW','Guinea-Bissau','0000-00-00 00:00:00','0000-00-00 00:00:00'),(92,'GY','Guyana','0000-00-00 00:00:00','0000-00-00 00:00:00'),(93,'HT','Haiti','0000-00-00 00:00:00','0000-00-00 00:00:00'),(94,'HM','Heard and Mc Donald Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(95,'HN','Honduras','0000-00-00 00:00:00','0000-00-00 00:00:00'),(96,'HK','Hong Kong','0000-00-00 00:00:00','0000-00-00 00:00:00'),(97,'HU','Hungary','0000-00-00 00:00:00','0000-00-00 00:00:00'),(98,'IS','Iceland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(99,'IN','India','0000-00-00 00:00:00','0000-00-00 00:00:00'),(100,'ID','Indonesia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(101,'IR','Iran (Islamic Republic of)','0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,'IQ','Iraq','0000-00-00 00:00:00','0000-00-00 00:00:00'),(103,'IE','Ireland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(104,'IL','Israel','0000-00-00 00:00:00','0000-00-00 00:00:00'),(105,'IT','Italy','0000-00-00 00:00:00','0000-00-00 00:00:00'),(106,'CI','Ivory Coast','0000-00-00 00:00:00','0000-00-00 00:00:00'),(107,'JM','Jamaica','0000-00-00 00:00:00','0000-00-00 00:00:00'),(108,'JP','Japan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(109,'JO','Jordan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(110,'KZ','Kazakhstan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(111,'KE','Kenya','0000-00-00 00:00:00','0000-00-00 00:00:00'),(112,'KI','Kiribati','0000-00-00 00:00:00','0000-00-00 00:00:00'),(113,'KP','Korea, Democratic People\'s Republic of','0000-00-00 00:00:00','0000-00-00 00:00:00'),(114,'KR','Korea, Republic of','0000-00-00 00:00:00','0000-00-00 00:00:00'),(115,'XK','Kosovo','0000-00-00 00:00:00','0000-00-00 00:00:00'),(116,'KW','Kuwait','0000-00-00 00:00:00','0000-00-00 00:00:00'),(117,'KG','Kyrgyzstan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(118,'LA','Lao People\'s Democratic Republic','0000-00-00 00:00:00','0000-00-00 00:00:00'),(119,'LV','Latvia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(120,'LB','Lebanon','0000-00-00 00:00:00','0000-00-00 00:00:00'),(121,'LS','Lesotho','0000-00-00 00:00:00','0000-00-00 00:00:00'),(122,'LR','Liberia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(123,'LY','Libyan Arab Jamahiriya','0000-00-00 00:00:00','0000-00-00 00:00:00'),(124,'LI','Liechtenstein','0000-00-00 00:00:00','0000-00-00 00:00:00'),(125,'LT','Lithuania','0000-00-00 00:00:00','0000-00-00 00:00:00'),(126,'LU','Luxembourg','0000-00-00 00:00:00','0000-00-00 00:00:00'),(127,'MO','Macau','0000-00-00 00:00:00','0000-00-00 00:00:00'),(128,'MK','Macedonia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(129,'MG','Madagascar','0000-00-00 00:00:00','0000-00-00 00:00:00'),(130,'MW','Malawi','0000-00-00 00:00:00','0000-00-00 00:00:00'),(131,'MY','Malaysia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(132,'MV','Maldives','0000-00-00 00:00:00','0000-00-00 00:00:00'),(133,'ML','Mali','0000-00-00 00:00:00','0000-00-00 00:00:00'),(134,'MT','Malta','0000-00-00 00:00:00','0000-00-00 00:00:00'),(135,'MH','Marshall Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(136,'MQ','Martinique','0000-00-00 00:00:00','0000-00-00 00:00:00'),(137,'MR','Mauritania','0000-00-00 00:00:00','0000-00-00 00:00:00'),(138,'MU','Mauritius','0000-00-00 00:00:00','0000-00-00 00:00:00'),(139,'TY','Mayotte','0000-00-00 00:00:00','0000-00-00 00:00:00'),(140,'MX','Mexico','0000-00-00 00:00:00','0000-00-00 00:00:00'),(141,'FM','Micronesia, Federated States of','0000-00-00 00:00:00','0000-00-00 00:00:00'),(142,'MD','Moldova, Republic of','0000-00-00 00:00:00','0000-00-00 00:00:00'),(143,'MC','Monaco','0000-00-00 00:00:00','0000-00-00 00:00:00'),(144,'MN','Mongolia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(145,'ME','Montenegro','0000-00-00 00:00:00','0000-00-00 00:00:00'),(146,'MS','Montserrat','0000-00-00 00:00:00','0000-00-00 00:00:00'),(147,'MA','Morocco','0000-00-00 00:00:00','0000-00-00 00:00:00'),(148,'MZ','Mozambique','0000-00-00 00:00:00','0000-00-00 00:00:00'),(149,'MM','Myanmar','0000-00-00 00:00:00','0000-00-00 00:00:00'),(150,'NA','Namibia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(151,'NR','Nauru','0000-00-00 00:00:00','0000-00-00 00:00:00'),(152,'NP','Nepal','0000-00-00 00:00:00','0000-00-00 00:00:00'),(153,'NL','Netherlands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(154,'AN','Netherlands Antilles','0000-00-00 00:00:00','0000-00-00 00:00:00'),(155,'NC','New Caledonia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(156,'NZ','New Zealand','0000-00-00 00:00:00','0000-00-00 00:00:00'),(157,'NI','Nicaragua','0000-00-00 00:00:00','0000-00-00 00:00:00'),(158,'NE','Niger','0000-00-00 00:00:00','0000-00-00 00:00:00'),(159,'NG','Nigeria','0000-00-00 00:00:00','0000-00-00 00:00:00'),(160,'NU','Niue','0000-00-00 00:00:00','0000-00-00 00:00:00'),(161,'NF','Norfork Island','0000-00-00 00:00:00','0000-00-00 00:00:00'),(162,'MP','Northern Mariana Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(163,'NO','Norway','0000-00-00 00:00:00','0000-00-00 00:00:00'),(164,'OM','Oman','0000-00-00 00:00:00','0000-00-00 00:00:00'),(165,'PK','Pakistan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(166,'PW','Palau','0000-00-00 00:00:00','0000-00-00 00:00:00'),(167,'PA','Panama','0000-00-00 00:00:00','0000-00-00 00:00:00'),(168,'PG','Papua New Guinea','0000-00-00 00:00:00','0000-00-00 00:00:00'),(169,'PY','Paraguay','0000-00-00 00:00:00','0000-00-00 00:00:00'),(170,'PE','Peru','0000-00-00 00:00:00','0000-00-00 00:00:00'),(171,'PH','Philippines','0000-00-00 00:00:00','0000-00-00 00:00:00'),(172,'PN','Pitcairn','0000-00-00 00:00:00','0000-00-00 00:00:00'),(173,'PL','Poland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(174,'PT','Portugal','0000-00-00 00:00:00','0000-00-00 00:00:00'),(175,'PR','Puerto Rico','0000-00-00 00:00:00','0000-00-00 00:00:00'),(176,'QA','Qatar','0000-00-00 00:00:00','0000-00-00 00:00:00'),(177,'RE','Reunion','0000-00-00 00:00:00','0000-00-00 00:00:00'),(178,'RO','Romania','0000-00-00 00:00:00','0000-00-00 00:00:00'),(179,'RU','Russian Federation','0000-00-00 00:00:00','0000-00-00 00:00:00'),(180,'RW','Rwanda','0000-00-00 00:00:00','0000-00-00 00:00:00'),(181,'KN','Saint Kitts and Nevis','0000-00-00 00:00:00','0000-00-00 00:00:00'),(182,'LC','Saint Lucia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(183,'VC','Saint Vincent and the Grenadines','0000-00-00 00:00:00','0000-00-00 00:00:00'),(184,'WS','Samoa','0000-00-00 00:00:00','0000-00-00 00:00:00'),(185,'SM','San Marino','0000-00-00 00:00:00','0000-00-00 00:00:00'),(186,'ST','Sao Tome and Principe','0000-00-00 00:00:00','0000-00-00 00:00:00'),(187,'SA','Saudi Arabia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(188,'SN','Senegal','0000-00-00 00:00:00','0000-00-00 00:00:00'),(189,'RS','Serbia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(190,'SC','Seychelles','0000-00-00 00:00:00','0000-00-00 00:00:00'),(191,'SL','Sierra Leone','0000-00-00 00:00:00','0000-00-00 00:00:00'),(192,'SG','Singapore','0000-00-00 00:00:00','0000-00-00 00:00:00'),(193,'SK','Slovakia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(194,'SI','Slovenia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(195,'SB','Solomon Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(196,'SO','Somalia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(197,'ZA','South Africa','0000-00-00 00:00:00','0000-00-00 00:00:00'),(198,'GS','South Georgia South Sandwich Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(199,'ES','Spain','0000-00-00 00:00:00','0000-00-00 00:00:00'),(200,'LK','Sri Lanka','0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,'SH','St. Helena','0000-00-00 00:00:00','0000-00-00 00:00:00'),(202,'PM','St. Pierre and Miquelon','0000-00-00 00:00:00','0000-00-00 00:00:00'),(203,'SD','Sudan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(204,'SR','Suriname','0000-00-00 00:00:00','0000-00-00 00:00:00'),(205,'SJ','Svalbarn and Jan Mayen Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(206,'SZ','Swaziland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(207,'SE','Sweden','0000-00-00 00:00:00','0000-00-00 00:00:00'),(208,'CH','Switzerland','0000-00-00 00:00:00','0000-00-00 00:00:00'),(209,'SY','Syrian Arab Republic','0000-00-00 00:00:00','0000-00-00 00:00:00'),(210,'TW','Taiwan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(211,'TJ','Tajikistan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(212,'TZ','Tanzania, United Republic of','0000-00-00 00:00:00','0000-00-00 00:00:00'),(213,'TH','Thailand','0000-00-00 00:00:00','0000-00-00 00:00:00'),(214,'TG','Togo','0000-00-00 00:00:00','0000-00-00 00:00:00'),(215,'TK','Tokelau','0000-00-00 00:00:00','0000-00-00 00:00:00'),(216,'TO','Tonga','0000-00-00 00:00:00','0000-00-00 00:00:00'),(217,'TT','Trinidad and Tobago','0000-00-00 00:00:00','0000-00-00 00:00:00'),(218,'TN','Tunisia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(219,'TR','Turkey','0000-00-00 00:00:00','0000-00-00 00:00:00'),(220,'TM','Turkmenistan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(221,'TC','Turks and Caicos Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(222,'TV','Tuvalu','0000-00-00 00:00:00','0000-00-00 00:00:00'),(223,'UG','Uganda','0000-00-00 00:00:00','0000-00-00 00:00:00'),(224,'UA','Ukraine','0000-00-00 00:00:00','0000-00-00 00:00:00'),(225,'AE','United Arab Emirates','0000-00-00 00:00:00','0000-00-00 00:00:00'),(226,'GB','United Kingdom','0000-00-00 00:00:00','0000-00-00 00:00:00'),(227,'UM','United States minor outlying islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(228,'UY','Uruguay','0000-00-00 00:00:00','0000-00-00 00:00:00'),(229,'UZ','Uzbekistan','0000-00-00 00:00:00','0000-00-00 00:00:00'),(230,'VU','Vanuatu','0000-00-00 00:00:00','0000-00-00 00:00:00'),(231,'VA','Vatican City State','0000-00-00 00:00:00','0000-00-00 00:00:00'),(232,'VE','Venezuela','0000-00-00 00:00:00','0000-00-00 00:00:00'),(233,'VN','Vietnam','0000-00-00 00:00:00','0000-00-00 00:00:00'),(234,'VG','Virgin Islands (British)','0000-00-00 00:00:00','0000-00-00 00:00:00'),(235,'VI','Virgin Islands (U.S.)','0000-00-00 00:00:00','0000-00-00 00:00:00'),(236,'WF','Wallis and Futuna Islands','0000-00-00 00:00:00','0000-00-00 00:00:00'),(237,'EH','Western Sahara','0000-00-00 00:00:00','0000-00-00 00:00:00'),(238,'YE','Yemen','0000-00-00 00:00:00','0000-00-00 00:00:00'),(239,'YU','Yugoslavia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(240,'ZR','Zaire','0000-00-00 00:00:00','0000-00-00 00:00:00'),(241,'ZM','Zambia','0000-00-00 00:00:00','0000-00-00 00:00:00'),(242,'ZW','Zimbabwe','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flags`
--

DROP TABLE IF EXISTS `flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `flags_review_id_foreign` (`review_id`),
  CONSTRAINT `flags_review_id_foreign` FOREIGN KEY (`review_id`) REFERENCES `reviews` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flags`
--

LOCK TABLES `flags` WRITE;
/*!40000 ALTER TABLE `flags` DISABLE KEYS */;
/*!40000 ALTER TABLE `flags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generalfunctions`
--

DROP TABLE IF EXISTS `generalfunctions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generalfunctions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generalfunctions`
--

LOCK TABLES `generalfunctions` WRITE;
/*!40000 ALTER TABLE `generalfunctions` DISABLE KEYS */;
/*!40000 ALTER TABLE `generalfunctions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gig_categories`
--

DROP TABLE IF EXISTS `gig_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gig_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `gig_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `gig_categories_gig_id_foreign` (`gig_id`),
  CONSTRAINT `gig_categories_gig_id_foreign` FOREIGN KEY (`gig_id`) REFERENCES `gigs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gig_categories`
--

LOCK TABLES `gig_categories` WRITE;
/*!40000 ALTER TABLE `gig_categories` DISABLE KEYS */;
INSERT INTO `gig_categories` VALUES (1,'Events',1,'2015-05-27 18:26:00','2015-05-27 18:26:00'),(2,'Engagement',2,'2015-06-01 11:28:35','2015-06-01 11:28:35'),(7,'Engagement',6,'2015-06-09 16:10:05','2015-06-09 16:10:05'),(8,'Events',6,'2015-06-09 16:10:05','2015-06-09 16:10:05'),(9,'Engagement',7,'2015-06-09 16:10:15','2015-06-09 16:10:15'),(10,'Events',7,'2015-06-09 16:10:15','2015-06-09 16:10:15'),(11,'Engagement',8,'2015-06-09 16:11:08','2015-06-09 16:11:08'),(12,'Events',8,'2015-06-09 16:11:08','2015-06-09 16:11:08'),(19,'Engagement',10,'2015-06-10 17:16:09','2015-06-10 17:16:09'),(20,'Events',10,'2015-06-10 17:16:09','2015-06-10 17:16:09'),(21,'Engagement',11,'2015-06-10 17:16:29','2015-06-10 17:16:29'),(22,'Events',11,'2015-06-10 17:16:29','2015-06-10 17:16:29'),(25,'Engagement',9,'2015-06-10 17:32:00','2015-06-10 17:32:00'),(26,'Events',9,'2015-06-10 17:32:00','2015-06-10 17:32:00'),(27,'Events',12,'2015-06-11 05:45:03','2015-06-11 05:45:03'),(28,'Real Estate',12,'2015-06-11 05:45:03','2015-06-11 05:45:03'),(32,'Engagement',16,'2015-06-11 16:41:05','2015-06-11 16:41:05'),(33,'Events',16,'2015-06-11 16:41:05','2015-06-11 16:41:05'),(34,'Engagement',17,'2015-06-11 16:47:45','2015-06-11 16:47:45'),(35,'Events',17,'2015-06-11 16:47:45','2015-06-11 16:47:45'),(38,'Engagement',5,'2015-06-11 17:26:55','2015-06-11 17:26:55'),(39,'Events',5,'2015-06-11 17:26:55','2015-06-11 17:26:55'),(42,'Engagement',19,'2015-06-15 14:50:48','2015-06-15 14:50:48'),(43,'Wedding',18,'2015-06-15 16:16:29','2015-06-15 16:16:29'),(44,'Lifestyle',18,'2015-06-15 16:16:29','2015-06-15 16:16:29'),(45,'Engagement',20,'2015-06-16 11:29:12','2015-06-16 11:29:12'),(46,'Wedding',21,'2015-06-21 06:34:45','2015-06-21 06:34:45'),(47,'Real Estate',21,'2015-06-21 06:34:45','2015-06-21 06:34:45');
/*!40000 ALTER TABLE `gig_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gigs`
--

DROP TABLE IF EXISTS `gigs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gigs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `short` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `requirements` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `budget` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `gigs_user_id_foreign` (`user_id`),
  CONSTRAINT `gigs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gigs`
--

LOCK TABLES `gigs` WRITE;
/*!40000 ALTER TABLE `gigs` DISABLE KEYS */;
INSERT INTO `gigs` VALUES (1,'DelhiDelhiDelhiDelhi','Delhi','2015-05-27','Afternoon','DelhiDelhiDelhiDelhiDelhiDelhiDelhiDelhiDelhi','100-200',60,'2015-05-27 18:26:00','2015-05-27 18:26:00'),(2,'testedtested','Dallas','2015-06-17','Morning','testedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtestedtested','100-200',95,'2015-06-01 11:28:35','2015-06-01 11:28:35'),(4,'short gig','Toronto, ON, Canada','2015-10-06','Morning','This is the long post description','200-500',104,'2015-06-08 05:20:00','2015-06-21 06:22:07'),(5,'testing project 12','4536553','2015-01-03','Morning','this is a testing project.','100-200',122,'2015-06-09 16:08:10','2015-06-09 16:20:11'),(6,'testing project','Delhi, CA, United States','2015-01-03','Morning','this is a testing project.','100-200',122,'2015-06-09 16:10:05','2015-06-09 16:10:05'),(7,'testing project','Delhi, CA, United States','2015-09-06','Morning','this is a testing project.','100-200',122,'2015-06-09 16:10:15','2015-06-09 16:10:15'),(8,'testing project','Del Norte, CO, United States','2015-09-06','Evening','this is a testing project.','100-200',122,'2015-06-09 16:11:08','2015-06-09 16:11:08'),(9,'testing demo  12.....','delhi','2015-03-06','Afternoon','today testing........1321321','200-500',126,'2015-06-10 17:14:38','2015-06-10 17:32:00'),(10,'testing demo  12.....','delhi','2015-03-06','Evening','today testing............................','200-500',126,'2015-06-10 17:16:09','2015-06-10 17:16:09'),(11,'testing demo  12.....','delhi','2015-03-06','Evening','today testing............................','200-500',126,'2015-06-10 17:16:29','2015-06-10 17:16:29'),(12,'Looking for a great photographer','toronto','0000-00-00','Morning','djf ksj dfklsj df lksdj fklsj df;lajd;fj ;asjdf','Not Specified',104,'2015-06-11 05:45:03','2015-06-11 05:45:03'),(16,'testing project of picinsider  ...............................','121','0000-00-00','Morning','dsfvvbgcbbdsjfvbhjdgvuhjdghvkjdbv','Not Specified',135,'2015-06-11 16:41:05','2015-06-11 16:41:05'),(17,'testing project picinsider','delhi','0000-00-00','Afternoon','vhkjdfgjbkhftrbkjtrok','100-200',122,'2015-06-11 16:47:45','2015-06-11 16:47:45'),(18,'kjlskdjf lksdj f','toronto','2015-02-06','Morning','lkj dasflkj sdflkj sdkljfksjdfk djfkj dfd','200-500',26,'2015-06-12 06:44:51','2015-06-15 16:16:29'),(19,'testing 123456','delhi','0000-00-00','Morning','hdhsfchvsgfdchdgyhfy','Not Specified',122,'2015-06-15 14:50:48','2015-06-15 14:50:48'),(20,'bcbbbb','chbgvnb','0000-00-00','Morning','hsdbfhdfgysfdartesrsgfdbgfghfnhgjhgj','Not Specified',155,'2015-06-16 11:29:12','2015-06-16 11:29:12'),(21,'Hello tester 2 - short summary','Toronto','0000-00-00','Morning','here\'s the long description of what we\'re looking for... it\'s a project in the jungles of subsaharan africa where people are interested and focused on thisngs to do... ipsum lorem ipsum','200-500',104,'2015-06-21 06:34:45','2015-06-21 06:34:45');
/*!40000 ALTER TABLE `gigs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invites`
--

DROP TABLE IF EXISTS `invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invited_by` int(10) unsigned NOT NULL,
  `invite_to` int(10) unsigned NOT NULL,
  `gig_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `invites_invited_by_foreign` (`invited_by`),
  KEY `invites_invite_to_foreign` (`invite_to`),
  KEY `invites_gig_id_foreign` (`gig_id`),
  CONSTRAINT `invites_gig_id_foreign` FOREIGN KEY (`gig_id`) REFERENCES `gigs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `invites_invited_by_foreign` FOREIGN KEY (`invited_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `invites_invite_to_foreign` FOREIGN KEY (`invite_to`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invites`
--

LOCK TABLES `invites` WRITE;
/*!40000 ALTER TABLE `invites` DISABLE KEYS */;
INSERT INTO `invites` VALUES (1,122,135,17,'2015-06-11 17:22:36','2015-06-11 17:22:36'),(2,122,135,17,'2015-06-11 17:22:40','2015-06-11 17:22:40'),(3,122,80,8,'2015-06-11 17:30:42','2015-06-11 17:30:42'),(4,122,30,5,'2015-06-11 17:36:27','2015-06-11 17:36:27'),(5,26,81,18,'2015-06-12 06:45:06','2015-06-12 06:45:06'),(6,122,128,19,'2015-06-15 14:52:17','2015-06-15 14:52:17'),(7,122,79,5,'2015-06-15 15:31:53','2015-06-15 15:31:53'),(8,122,79,5,'2015-06-15 15:31:55','2015-06-15 15:31:55'),(9,26,80,18,'2015-06-16 14:34:03','2015-06-16 14:34:03'),(10,26,80,18,'2015-06-16 14:34:06','2015-06-16 14:34:06'),(11,26,80,18,'2015-06-16 14:34:24','2015-06-16 14:34:24'),(12,26,25,18,'2015-06-16 14:34:57','2015-06-16 14:34:57'),(13,26,25,18,'2015-06-16 14:34:57','2015-06-16 14:34:57'),(14,26,25,18,'2015-06-16 14:34:58','2015-06-16 14:34:58'),(15,26,25,18,'2015-06-16 14:34:58','2015-06-16 14:34:58'),(16,26,25,18,'2015-06-16 14:34:58','2015-06-16 14:34:58'),(17,26,25,18,'2015-06-16 14:34:58','2015-06-16 14:34:58'),(18,26,25,18,'2015-06-16 14:34:58','2015-06-16 14:34:58'),(19,26,25,18,'2015-06-16 14:36:36','2015-06-16 14:36:36'),(20,26,25,18,'2015-06-16 14:36:36','2015-06-16 14:36:36'),(21,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(22,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(23,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(24,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(25,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(26,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(27,26,25,18,'2015-06-16 14:36:37','2015-06-16 14:36:37'),(28,26,80,18,'2015-06-16 14:38:02','2015-06-16 14:38:02'),(29,26,80,18,'2015-06-16 14:38:04','2015-06-16 14:38:04'),(30,26,80,18,'2015-06-16 14:38:04','2015-06-16 14:38:04'),(31,26,80,18,'2015-06-16 14:38:04','2015-06-16 14:38:04'),(32,26,80,18,'2015-06-16 14:38:04','2015-06-16 14:38:04'),(33,26,80,18,'2015-06-16 14:38:04','2015-06-16 14:38:04'),(34,26,80,18,'2015-06-16 14:38:45','2015-06-16 14:38:45'),(35,104,132,21,'2015-06-21 06:35:01','2015-06-21 06:35:01'),(36,104,81,21,'2015-06-21 06:35:35','2015-06-21 06:35:35');
/*!40000 ALTER TABLE `invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'registration',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `keys_user_id_foreign` (`user_id`),
  CONSTRAINT `keys_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keys`
--

LOCK TABLES `keys` WRITE;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
INSERT INTO `keys` VALUES (20,'$2y$10$Ws68180FdAiCgn3bxV8bxeShX9hie2Ifp2MUnq5hjJU8h6dU2GGaC','registration',25,'2015-05-22 17:53:40','2015-05-22 17:53:40'),(21,'$2y$10$J0PNYpebCKrfxW9Wmd.NNevf5bD5ygdkaKYvogpMF54YETe6gJPSm','registration',26,'2015-05-25 10:55:41','2015-05-25 10:55:41'),(22,'$2y$10$hmkIxw6KlHtAlH/AtrhLeetSlCIiLUBx.ppce88NBtZqRjVEutXKK','registration',27,'2015-05-25 11:18:18','2015-05-25 11:18:18'),(25,'$2y$10$vq8PR7eaje2m4qX.SIeA7eX7GShWMa1bo.9FNY2T46nIOG.vZ.i2i','registration',30,'2015-05-26 11:37:29','2015-05-26 11:37:29'),(34,'$2y$10$6QBRHRZp1Axl0Kj3Bpf2..O3RipCRPlBZRLhk3MPqQwRjN.w5SBze','registration',40,'2015-05-27 15:58:53','2015-05-27 15:58:53'),(35,'$2y$10$PLkXSoyaXm3Agm7deIIIEOeWJlCdXz0trpG6/GYkMJTZIUVY9OzES','registration',41,'2015-05-27 15:59:37','2015-05-27 15:59:37'),(36,'$2y$10$Gr2WtQtc4vGPOg/izrTGCOPlQEE9GS1K/Fjoy5TTPmJZ1I8vVcRXi','registration',42,'2015-05-27 16:00:19','2015-05-27 16:00:19'),(38,'$2y$10$bj46WfTpmxWG.vaWi3SdMuq60c0cM3BO8wuKhP84csy6JxEfz9bAC','registration',43,'2015-05-27 16:06:04','2015-05-27 16:06:04'),(45,'$2y$10$ekEqhYMGO.wdVb7BkSIKQOluw.aLKYGfstzFtqdz9qMXphwcJI7ou','registration',60,'2015-05-27 18:08:48','2015-05-27 18:08:48'),(46,'$2y$10$ZongHGjHj1J10y/KBJcsIu4nC9YUOVdSJ.2gBaFfihUXXJNcuB.BS','registration',61,'2015-05-27 18:35:07','2015-05-27 18:35:07'),(47,'$2y$10$aMtNjYMybK/yB/YA9B205eNpuTgPFu/nda9qtvTCsMxwrnUVtLAfC','registration',66,'2015-05-27 18:41:00','2015-05-27 18:41:00'),(48,'$2y$10$c3ScIa7oSbsjKZR9EDCCleMDzYbbsI2t3JRRrGgwa2iqVzpSbbwHO','registration',68,'2015-05-27 18:44:50','2015-05-27 18:44:50'),(50,'$2y$10$NYCpA3NUheYEvPPGZ8.KYee4MnW3Abp00RlYuhRVzQA1gycOyfjKa','registration',69,'2015-05-27 18:50:26','2015-05-27 18:50:26'),(51,'$2y$10$BLFJufKHhyqPDOaVQGXMJeokAafG4lSPP6mVcX119pfQBB7Zz5//2','registration',70,'2015-05-27 18:51:02','2015-05-27 18:51:02'),(52,'$2y$10$OIlm91jFEH.jnDOUFnHmTOa4tzat4ezlDZ.ktCKegTtIrDZ80RaXi','registration',71,'2015-05-28 17:28:31','2015-05-28 17:28:31'),(53,'$2y$10$zF5/8dVWjbzsylS/ot/98e8CZtZ5028qS.kIBpdHtuyuwYKEtuLfe','registration',72,'2015-05-28 17:30:01','2015-05-28 17:30:01'),(56,'$2y$10$rIlPQ9rJL9dMffd2nXR1vu9KgO1se6w9KM4FSneuvWyxWSofhrN7y','registration',76,'2015-05-28 17:57:03','2015-05-28 17:57:03'),(57,'$2y$10$rdqOyx1SUhwHa3RvI./1COvNhlXGn.guhPjdThN.SLd7TXW3jv1c2','registration',77,'2015-05-29 17:49:32','2015-05-29 17:49:32'),(58,'$2y$10$7C4bftRogOFjB0BlURArAepGZPMF97KESVwGiqkJq5.lARjJH1Xyy','registration',78,'2015-05-29 17:55:16','2015-05-29 17:55:16'),(59,'605794c8fb68430983be8e236fdc298d','reset',78,'2015-05-29 17:56:16','2015-05-29 17:56:16'),(60,'54c1d66e84e2cfc8037a5d54ae7650ff','reset',78,'2015-05-29 17:56:37','2015-05-29 17:56:37'),(61,'$2y$10$pwvVSFd4hSVZs6Kaq7d18eX.24KnTZPQdDzv74ICGcm5y7M9LwzxO','registration',79,'2015-05-29 18:14:23','2015-05-29 18:14:23'),(62,'$2y$10$snyXyMCHwjTTSLnengMd8OItBDmAu0IjNGgUE9dM5/11JoTJvM0le','registration',80,'2015-05-29 18:27:12','2015-05-29 18:27:12'),(63,'$2y$10$UDAmNPGonMW1qpz619j4LeHHnzo3Adje8ggbCTC7i0kBy4EzwmGQO','registration',81,'2015-05-29 18:31:05','2015-05-29 18:31:05'),(64,'$2y$10$2ot9JPQ53QXmkGys3jzrM.9nKONsY/kNkpzlb7vdcWQQFAV.y7Ona','registration',82,'2015-05-29 18:31:56','2015-05-29 18:31:56'),(65,'$2y$10$h9Ko2zNctkum/kU8HptjbeMIKOQQ4C1DjwUEV2Mgit.CwX7b1pQbe','registration',83,'2015-05-29 18:33:03','2015-05-29 18:33:03'),(66,'$2y$10$/s17CE5G0FPf/ZJv73T/qOBQnN/hl0vSiPzS/E9swWelWfrHMi/iO','registration',84,'2015-05-29 18:35:25','2015-05-29 18:35:25'),(67,'$2y$10$lf1bz2UhG2ZG8Q.KVZWLAO/v9niPzMbT61ELOudGKVyWU4X4D5JkC','registration',85,'2015-05-29 18:37:33','2015-05-29 18:37:33'),(68,'$2y$10$Z9AT6VlHqo9A.eIKvMgvHOlOn..EV4KbfvBTIZynFF40OyWEsur4G','registration',88,'2015-05-29 18:39:00','2015-05-29 18:39:00'),(70,'$2y$10$tRYxNAyRtrO8.JN5UubFiOJGlCxoYswtsP5eLXQPtJmi4oypl0eGu','registration',90,'2015-06-01 09:59:33','2015-06-01 09:59:33'),(71,'$2y$10$YB1Co1saRjX2ORcVeF06kedhvpE8oU.p0d3JhoZEnHoZ6G2RzREf6','registration',91,'2015-06-01 10:37:55','2015-06-01 10:37:55'),(73,'$2y$10$tenrsl0xsl1MJ7B48QBl/uM2I/bRgG25Ni6k4T6IYfEJ8xBoEtWmq','registration',93,'2015-06-01 10:42:55','2015-06-01 10:42:55'),(74,'$2y$10$uw4YSn0Qaabm7hhrpFnj4Ooj2LlhOqeeMGFVZ5HOQZ41ab6J0T01O','registration',94,'2015-06-01 10:50:25','2015-06-01 10:50:25'),(75,'$2y$10$Mc7XwdKGAiG9nwtq/gFPX.SB1koOdcLsFhLE84mH4kcNQdYCxY91e','registration',95,'2015-06-01 11:08:44','2015-06-01 11:08:44'),(76,'ab6dbb75c540cdadbaa6f75cb9515831','reset',91,'2015-06-01 11:16:23','2015-06-01 11:16:23'),(77,'$2y$10$RxtnmZBYhn1O1e1O9qPE7ud9seivfexfaTqmmUw0aHMFepFF9tx42','registration',96,'2015-06-02 12:07:57','2015-06-02 12:07:57'),(78,'$2y$10$P/i2wkcw8FtutFpjkxMecOqy9vBXmBGNsVNAXuSNjHjL4l6xE.JGm','registration',97,'2015-06-02 12:10:12','2015-06-02 12:10:12'),(79,'$2y$10$F8EpuDY9n5CCtgFQv2sqjOiHNMmK9F7h7B2ckmioOFh5XlW3OWfNi','registration',98,'2015-06-02 12:10:25','2015-06-02 12:10:25'),(80,'$2y$10$7ekompzCJleYM5RuvYJnWuXoh9NQSMQ/4MnQInSAa3iu2TNeGEmEe','registration',99,'2015-06-02 12:13:34','2015-06-02 12:13:34'),(81,'$2y$10$HhxfjgX5dCFDcrmF/pUDJexPe2GcoRw2ZMshTnLKAf8e3zAMlPLVe','registration',100,'2015-06-02 12:36:00','2015-06-02 12:36:00'),(82,'$2y$10$vb5Me.eY8ApHIDDq/Mbdd..yo30jgdKF2zAQYv2sJ0Z4oh3YmW/SW','registration',101,'2015-06-02 12:38:29','2015-06-02 12:38:29'),(83,'$2y$10$h2H7h1TYqZjrsInszIGBmuJrkBWgzd8LvU0BYPPLu3FjH84dpdTRe','registration',102,'2015-06-02 13:21:58','2015-06-02 13:21:58'),(84,'$2y$10$7RfzwKDJwAqtLqHVS6tK/eq855lJLEX3nachn8VbdcPnRvQC0apxu','registration',103,'2015-06-02 15:59:51','2015-06-02 15:59:51'),(85,'$2y$10$uvWZmpJ6IyaEr9uMEAFp7.6QOGCou288ddlkXHZirlx8UXA/QSaeW','registration',104,'2015-06-03 00:02:23','2015-06-03 00:02:23'),(86,'8441829f9dd8555a7812fd7c88dd5d02','reset',104,'2015-06-03 00:16:20','2015-06-03 00:16:20'),(87,'$2y$10$U3bvHAzSaPwElX55xJRXXuz3Wd7kydva7ErRZgasv.41hnqJRms2W','registration',105,'2015-06-03 05:50:29','2015-06-03 05:50:29'),(89,'$2y$10$tpzwzanm2vrVQvRpH9xdIOttmCcq76ehZiUoeLiBsDoOPuAwfbwW2','registration',107,'2015-06-03 18:39:40','2015-06-03 18:39:40'),(90,'$2y$10$4f0GE3lY9M5bDDP2CUiGouI1F4tw8MMdHi8yX3aTcjAyjssL2efHm','registration',108,'2015-06-03 18:57:24','2015-06-03 18:57:24'),(91,'$2y$10$QRrH.VyUTcAkL1SyDBPZZukMQKqxMxmMaHKW2pZreSrwLXh/G1IVu','registration',109,'2015-06-08 04:56:10','2015-06-08 04:56:10'),(92,'$2y$10$LaixSbC0JTZnOU3WknjS3eRXytaKJztKH5EP8db11Th01rx.Kr1aa','registration',110,'2015-06-08 04:56:45','2015-06-08 04:56:45'),(93,'$2y$10$BnmE1XfaFkjv8xYZxFW0Tux2RYSuzAlWYbA9cVGVKTPV1ddHEMs2e','registration',111,'2015-06-08 04:57:18','2015-06-08 04:57:18'),(94,'$2y$10$yF8kp69GCiJAvCIqHWFuJu2kBa2qqn76WJv6M6ZaXOKPTtaV.fTCS','registration',112,'2015-06-09 13:17:23','2015-06-09 13:17:23'),(96,'$2y$10$hXzqZyVWiU0aWcze/uTT9.92OanYYNbyYqbO.ESiYoZfTqeNF4Iv2','registration',118,'2015-06-09 14:12:28','2015-06-09 14:12:28'),(99,'$2y$10$zKudJvYLAJliYcwjE7L0xeDjJjSY3o1YXPYNkhUkRAly/mSq3/GAO','registration',121,'2015-06-09 14:51:03','2015-06-09 14:51:03'),(100,'$2y$10$feKN1FooJ1ZAmdh/0yYhZeu206IEq25/mlShMdwkw8zbI9E2/ksEe','registration',122,'2015-06-09 15:37:14','2015-06-09 15:37:14'),(101,'$2y$10$Kf6Zz0ZKJNpyi0YYY5txZO4tOCCBGG1K9X8/bvqfZS38Zazt7UJ5m','registration',123,'2015-06-09 15:43:05','2015-06-09 15:43:05'),(102,'$2y$10$gmOMI56w/ghnqtlfG.D/Rexwxvv0wTvEkSZx5wxL08ZhKCtmyWWEy','registration',124,'2015-06-09 15:46:47','2015-06-09 15:46:47'),(103,'$2y$10$bPoc64MJh1pZUiLOZhL0je6K2PBp31SHasmSecZWO81lgaMOaTUGi','registration',125,'2015-06-09 16:44:52','2015-06-09 16:44:52'),(104,'$2y$10$ApTgpo2JqVEWHcF8rPuhMegRZjVyhibhms8IGs.jP5eY7X4/6Fdry','registration',126,'2015-06-10 16:46:16','2015-06-10 16:46:16'),(106,'$2y$10$laX38i7.pGwoFXJTnrkb2eTFpW9oAsYGCeBaNp6DlJ/eb5eAbj/wu','registration',128,'2015-06-10 16:56:17','2015-06-10 16:56:17'),(107,'06877097b518f3607ba2ab269d45e567','reset',66,'2015-06-10 17:05:51','2015-06-10 17:05:51'),(108,'$2y$10$LDiH/trNEdjacp48z04ISeG7H0UPq7pOWL5/yCgDAhNRLxyV84KNi','registration',129,'2015-06-10 17:37:51','2015-06-10 17:37:51'),(111,'$2y$10$3p/VrwVOX9hm56evBb56pOphBNpKvCqIN6F.fd3ZRZhg7GZ6jUFEa','registration',132,'2015-06-11 16:01:36','2015-06-11 16:01:36'),(112,'$2y$10$BWqSHWBor5R9doY85kN2Au4P2Lmb3x4MVs03zfc6feK9CbUS7uxbi','registration',133,'2015-06-11 16:20:51','2015-06-11 16:20:51'),(114,'$2y$10$xTFaxGni/Dt2xyZY5VBoB.6WWINWUvGXwQdlWdm6HGdeG6swr741y','registration',135,'2015-06-11 16:23:27','2015-06-11 16:23:27'),(115,'$2y$10$0X.kaAS/gDqybQUZk0jzs.rTlce.vxEx3hRcGKrZbXgoVP3fbYid.','registration',137,'2015-06-11 16:44:27','2015-06-11 16:44:27'),(116,'$2y$10$C6.MqyHOSkz2iKAD4j9g3.7jF754a2TUGDBQbTO.SG9vOSfxBtwA.','registration',139,'2015-06-11 18:20:55','2015-06-11 18:20:55'),(117,'$2y$10$ay/8LswDxFZkdWRBXhDGce6SRpZWgrktQ45ZrT2ECREefM2gQq0Lu','registration',140,'2015-06-12 12:15:50','2015-06-12 12:15:50'),(118,'$2y$10$tEjDn2GFBR6XxnjMacxGSuCgzywAxapsZ03PrOluq.UzosbGfKkKq','registration',141,'2015-06-12 17:46:04','2015-06-12 17:46:04'),(119,'$2y$10$mahPRBYJxBKdVGFeDFqc4u4j0C1xxJaWVmyMNkr.bcMtBrJ635E9y','registration',142,'2015-06-14 16:35:58','2015-06-14 16:35:58'),(120,'$2y$10$vcIyjdj4itrfQhN1.rvbMeSnKwsqwvu44AiTE1eoU50X4HKbDuxae','registration',143,'2015-06-15 14:54:37','2015-06-15 14:54:37'),(121,'$2y$10$4OauIDVPBiLNlwoHJ9E9i.Fnke7eSRBwbQYLlvSh1XZ3QfqYCmF6e','registration',144,'2015-06-15 15:00:25','2015-06-15 15:00:25'),(122,'$2y$10$nS63a1XO7jBXMhxqzLKVfevH6/Qdzx7Rh2FPnPdBb2m0RMZ/UEcqe','registration',145,'2015-06-15 15:02:42','2015-06-15 15:02:42'),(123,'$2y$10$wZOWaQSAVjYd0CcMUf8UFen35D/N7tUmEWUWXGGXb/TdZU43nQPjm','registration',146,'2015-06-15 15:08:39','2015-06-15 15:08:39'),(124,'$2y$10$eOgnT1wko8JxFshypuCxfe3lwq5R12SdbAJtqaP1VEfH/nZgvpmOG','registration',147,'2015-06-15 15:13:15','2015-06-15 15:13:15'),(125,'$2y$10$vmweQlMH0WfyDaN9TVvC/eu3HMsGDWUqQz7jE7lyW5S.Y.cwLdVuK','registration',148,'2015-06-15 15:18:41','2015-06-15 15:18:41'),(126,'$2y$10$TbRW7Ha4Yk7P3VZb2nPQheHQtbyblfF1cI2BMcy0NiKOeDFZTQlP2','registration',149,'2015-06-15 15:28:31','2015-06-15 15:28:31'),(127,'$2y$10$TnumOhrm.1M6/4d2rJxKRuyM6Yn/KOjADbmhfnVGx34EuXX3sLQVS','registration',150,'2015-06-15 15:36:55','2015-06-15 15:36:55'),(128,'68074a2836b3917a4214091081ec9d2b','reset',143,'2015-06-15 16:03:27','2015-06-15 16:03:27'),(129,'$2y$10$7w/tnrMtnhwnBAKl/RCLpOjeP8ewrKkuY9k4YtN47FiW64SIFZ0LO','registration',152,'2015-06-15 16:13:33','2015-06-15 16:13:33'),(130,'$2y$10$2k30vFAIZrVPu7Ssex0cee5aN.gdbGHD2JjcynhJZMbyq8JVHoLNK','registration',153,'2015-06-15 16:17:55','2015-06-15 16:17:55'),(132,'$2y$10$Gmk64prDmRp8VPxp2yp6AO1D27Ix4hODdUf4GvfT/PaBVwWIoPs3S','registration',155,'2015-06-16 11:16:14','2015-06-16 11:16:14'),(133,'$2y$10$BukCFk/.Gn3hp0t.dE5fierzFS/dogirJmGsRtz1YrNxRuAZPAEVi','registration',156,'2015-06-16 11:38:16','2015-06-16 11:38:16'),(134,'$2y$10$5cOUr.LeMltvA1p4V74kiOl7NSLQbIZzVzc7JIw4RFw.M9BzeQMCq','registration',157,'2015-06-16 17:00:27','2015-06-16 17:00:27'),(135,'$2y$10$3Kt.kP1GJUKbJvMiVwYfrOTJRV42UqSwYDhxChAkrSJsUjLNFf6H.','registration',158,'2015-06-20 11:44:56','2015-06-20 11:44:56'),(136,'0243edd345b6fc08dd2b6e25106b1c41','reset',104,'2015-06-21 06:15:26','2015-06-21 06:15:26'),(137,'3d516fb15fe6aa8e4d7f560acb6ccd3f','reset',104,'2015-06-21 22:36:15','2015-06-21 22:36:15');
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `photographer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `languages_photographer_id_foreign` (`photographer_id`),
  CONSTRAINT `languages_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_id` int(10) unsigned NOT NULL,
  `from_id` int(10) unsigned DEFAULT NULL,
  `to_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `connect` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `messages_to_id_foreign` (`to_id`),
  CONSTRAINT `messages_to_id_foreign` FOREIGN KEY (`to_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,25,0,' Matthew Charles ','Some name','someone@something.com','ashsjakhkjdsh','Call me','2015-06-02 14:26:48','2015-06-02 14:26:48'),(2,81,0,' Charles David ','Peter pan','peter@pan.com','Call me peter pan','Call me','2015-06-02 15:14:43','2015-06-02 15:14:43'),(3,80,0,' Chris Morris ','Peter england','peter@england.com','Sending a test message from mobile','Call me','2015-06-02 19:43:51','2015-06-02 19:43:51'),(4,80,0,' Chris Morris ','Shahrukh Khan','skhan@myzealit.com','testing thsi box','Call me','2015-06-12 13:00:23','2015-06-12 13:00:23'),(5,79,26,'John David','gaurav','gaurav@yopmail.com','Test message','Call me','2015-06-12 16:15:55','2015-06-12 16:15:55'),(6,25,122,'Matthew Charles','Neha','verma@yopmail.com','testing demo','Send email','2015-06-15 14:31:46','2015-06-15 14:31:46'),(7,81,0,'Charles David','Web','webappstore2015@gmail.com','Hello Picinsider...','Send email','2015-06-19 17:00:21','2015-06-19 17:00:21'),(8,81,0,'Charles David','Web','webappstore2015@gmail.com','Hello Picinsider...','Send email','2015-06-19 17:00:42','2015-06-19 17:00:42'),(9,97,0,'soni','Radhey','radhey.mzit@gmail.com','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).','Send email','2015-06-19 17:04:56','2015-06-19 17:04:56'),(10,157,0,'Tom','zoya','zoya@yopmail.com','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.','Internal Message','2015-06-19 17:07:59','2015-06-19 17:07:59'),(11,157,0,'Tom','zoya','zoya@yopmail.com','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.','Send email','2015-06-19 17:08:37','2015-06-19 17:08:37'),(12,157,0,'Tom','zoya','zoya@yopmail.com','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.','Call me','2015-06-19 17:08:56','2015-06-19 17:08:56'),(13,140,0,'web app','Radhey','radhey.mzit@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Send email','2015-06-19 17:22:10','2015-06-19 17:22:10'),(14,140,0,'web app','zoya','zoya@yopmail.com','EDW T4 EFGT 34FR 3RFQ EWF F','Internal Message','2015-06-19 17:26:24','2015-06-19 17:26:24');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_05_13_120532_create_countries_table',1),('2014_10_12_000000_create_users_table',2),('2014_10_12_100000_create_password_resets_table',2),('2015_05_11_094615_create_categories_table',2),('2015_05_11_101455_create_photographers_table',2),('2015_05_11_113115_create_cities_table',2),('2015_05_11_122008_create_reviews_table',2),('2015_05_12_103301_create_roles_table',2),('2015_05_13_061249_create_keys_table',2),('2015_05_13_071233_create_generalfunctions_table',2),('2015_05_14_111336_create_photographercategories_table',2),('2015_05_14_125022_create_photos_table',2),('2015_05_14_125803_create_messages_table',2),('2015_05_16_052317_create_rating_likes_table',2),('2015_05_20_070732_create_profiles_table',2),('2015_05_25_055910_create_gigs_table',3),('2015_05_26_071216_create_sliders_table',3),('2015_05_27_061622_create_gig_categories_table',3),('2015_05_27_061758_create_invites_table',3),('2015_05_27_093052_create_abouts_table',4),('2015_05_28_100351_create_flags_table',5),('2015_06_05_051453_create_blogs_table',7),('2015_05_14_123831_create_services_table',8),('2015_06_11_064708_create_styles_table',8),('2015_06_11_065311_create_photographer_styles_table',8),('2015_06_11_065319_create_photographer_services_table',8),('2015_06_11_073800_create_languages_table',8),('2015_06_02_124209_create_recentviews_table',9);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photographer_services`
--

DROP TABLE IF EXISTS `photographer_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photographer_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photographer_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `photographer_services_photographer_id_foreign` (`photographer_id`),
  KEY `photographer_services_service_id_foreign` (`service_id`),
  CONSTRAINT `photographer_services_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `photographer_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photographer_services`
--

LOCK TABLES `photographer_services` WRITE;
/*!40000 ALTER TABLE `photographer_services` DISABLE KEYS */;
INSERT INTO `photographer_services` VALUES (1,133,1,'2015-06-11 16:33:51','2015-06-11 16:33:51'),(2,133,2,'2015-06-11 16:33:51','2015-06-11 16:33:51'),(3,133,3,'2015-06-11 16:33:51','2015-06-11 16:33:51');
/*!40000 ALTER TABLE `photographer_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photographer_styles`
--

DROP TABLE IF EXISTS `photographer_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photographer_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photographer_id` int(10) unsigned NOT NULL,
  `style_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `photographer_styles_photographer_id_foreign` (`photographer_id`),
  KEY `photographer_styles_style_id_foreign` (`style_id`),
  CONSTRAINT `photographer_styles_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `photographer_styles_style_id_foreign` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photographer_styles`
--

LOCK TABLES `photographer_styles` WRITE;
/*!40000 ALTER TABLE `photographer_styles` DISABLE KEYS */;
INSERT INTO `photographer_styles` VALUES (3,133,1,'2015-06-11 16:33:51','2015-06-11 16:33:51'),(4,133,2,'2015-06-11 16:33:51','2015-06-11 16:33:51');
/*!40000 ALTER TABLE `photographer_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photographercategories`
--

DROP TABLE IF EXISTS `photographercategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photographercategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `photographer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `photographercategories_photographer_id_foreign` (`photographer_id`),
  CONSTRAINT `photographercategories_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photographercategories`
--

LOCK TABLES `photographercategories` WRITE;
/*!40000 ALTER TABLE `photographercategories` DISABLE KEYS */;
INSERT INTO `photographercategories` VALUES (11,'Wedding',25,'2015-05-22 17:54:22','2015-05-22 17:54:22'),(12,'Event',25,'2015-05-22 17:54:22','2015-05-22 17:54:22'),(13,'product',25,'2015-05-22 17:54:22','2015-05-22 17:54:22'),(15,'Wedding',30,'2015-05-26 12:18:42','2015-05-26 12:18:42'),(16,'Event',30,'2015-05-26 12:18:42','2015-05-26 12:18:42'),(17,'product',30,'2015-05-26 12:18:42','2015-05-26 12:18:42'),(18,'Engagement',30,'2015-05-26 12:18:42','2015-05-26 12:18:42'),(21,'Wedding',77,'2015-05-29 17:52:56','2015-05-29 17:52:56'),(22,'Event',102,'2015-06-02 13:26:14','2015-06-02 13:26:14'),(23,'Wedding',103,'2015-06-02 16:01:47','2015-06-02 16:01:47'),(24,'Event',103,'2015-06-02 16:01:47','2015-06-02 16:01:47'),(25,'product',103,'2015-06-02 16:01:47','2015-06-02 16:01:47'),(26,'Wedding',107,'2015-06-03 18:40:04','2015-06-03 18:40:04'),(27,'Event',107,'2015-06-03 18:40:04','2015-06-03 18:40:04'),(28,'Wedding',108,'2015-06-03 18:57:33','2015-06-03 18:57:33'),(29,'Event',108,'2015-06-03 18:57:33','2015-06-03 18:57:33'),(30,'product',108,'2015-06-03 18:57:33','2015-06-03 18:57:33'),(31,'Wedding',121,'2015-06-09 14:58:12','2015-06-09 14:58:12'),(32,'Event',121,'2015-06-09 14:58:12','2015-06-09 14:58:12'),(33,'Wedding',125,'2015-06-09 16:46:21','2015-06-09 16:46:21'),(34,'Wedding',133,'2015-06-11 16:22:08','2015-06-11 16:22:08'),(35,'Event',133,'2015-06-11 16:22:08','2015-06-11 16:22:08'),(36,'product',133,'2015-06-11 16:22:08','2015-06-11 16:22:08'),(37,'Wedding',140,'2015-06-12 12:18:24','2015-06-12 12:18:24'),(38,'Wedding',140,'2015-06-12 14:20:32','2015-06-12 14:20:32'),(39,'Wedding',142,'2015-06-14 16:40:32','2015-06-14 16:40:32'),(40,'product',142,'2015-06-14 16:40:32','2015-06-14 16:40:32'),(41,'Wedding',145,'2015-06-15 15:03:10','2015-06-15 15:03:10');
/*!40000 ALTER TABLE `photographercategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photographers`
--

DROP TABLE IF EXISTS `photographers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photographers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `company_name` text COLLATE utf8_unicode_ci,
  `phone_number` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` text COLLATE utf8_unicode_ci,
  `city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `established` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `studio` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `under500` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `under1000` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pinterest` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `certified` tinyint(4) DEFAULT NULL,
  `brand` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `interests` text COLLATE utf8_unicode_ci,
  `photo` text COLLATE utf8_unicode_ci,
  `logo` text COLLATE utf8_unicode_ci,
  `featured` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `subscribe` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `photographers_user_id_foreign` (`user_id`),
  CONSTRAINT `photographers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photographers`
--

LOCK TABLES `photographers` WRITE;
/*!40000 ALTER TABLE `photographers` DISABLE KEYS */;
INSERT INTO `photographers` VALUES (12,'Matthew Charles','Charles Photography','8527472679','www.matthew.com','Toronto',NULL,'Yes','2000-3000','Yes','Yes','facebook','twitter','pinterest','gplus',NULL,0,'Canon','We are very good ',NULL,'XWDODeJILeuE.jpg','','No','Yes','Yes',25,'2015-05-22 17:53:40','2015-05-22 17:54:22'),(14,'Steve Smith','Spark Photography','02075124560','www.google.com','Vancouver','','No','500-1000','Yes','Yes','','','','','',1,'nikkon','This is a test photography',NULL,'PE5JDA2JG9SV.jpg',NULL,'Yes','Yes','Yes',30,'2015-05-26 11:37:29','2015-06-09 05:58:19'),(18,'','ttested123','','','','','No','1000-2000','No','No','','','','','',0,'','',NULL,NULL,NULL,'No','No','No',66,'2015-05-27 18:41:00','2015-06-15 16:45:17'),(19,'test test','ttested123','','','',NULL,'Yes','0-1000',NULL,NULL,'','','','',NULL,0,'','',NULL,'VwngJLDfi37h.jpg','','No',NULL,NULL,68,'2015-05-27 18:44:50','2015-05-27 18:45:01'),(20,'','atested',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,70,'2015-05-27 18:51:02','2015-05-27 18:51:02'),(23,'Mark Hales','tttttttt','','sfasfas','sfas','fasfasfasf','Yes','Yes','Yes','Yes','asfasfas','','','','',0,'','',NULL,'jvbgRqbs4nyi.jpg',NULL,'No','Yes','No',76,'2015-05-28 17:57:03','2015-05-30 00:47:03'),(24,'aaaaaa aaaaaa','aaaaaa','9716970956','www.gmail.com','delhi',NULL,'Yes','0-1000',NULL,NULL,'','','','',NULL,0,'delhi','test',NULL,'jqpzFJGaG4P7.jpeg','','No',NULL,NULL,77,'2015-05-29 17:49:32','2015-05-29 17:52:56'),(25,'John David','David Co. ','','','New YORK','2015','Yes','1000-2000','Yes','Yes','','','','','',1,'','I have worked for wedding and various functions.\r\nI have worked for wedding and various functions.\r\nI have worked for wedding and various functions.',NULL,'YTPJFLTuCWjx.jpg',NULL,'Yes','Yes','No',79,'2015-05-29 18:14:23','2015-06-03 18:47:15'),(26,'Chris Morris','Morris Co. Ltd','','','California','2000','Yes','Yes','Yes','Yes','','','','','',0,'','This is just a test description for photographer.\r\nThis is just a test description for photographer\r\nThis is just a test description for photographer',NULL,'NFcDUm026d2w.jpg',NULL,'Yes','Yes','No',80,'2015-05-29 18:27:12','2015-05-29 18:52:38'),(27,'Charles David','Charles','','','','','Yes','1000-2000','Yes','Yes','','','','','',0,'','This is just a test description for photographer',NULL,'kY36mIuBlN09.jpg',NULL,'Yes','Yes','No',81,'2015-05-29 18:31:05','2015-06-02 11:57:02'),(28,'Brad Hogg','brad cop','','','','','Yes','Yes','Yes','Yes','','','','','',0,'','This is just a test description for photographer',NULL,'BHsmXP6WTg9c.jpg',NULL,'No','Yes','No',82,'2015-05-29 18:31:56','2015-06-09 16:43:16'),(29,'Nathen Coulter','nathen corp','','','','','Yes','1000-2000','Yes','Yes','','','','','',0,'','This is just a test description for photographer',NULL,'1oiMyXu88iGz.jpg',NULL,'Yes','Yes','No',83,'2015-05-29 18:33:03','2015-06-02 11:56:09'),(30,'David Villa','David Co. ',NULL,'','','','Yes','Yes','Yes','Yes','','','','',NULL,0,'','This is just a test description for photographe',NULL,'qwuN8etw5zwk.jpg','','Yes','Yes',NULL,84,'2015-05-29 18:35:25','2015-05-29 18:35:25'),(31,'Ryan Ten','Ten',NULL,'','','','Yes','Yes','Yes','Yes','','','','',NULL,0,'','',NULL,'v1ZM4hf8WICK.jpg','','Yes','Yes',NULL,85,'2015-05-29 18:37:33','2015-05-29 18:37:33'),(32,'Andrew  Rebet','Charles','','','','','Yes','Yes','Yes','Yes','','','','','',0,'','',NULL,'fcFdUebl45cp.jpeg',NULL,'No','Yes','No',88,'2015-05-29 18:39:00','2015-05-30 00:46:50'),(34,'122321 nh514524','jkjlkj',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Yes',NULL,NULL,90,'2015-06-01 09:59:31','2015-06-11 09:21:45'),(36,'asfasfasf asfasfasfasfasfas','ffasfasf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,93,'2015-06-01 10:42:54','2015-06-01 10:42:54'),(37,'David Miller','A&S Comp.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,94,'2015-06-01 10:50:25','2015-06-01 10:50:25'),(38,'tested','aaaaaa','','','','','Yes','1000-2000','Yes','Yes','','','','','',0,'','',NULL,'N0ijibnXJMNB.jpg',NULL,'Yes','Yes','No',95,'2015-06-01 11:08:44','2015-06-09 05:52:46'),(39,'soni','itp','','','','','Yes','100-500','Yes','Yes','','','','','',0,'','',NULL,'DONMTDTppzIg.jpeg','','No','Yes',NULL,97,'2015-06-02 12:10:12','2015-06-11 09:37:37'),(40,'1234 5678','itp','nvkjdfhbvujgfvc','www.sparkphotography.com','delhi',NULL,'Yes','0-1000',NULL,NULL,'','','','',NULL,0,'canvas','this a testing demo',NULL,NULL,'','No',NULL,NULL,102,'2015-06-02 13:21:58','2015-06-02 13:26:14'),(41,'Latest Photography','Latest Photography','212-222-2222','www.photography.com','Boston',NULL,'Yes','1000-2000','Yes','Yes','facebook','twitter','pinterest','gplus',NULL,0,'Kodak','Photography Photography Photography Photography Photography Photography Photography Photography Photography Photography Photography Photography ',NULL,'PZujaRIuKWPe.jpg','','No','Yes',NULL,103,'2015-06-02 15:59:51','2015-06-02 16:01:47'),(42,'leslie mark','Leslie\'s photos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,105,'2015-06-03 05:50:28','2015-06-03 05:50:28'),(44,'Lazar Angelov','Lazar photography','','','',NULL,'Yes','0-1000',NULL,NULL,'','','','',NULL,0,'','',NULL,'jbrHTXnSLMlt.jpg','','No','Yes','Yes',107,'2015-06-03 18:39:39','2015-06-03 18:40:04'),(45,'Matthew Charles','Matthew Photography','','','',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','',NULL,'izyHbrN4vUn6.jpg','','No',NULL,NULL,108,'2015-06-03 18:57:24','2015-06-03 18:57:33'),(46,'First Last','R co 06.07.2015',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,109,'2015-06-08 04:56:10','2015-06-08 04:56:10'),(47,'Firstc Lastc','R co',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,110,'2015-06-08 04:56:45','2015-06-08 04:56:45'),(48,'Firstcss Lastcss','R co',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'No',NULL,NULL,111,'2015-06-08 04:57:18','2015-06-08 04:57:18'),(50,'kldjfklj kjhdskfh','testint g','8527472679','www.matthew.com','Toronto',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','',NULL,'T9WFqux2mcqD.jpg','','No','Yes','Yes',121,'2015-06-09 14:51:02','2015-06-09 14:58:12'),(51,'132132','itp','','','','','Yes','1000-2000','Yes','Yes','','','','','',0,'','',NULL,'5CA7YxQMOpSk.png',NULL,'Yes','Yes','No',123,'2015-06-09 15:43:05','2015-06-11 09:26:58'),(52,'1234 5678','hcl','hjbjnhbj','122','bnb',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','bnb',NULL,'5rdHFdNd48XK.png','','No',NULL,NULL,124,'2015-06-09 15:46:47','2015-06-09 15:47:44'),(53,'1234 5678','hcl','','','',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','',NULL,'E2cPHF3ArXAL.png','','No',NULL,NULL,125,'2015-06-09 16:44:51','2015-06-09 16:56:58'),(55,'516321321','112232',NULL,'122','12','2015','Yes','0-1000','Yes','Yes','','','','',NULL,0,'','',NULL,'jE7QnETJkJ0O.xlsx','','No','Yes',NULL,128,'2015-06-10 16:56:17','2015-06-10 16:56:17'),(56,'shilpi verma','hcl','1321321','www.sparkphotography.com','',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','cbnbvnmbmnb',NULL,'BpAo0gMp9XGu.png','','No',NULL,NULL,129,'2015-06-10 17:37:50','2015-06-10 17:40:04'),(58,'ricky swain','hcl','545-142-1212','www.photography.com','delhi',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'',NULL,NULL,'bqB2zhREvZhe.png','','No',NULL,NULL,132,'2015-06-11 16:01:36','2015-06-11 16:04:52'),(59,'Some Company','Some Company ltd','212-457-4547','www.somecompany.com','Toronto',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','djkhsdlkjflskdjf lskdjflskdjf lksdjflksdjflksdjflksdjflksdjflkj','Photography','CmWxPPza7hbG.jpg','','No',NULL,NULL,133,'2015-06-11 16:20:50','2015-06-11 16:33:51'),(61,'sonal tyagi','itp','213-213-____','WWW.PHOTOGRAPHY.COM','',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','','','aq2cUAAHntdH.png','','No',NULL,NULL,135,'2015-06-11 16:23:27','2015-06-11 16:25:48'),(62,'test test','xyzt','145-254-3641','ww.google.com','noida',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'test',NULL,NULL,'ci0KHdt4qsRh.jpg','','No','Yes',NULL,137,'2015-06-11 16:44:27','2015-06-11 16:47:00'),(63,'somethigsomething some','something',NULL,'','','','Yes','0-1000','Yes','Yes','','','','',NULL,0,'','',NULL,'2j25AQ7A92Rz.jpg','','No','Yes',NULL,139,'2015-06-11 18:20:55','2015-06-11 18:20:55'),(64,'web app','Test Technologies','2222222','www.testtech.com','Delhi','2012','Yes','1000-2000','No','Yes','','','','','',0,'','xxxxxxxxxxx','','Kap6h87ZiPyD.jpg',NULL,'No','No','No',140,'2015-06-12 12:15:50','2015-06-22 06:56:39'),(65,'Someone','some company',NULL,'','','','Yes','0-1000','Yes','Yes','','','','',NULL,0,'','',NULL,'QjTSTmfnBYbx.jpg','','No','Yes',NULL,141,'2015-06-12 17:46:04','2015-06-12 17:46:04'),(66,'R Dizzle','RL photos','232-232-3232','www.tester.com','Toronto',NULL,'No','1000-2000',NULL,NULL,'','','','',NULL,0,'NIkeon','','','default.png','','No','Yes','Yes',142,'2015-06-14 16:35:57','2015-06-14 16:44:46'),(67,'gfshfv ghfvh','itp','','','',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'',NULL,NULL,'/uploads/photographers/default.png','','No',NULL,NULL,144,'2015-06-15 15:00:25','2015-06-15 15:02:50'),(68,'kriti sharma','XYZ','145-236-9857','www.xyz.com','Noida, AS, India',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'','','','/uploads/photographers/default.png','','No',NULL,NULL,145,'2015-06-15 15:02:42','2015-06-15 15:03:16'),(69,'shilpa','itp',NULL,'','','','Yes','0-1000','Yes','Yes','','','','',NULL,0,'','',NULL,'ecgza6rDodgL.png','','Yes','Yes',NULL,146,'2015-06-15 15:08:39','2015-06-17 10:40:16'),(70,'viky','hcl','','','','','Yes','0-1000','Yes','Yes','','','','','',0,'','',NULL,'2RRuMu0IR7NE.JPG','','No','Yes',NULL,147,'2015-06-15 15:13:15','2015-06-19 17:20:02'),(71,'test test','ABC','','','Noida, UP, India',NULL,'No','0-1000',NULL,NULL,'','','','',NULL,0,'',NULL,NULL,'/uploads/photographers/default.png','','Yes',NULL,NULL,149,'2015-06-15 15:28:31','2015-06-17 10:40:14'),(72,'test parashar','xyzt','','','Delhi, CA, United States','','No','1000-2000','No','No','','','','','',0,'','','','noCWvhZyqouV.jpg',NULL,'Yes','No','No',150,'2015-06-15 15:36:55','2015-06-17 10:40:13'),(73,'hello app','Test Technologies','','','','','No','','No','No','','','','','',NULL,'','',NULL,NULL,NULL,'Yes',NULL,NULL,156,'2015-06-16 11:38:16','2015-06-17 10:13:52'),(74,'Tom','Test Technologies','','','Delhi','','Yes','1000-2000','Yes','Yes','','','','','',0,'','',NULL,'4deEuwHUYsLr.jpg',NULL,'Yes','Yes','No',157,'2015-06-16 17:00:27','2015-06-16 17:01:23');
/*!40000 ALTER TABLE `photographers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_url` text COLLATE utf8_unicode_ci NOT NULL,
  `photographer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `photos_photographer_id_foreign` (`photographer_id`),
  CONSTRAINT `photos_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (3,'banner-img.png',97,'2015-06-11 09:35:18','2015-06-11 09:35:18'),(4,'banner-img.png',135,'2015-06-11 17:45:15','2015-06-11 17:45:15'),(5,'banner-img.png',147,'2015-06-15 15:39:00','2015-06-15 15:39:00'),(6,'logo1-globe-by-zeth-lorenzo.jpg',156,'2015-06-16 14:53:44','2015-06-16 14:53:44'),(7,'felix-capital-team-lg-fc-an.jpg',140,'2015-06-21 22:45:02','2015-06-21 22:45:02');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_foreign` (`user_id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (4,'213213213213213','tested','tested','tested','213213213','United States',61,'2015-05-27 18:35:07','2015-05-27 18:35:07'),(5,'','','','','','United States',72,'2015-05-28 17:52:07','2015-05-28 17:52:07'),(6,'145-236-5785','','','','','United States',27,'2015-06-15 14:39:48','2015-06-15 14:39:48'),(7,'','','','','','United States',26,'2015-06-15 15:48:41','2015-06-15 15:48:41'),(8,'','','','','','United States',152,'2015-06-15 16:13:33','2015-06-15 16:13:33'),(9,'','','','','','United States',153,'2015-06-15 16:17:55','2015-06-15 16:17:55'),(10,NULL,NULL,NULL,NULL,NULL,NULL,122,'2015-06-15 16:29:53','2015-06-15 16:29:53'),(11,'','','','','','United States',25,'2015-06-16 09:59:01','2015-06-16 09:59:01'),(12,'','','','','','India',158,'2015-06-20 11:44:56','2015-06-20 11:44:56'),(13,'','','','','','United States',96,'2015-06-20 12:09:01','2015-06-20 12:09:01'),(14,NULL,NULL,NULL,NULL,NULL,NULL,104,'2015-06-21 06:18:20','2015-06-21 06:18:20');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_likes`
--

DROP TABLE IF EXISTS `rating_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `likes` int(11) DEFAULT NULL,
  `dislikes` int(11) DEFAULT NULL,
  `review_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `rating_likes_review_id_foreign` (`review_id`),
  CONSTRAINT `rating_likes_review_id_foreign` FOREIGN KEY (`review_id`) REFERENCES `reviews` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_likes`
--

LOCK TABLES `rating_likes` WRITE;
/*!40000 ALTER TABLE `rating_likes` DISABLE KEYS */;
INSERT INTO `rating_likes` VALUES (3,1,NULL,12,'2015-06-02 12:25:27','2015-06-02 12:25:27'),(5,1,NULL,6,'2015-06-03 05:30:38','2015-06-03 05:30:38'),(7,1,NULL,15,'2015-06-03 05:40:26','2015-06-03 05:40:26'),(8,1,NULL,104,'2015-06-16 14:22:41','2015-06-16 14:22:41'),(9,18,1,111,'2015-06-17 09:28:51','2015-06-17 09:30:49'),(10,NULL,1,111,'2015-06-17 09:28:53','2015-06-17 09:28:53'),(11,NULL,1,111,'2015-06-17 09:28:54','2015-06-17 09:28:54'),(12,NULL,1,111,'2015-06-17 09:28:54','2015-06-17 09:28:54'),(13,NULL,1,111,'2015-06-17 09:28:54','2015-06-17 09:28:54'),(14,NULL,1,111,'2015-06-17 09:28:57','2015-06-17 09:28:57'),(15,NULL,1,111,'2015-06-17 09:29:12','2015-06-17 09:29:12'),(16,NULL,1,111,'2015-06-17 09:29:12','2015-06-17 09:29:12'),(17,NULL,1,111,'2015-06-17 09:29:12','2015-06-17 09:29:12'),(18,NULL,1,111,'2015-06-17 09:29:12','2015-06-17 09:29:12'),(19,NULL,1,111,'2015-06-17 09:29:12','2015-06-17 09:29:12'),(20,NULL,1,111,'2015-06-17 09:30:47','2015-06-17 09:30:47'),(21,NULL,1,111,'2015-06-17 09:30:48','2015-06-17 09:30:48'),(22,NULL,1,111,'2015-06-17 09:30:48','2015-06-17 09:30:48'),(23,NULL,1,111,'2015-06-17 09:30:49','2015-06-17 09:30:49'),(24,NULL,1,111,'2015-06-17 09:30:49','2015-06-17 09:30:49'),(25,8,NULL,113,'2015-06-17 09:58:27','2015-06-17 10:14:40'),(26,NULL,1,113,'2015-06-17 09:58:30','2015-06-17 09:58:30'),(27,NULL,1,113,'2015-06-17 09:58:30','2015-06-17 09:58:30'),(28,NULL,1,113,'2015-06-17 09:58:30','2015-06-17 09:58:30'),(29,NULL,1,113,'2015-06-17 10:14:39','2015-06-17 10:14:39'),(30,NULL,1,113,'2015-06-17 10:14:40','2015-06-17 10:14:40'),(31,4,NULL,105,'2015-06-21 21:42:53','2015-06-21 21:47:37'),(32,NULL,1,105,'2015-06-21 21:42:57','2015-06-21 21:42:57'),(33,1,NULL,108,'2015-06-21 21:43:14','2015-06-21 21:43:14'),(34,NULL,1,108,'2015-06-21 21:43:15','2015-06-21 21:43:15'),(35,NULL,1,105,'2015-06-21 21:47:38','2015-06-21 21:47:38');
/*!40000 ALTER TABLE `rating_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recentviews`
--

DROP TABLE IF EXISTS `recentviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recentviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `photographer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `recentviews_user_id_foreign` (`user_id`),
  KEY `recentviews_photographer_id_foreign` (`photographer_id`),
  CONSTRAINT `recentviews_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `recentviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recentviews`
--

LOCK TABLES `recentviews` WRITE;
/*!40000 ALTER TABLE `recentviews` DISABLE KEYS */;
INSERT INTO `recentviews` VALUES (1,104,30,'2015-06-14 20:40:48','2015-06-14 20:40:48'),(2,104,30,'2015-06-14 20:40:48','2015-06-14 20:40:48'),(3,104,30,'2015-06-14 20:40:48','2015-06-14 20:40:48'),(4,104,80,'2015-06-14 20:45:02','2015-06-14 20:45:02'),(5,104,80,'2015-06-14 20:45:03','2015-06-14 20:45:03'),(6,104,80,'2015-06-14 20:45:03','2015-06-14 20:45:03'),(7,104,80,'2015-06-14 20:45:18','2015-06-14 20:45:18'),(8,104,80,'2015-06-14 20:45:18','2015-06-14 20:45:18'),(9,104,80,'2015-06-14 20:45:18','2015-06-14 20:45:18'),(10,104,81,'2015-06-14 21:15:13','2015-06-14 21:15:13'),(11,104,81,'2015-06-14 21:15:13','2015-06-14 21:15:13'),(12,104,81,'2015-06-14 21:15:14','2015-06-14 21:15:14'),(13,104,81,'2015-06-14 21:15:21','2015-06-14 21:15:21'),(14,104,81,'2015-06-14 21:15:21','2015-06-14 21:15:21'),(15,104,81,'2015-06-14 21:15:21','2015-06-14 21:15:21'),(16,104,81,'2015-06-14 21:15:40','2015-06-14 21:15:40'),(17,104,81,'2015-06-14 21:15:40','2015-06-14 21:15:40'),(18,104,81,'2015-06-14 21:15:41','2015-06-14 21:15:41'),(19,122,80,'2015-06-15 13:37:15','2015-06-15 13:37:15'),(20,122,80,'2015-06-15 13:37:15','2015-06-15 13:37:15'),(21,122,121,'2015-06-15 13:37:20','2015-06-15 13:37:20'),(22,122,121,'2015-06-15 13:37:20','2015-06-15 13:37:20'),(23,26,30,'2015-06-15 13:43:08','2015-06-15 13:43:08'),(24,26,30,'2015-06-15 13:43:09','2015-06-15 13:43:09'),(25,26,30,'2015-06-15 13:43:12','2015-06-15 13:43:12'),(26,26,95,'2015-06-15 13:43:21','2015-06-15 13:43:21'),(27,26,95,'2015-06-15 13:43:22','2015-06-15 13:43:22'),(28,26,95,'2015-06-15 13:43:22','2015-06-15 13:43:22'),(29,122,121,'2015-06-15 13:46:41','2015-06-15 13:46:41'),(30,122,80,'2015-06-15 13:46:47','2015-06-15 13:46:47'),(31,122,25,'2015-06-15 14:31:12','2015-06-15 14:31:12'),(36,122,128,'2015-06-15 14:51:58','2015-06-15 14:51:58'),(37,143,140,'2015-06-15 14:55:33','2015-06-15 14:55:33'),(38,143,140,'2015-06-15 14:55:34','2015-06-15 14:55:34'),(39,145,140,'2015-06-15 15:10:31','2015-06-15 15:10:31'),(40,145,140,'2015-06-15 15:10:33','2015-06-15 15:10:33'),(41,148,25,'2015-06-15 15:18:52','2015-06-15 15:18:52'),(42,148,25,'2015-06-15 15:18:53','2015-06-15 15:18:53'),(43,122,80,'2015-06-15 15:25:44','2015-06-15 15:25:44'),(44,122,95,'2015-06-15 15:25:46','2015-06-15 15:25:46'),(45,122,95,'2015-06-15 15:27:05','2015-06-15 15:27:05'),(46,122,81,'2015-06-15 15:28:11','2015-06-15 15:28:11'),(47,122,79,'2015-06-15 15:31:46','2015-06-15 15:31:46'),(48,122,81,'2015-06-15 15:43:37','2015-06-15 15:43:37'),(49,26,30,'2015-06-15 16:54:29','2015-06-15 16:54:29'),(50,26,30,'2015-06-15 16:54:32','2015-06-15 16:54:32'),(51,26,80,'2015-06-15 17:18:35','2015-06-15 17:18:35'),(52,26,80,'2015-06-15 17:18:36','2015-06-15 17:18:36'),(53,26,80,'2015-06-15 17:19:59','2015-06-15 17:19:59'),(54,26,80,'2015-06-15 17:20:00','2015-06-15 17:20:00'),(55,26,80,'2015-06-15 17:26:32','2015-06-15 17:26:32'),(56,26,80,'2015-06-15 17:26:35','2015-06-15 17:26:35'),(57,26,25,'2015-06-16 14:20:11','2015-06-16 14:20:11'),(58,26,25,'2015-06-16 14:20:12','2015-06-16 14:20:12'),(59,26,25,'2015-06-16 14:20:13','2015-06-16 14:20:13'),(60,26,25,'2015-06-16 14:20:14','2015-06-16 14:20:14'),(61,26,25,'2015-06-16 14:22:19','2015-06-16 14:22:19'),(62,26,25,'2015-06-16 14:22:20','2015-06-16 14:22:20'),(63,26,80,'2015-06-16 14:33:29','2015-06-16 14:33:29'),(64,26,80,'2015-06-16 14:33:29','2015-06-16 14:33:29'),(65,26,80,'2015-06-16 14:33:29','2015-06-16 14:33:29'),(66,26,80,'2015-06-16 14:33:30','2015-06-16 14:33:30'),(67,26,80,'2015-06-16 14:34:43','2015-06-16 14:34:43'),(68,26,25,'2015-06-16 14:35:55','2015-06-16 14:35:55'),(69,26,25,'2015-06-16 14:36:28','2015-06-16 14:36:28'),(70,26,80,'2015-06-16 14:37:55','2015-06-16 14:37:55'),(71,26,80,'2015-06-16 14:39:05','2015-06-16 14:39:05'),(72,26,97,'2015-06-16 14:41:46','2015-06-16 14:41:46'),(73,26,97,'2015-06-16 14:41:46','2015-06-16 14:41:46'),(74,26,97,'2015-06-16 14:41:47','2015-06-16 14:41:47'),(75,26,97,'2015-06-16 14:41:50','2015-06-16 14:41:50'),(76,26,81,'2015-06-16 14:45:21','2015-06-16 14:45:21'),(77,26,81,'2015-06-16 14:45:22','2015-06-16 14:45:22'),(78,26,81,'2015-06-16 14:45:22','2015-06-16 14:45:22'),(79,26,157,'2015-06-16 17:00:45','2015-06-16 17:00:45'),(80,26,157,'2015-06-16 17:00:46','2015-06-16 17:00:46'),(81,26,157,'2015-06-16 17:00:47','2015-06-16 17:00:47'),(82,122,79,'2015-06-17 09:24:34','2015-06-17 09:24:34'),(83,122,79,'2015-06-17 09:24:34','2015-06-17 09:24:34'),(84,122,79,'2015-06-17 09:24:35','2015-06-17 09:24:35'),(85,122,79,'2015-06-17 09:25:23','2015-06-17 09:25:23'),(86,97,79,'2015-06-17 09:25:50','2015-06-17 09:25:50'),(87,122,80,'2015-06-17 09:27:26','2015-06-17 09:27:26'),(88,122,80,'2015-06-17 09:27:27','2015-06-17 09:27:27'),(89,122,80,'2015-06-17 09:27:28','2015-06-17 09:27:28'),(90,122,80,'2015-06-17 09:28:30','2015-06-17 09:28:30'),(91,97,80,'2015-06-17 09:30:35','2015-06-17 09:30:35'),(92,26,157,'2015-06-17 09:54:53','2015-06-17 09:54:53'),(93,26,157,'2015-06-17 09:54:54','2015-06-17 09:54:54'),(94,26,157,'2015-06-17 09:54:54','2015-06-17 09:54:54'),(95,26,157,'2015-06-17 09:55:02','2015-06-17 09:55:02'),(96,26,157,'2015-06-17 09:55:27','2015-06-17 09:55:27'),(97,156,157,'2015-06-17 09:57:24','2015-06-17 09:57:24'),(98,156,157,'2015-06-17 09:58:17','2015-06-17 09:58:17'),(99,26,157,'2015-06-17 10:03:56','2015-06-17 10:03:56'),(100,156,157,'2015-06-17 10:14:31','2015-06-17 10:14:31'),(101,26,157,'2015-06-17 10:21:29','2015-06-17 10:21:29'),(102,26,157,'2015-06-17 10:22:55','2015-06-17 10:22:55'),(103,26,157,'2015-06-17 10:23:08','2015-06-17 10:23:08'),(104,26,157,'2015-06-17 10:27:10','2015-06-17 10:27:10'),(105,140,80,'2015-06-17 14:31:20','2015-06-17 14:31:20'),(106,140,80,'2015-06-17 14:31:35','2015-06-17 14:31:35'),(107,140,79,'2015-06-17 15:23:05','2015-06-17 15:23:05'),(108,140,80,'2015-06-17 15:23:11','2015-06-17 15:23:11'),(109,140,123,'2015-06-17 15:23:16','2015-06-17 15:23:16'),(110,140,123,'2015-06-17 15:23:16','2015-06-17 15:23:16'),(111,140,123,'2015-06-17 15:23:17','2015-06-17 15:23:17'),(112,140,81,'2015-06-17 15:31:25','2015-06-17 15:31:25'),(113,140,81,'2015-06-17 15:31:46','2015-06-17 15:31:46'),(114,140,81,'2015-06-17 15:47:29','2015-06-17 15:47:29'),(115,140,81,'2015-06-17 15:47:36','2015-06-17 15:47:36'),(116,104,80,'2015-06-17 17:33:56','2015-06-17 17:33:56'),(117,104,80,'2015-06-17 17:33:56','2015-06-17 17:33:56'),(118,26,157,'2015-06-17 17:43:17','2015-06-17 17:43:17'),(119,122,80,'2015-06-19 11:40:52','2015-06-19 11:40:52'),(120,122,80,'2015-06-19 11:40:54','2015-06-19 11:40:54'),(121,122,80,'2015-06-19 11:40:54','2015-06-19 11:40:54'),(122,26,157,'2015-06-19 17:20:30','2015-06-19 17:20:30'),(123,26,140,'2015-06-19 17:21:09','2015-06-19 17:21:09'),(124,26,140,'2015-06-19 17:21:09','2015-06-19 17:21:09'),(125,26,140,'2015-06-19 17:21:09','2015-06-19 17:21:09'),(126,104,79,'2015-06-21 06:17:04','2015-06-21 06:17:04'),(127,104,79,'2015-06-21 06:17:04','2015-06-21 06:17:04'),(128,104,150,'2015-06-21 06:17:14','2015-06-21 06:17:14'),(129,104,150,'2015-06-21 06:17:15','2015-06-21 06:17:15'),(130,104,79,'2015-06-21 06:19:44','2015-06-21 06:19:44'),(131,104,79,'2015-06-21 06:19:45','2015-06-21 06:19:45'),(132,104,132,'2015-06-21 06:34:55','2015-06-21 06:34:55'),(133,104,132,'2015-06-21 06:34:55','2015-06-21 06:34:55'),(134,104,81,'2015-06-21 06:35:28','2015-06-21 06:35:28'),(135,104,81,'2015-06-21 06:35:28','2015-06-21 06:35:28'),(136,104,25,'2015-06-21 06:39:04','2015-06-21 06:39:04'),(137,104,25,'2015-06-21 06:39:04','2015-06-21 06:39:04'),(138,26,79,'2015-06-21 21:36:09','2015-06-21 21:36:09'),(139,26,79,'2015-06-21 21:36:10','2015-06-21 21:36:10'),(140,26,79,'2015-06-21 21:36:21','2015-06-21 21:36:21'),(141,26,79,'2015-06-21 21:36:22','2015-06-21 21:36:22'),(142,104,81,'2015-06-21 21:37:15','2015-06-21 21:37:15'),(143,104,81,'2015-06-21 21:37:15','2015-06-21 21:37:15'),(144,104,81,'2015-06-21 21:41:37','2015-06-21 21:41:37'),(145,104,81,'2015-06-21 21:41:38','2015-06-21 21:41:38'),(146,104,79,'2015-06-21 21:41:40','2015-06-21 21:41:40'),(147,104,79,'2015-06-21 21:41:41','2015-06-21 21:41:41'),(148,104,79,'2015-06-21 21:47:54','2015-06-21 21:47:54'),(149,104,79,'2015-06-21 21:47:54','2015-06-21 21:47:54'),(150,104,79,'2015-06-21 21:48:15','2015-06-21 21:48:15'),(151,104,79,'2015-06-21 21:48:16','2015-06-21 21:48:16'),(152,104,79,'2015-06-21 21:51:47','2015-06-21 21:51:47'),(153,104,79,'2015-06-21 21:51:51','2015-06-21 21:51:51'),(154,104,147,'2015-06-21 21:52:11','2015-06-21 21:52:11'),(155,104,147,'2015-06-21 21:52:11','2015-06-21 21:52:11'),(156,104,121,'2015-06-21 21:52:17','2015-06-21 21:52:17'),(157,104,121,'2015-06-21 21:52:17','2015-06-21 21:52:17'),(158,104,79,'2015-06-21 21:59:10','2015-06-21 21:59:10'),(159,104,79,'2015-06-21 21:59:10','2015-06-21 21:59:10'),(160,140,81,'2015-06-22 06:56:10','2015-06-22 06:56:10'),(161,140,81,'2015-06-22 06:56:11','2015-06-22 06:56:11');
/*!40000 ALTER TABLE `recentviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stars` int(11) DEFAULT NULL,
  `review` text COLLATE utf8_unicode_ci,
  `photographer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `reviews_photographer_id_foreign` (`photographer_id`),
  KEY `reviews_user_id_foreign` (`user_id`),
  CONSTRAINT `reviews_photographer_id_foreign` FOREIGN KEY (`photographer_id`) REFERENCES `photographers` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (6,'testedtested',72,'test',1,'tested',25,'2015-05-28 18:03:30','2015-05-28 18:03:30'),(8,'bad ',95,'tested',1,'bad',85,'2015-06-01 11:10:04','2015-06-01 11:10:04'),(11,'testedtested',80,'Chris Morris',1,'testedtested',81,'2015-06-02 12:05:39','2015-06-02 12:05:39'),(12,'good one good one',97,'test',1,'good one........',80,'2015-06-02 12:25:07','2015-06-02 12:25:07'),(14,'Test',101,'somil mishra',5,'This is review for the purpose of testing.',80,'2015-06-02 17:53:54','2015-06-02 17:53:54'),(15,'Great photographer',104,'Mark Lee',5,'This is a test review',25,'2015-06-03 05:30:17','2015-06-03 05:30:17'),(16,'kldjf slkdjf lskdj f',26,'gaurav',3,'lksjd flksjd f',30,'2015-06-09 05:59:10','2015-06-09 05:59:10'),(17,'kldjf slkdjf lskdj f',26,'gaurav',3,'lksjd flksjd f',30,'2015-06-09 05:59:13','2015-06-09 05:59:13'),(18,'kldjf slkdjf lskdj f',26,'gaurav',3,'lksjd flksjd f',30,'2015-06-09 05:59:15','2015-06-09 05:59:15'),(102,'this is a test review from developer',26,'gaurav',5,'lkhdkfhjsdkjfhsdkfhkjh',80,'2015-06-15 17:26:51','2015-06-15 17:26:51'),(103,'Lorem Ipsum is simply dummy text of the printing and typesetting',26,'gaurav',2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',25,'2015-06-16 14:20:37','2015-06-16 14:20:37'),(104,'Lorem Ipsum is simply dummy text of the printing and typesetting',26,'gaurav',2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',25,'2015-06-16 14:20:38','2015-06-16 14:20:38'),(105,'testing demo',122,'Neha 2',1,'bad bad bad bad',79,'2015-06-17 09:24:56','2015-06-17 09:24:56'),(106,'testing demo',122,'Neha 2',1,'bad bad bad bad',79,'2015-06-17 09:25:00','2015-06-17 09:25:00'),(107,'testing demo',122,'Neha 2',1,'bad bad bad bad',79,'2015-06-17 09:25:01','2015-06-17 09:25:01'),(108,'testing demo',122,'Neha 2',1,'bad bad bad bad',79,'2015-06-17 09:25:08','2015-06-17 09:25:08'),(109,'testing demo',122,'Neha 2',1,'bad bad bad bad',79,'2015-06-17 09:25:08','2015-06-17 09:25:08'),(110,'testing demo',122,'Neha 2',1,'bad bad bad bad',79,'2015-06-17 09:25:08','2015-06-17 09:25:08'),(111,'testing................',122,'Neha 2',1,'good one good one',80,'2015-06-17 09:28:07','2015-06-17 09:28:07'),(112,'The standard chunk ',26,'gaurav',4,'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',157,'2015-06-17 09:55:20','2015-06-17 09:55:20'),(113,'Donate',156,'hello app',3,'If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.',157,'2015-06-17 09:58:06','2015-06-17 09:58:06'),(114,'abcd',140,'web app',3,'hello nice 1',81,'2015-06-17 15:31:44','2015-06-17 15:31:44'),(115,'Great photos',104,'Mark Lee',4,'he came he saw he conquered',79,'2015-06-21 21:48:07','2015-06-21 21:48:07'),(116,'Great photos',104,'Mark Lee',4,'he came he saw he conquered',79,'2015-06-21 21:48:08','2015-06-21 21:48:08');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `roles_user_id_foreign` (`user_id`),
  CONSTRAINT `roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (20,25,'admin','2015-05-22 17:53:40','2015-05-22 17:53:40'),(21,26,'admin','2015-05-25 10:55:40','2015-05-25 10:55:40'),(22,27,'user','2015-05-25 11:18:18','2015-05-25 11:18:18'),(25,30,'photographer','2015-05-26 11:37:29','2015-05-26 11:37:29'),(33,40,'user','2015-05-27 15:58:53','2015-05-27 15:58:53'),(34,41,'user','2015-05-27 15:59:37','2015-05-27 15:59:37'),(35,42,'admin','2015-05-27 16:00:19','2015-05-27 16:00:19'),(36,43,'user','2015-05-27 16:06:04','2015-05-27 16:06:04'),(40,60,'user','2015-05-27 18:08:48','2015-05-27 18:08:48'),(41,61,'user','2015-05-27 18:35:07','2015-05-27 18:35:07'),(42,66,'photographer','2015-05-27 18:41:00','2015-05-27 18:41:00'),(43,68,'photographer','2015-05-27 18:44:50','2015-05-27 18:44:50'),(44,69,'user','2015-05-27 18:50:25','2015-05-27 18:50:25'),(45,70,'photographer','2015-05-27 18:51:02','2015-05-27 18:51:02'),(46,71,'user','2015-05-28 17:28:31','2015-05-28 17:28:31'),(47,72,'user','2015-05-28 17:30:01','2015-05-28 17:30:01'),(50,76,'photographer','2015-05-28 17:57:03','2015-05-28 17:57:03'),(51,77,'photographer','2015-05-29 17:49:32','2015-05-29 17:49:32'),(52,78,'user','2015-05-29 17:55:15','2015-05-29 17:55:15'),(53,79,'photographer','2015-05-29 18:14:23','2015-05-29 18:14:23'),(54,80,'photographer','2015-05-29 18:27:12','2015-05-29 18:27:12'),(55,81,'photographer','2015-05-29 18:31:05','2015-05-29 18:31:05'),(56,82,'photographer','2015-05-29 18:31:56','2015-05-29 18:31:56'),(57,83,'photographer','2015-05-29 18:33:03','2015-05-29 18:33:03'),(58,84,'photographer','2015-05-29 18:35:25','2015-05-29 18:35:25'),(59,85,'photographer','2015-05-29 18:37:33','2015-05-29 18:37:33'),(60,88,'photographer','2015-05-29 18:39:00','2015-05-29 18:39:00'),(62,90,'photographer','2015-06-01 09:59:32','2015-06-01 09:59:32'),(63,91,'user','2015-06-01 10:37:55','2015-06-01 10:37:55'),(65,93,'photographer','2015-06-01 10:42:54','2015-06-01 10:42:54'),(66,94,'photographer','2015-06-01 10:50:25','2015-06-01 10:50:25'),(67,95,'photographer','2015-06-01 11:08:44','2015-06-01 11:08:44'),(68,96,'user','2015-06-02 12:07:57','2015-06-02 12:07:57'),(69,97,'photographer','2015-06-02 12:10:12','2015-06-02 12:10:12'),(70,98,'user','2015-06-02 12:10:25','2015-06-02 12:10:25'),(71,99,'user','2015-06-02 12:13:34','2015-06-02 12:13:34'),(72,100,'user','2015-06-02 12:36:00','2015-06-02 12:36:00'),(73,101,'user','2015-06-02 12:38:29','2015-06-02 12:38:29'),(74,102,'photographer','2015-06-02 13:21:58','2015-06-02 13:21:58'),(75,103,'photographer','2015-06-02 15:59:51','2015-06-02 15:59:51'),(76,104,'user','2015-06-03 00:02:23','2015-06-03 00:02:23'),(77,105,'photographer','2015-06-03 05:50:28','2015-06-03 05:50:28'),(79,107,'photographer','2015-06-03 18:39:39','2015-06-03 18:39:39'),(80,108,'photographer','2015-06-03 18:57:24','2015-06-03 18:57:24'),(81,109,'photographer','2015-06-08 04:56:10','2015-06-08 04:56:10'),(82,110,'photographer','2015-06-08 04:56:45','2015-06-08 04:56:45'),(83,111,'photographer','2015-06-08 04:57:18','2015-06-08 04:57:18'),(84,112,'user','2015-06-09 13:17:23','2015-06-09 13:17:23'),(86,118,'user','2015-06-09 14:12:28','2015-06-09 14:12:28'),(89,121,'photographer','2015-06-09 14:51:02','2015-06-09 14:51:02'),(90,122,'user','2015-06-09 15:37:14','2015-06-09 15:37:14'),(91,123,'photographer','2015-06-09 15:43:05','2015-06-09 15:43:05'),(92,124,'photographer','2015-06-09 15:46:47','2015-06-09 15:46:47'),(93,125,'photographer','2015-06-09 16:44:51','2015-06-09 16:44:51'),(94,126,'user','2015-06-10 16:46:16','2015-06-10 16:46:16'),(96,128,'photographer','2015-06-10 16:56:17','2015-06-10 16:56:17'),(97,129,'photographer','2015-06-10 17:37:50','2015-06-10 17:37:50'),(100,132,'photographer','2015-06-11 16:01:36','2015-06-11 16:01:36'),(101,133,'photographer','2015-06-11 16:20:51','2015-06-11 16:20:51'),(103,135,'photographer','2015-06-11 16:23:27','2015-06-11 16:23:27'),(104,137,'photographer','2015-06-11 16:44:27','2015-06-11 16:44:27'),(105,139,'photographer','2015-06-11 18:20:55','2015-06-11 18:20:55'),(106,140,'photographer','2015-06-12 12:15:50','2015-06-12 12:15:50'),(107,141,'photographer','2015-06-12 17:46:04','2015-06-12 17:46:04'),(108,142,'photographer','2015-06-14 16:35:57','2015-06-14 16:35:57'),(109,143,'user','2015-06-15 14:54:37','2015-06-15 14:54:37'),(110,144,'photographer','2015-06-15 15:00:25','2015-06-15 15:00:25'),(111,145,'photographer','2015-06-15 15:02:42','2015-06-15 15:02:42'),(112,146,'photographer','2015-06-15 15:08:39','2015-06-15 15:08:39'),(113,147,'photographer','2015-06-15 15:13:15','2015-06-15 15:13:15'),(114,148,'user','2015-06-15 15:18:41','2015-06-15 15:18:41'),(115,149,'photographer','2015-06-15 15:28:31','2015-06-15 15:28:31'),(116,150,'photographer','2015-06-15 15:36:55','2015-06-15 15:36:55'),(117,152,'user','2015-06-15 16:13:33','2015-06-15 16:13:33'),(118,153,'user','2015-06-15 16:17:55','2015-06-15 16:17:55'),(120,155,'user','2015-06-16 11:16:14','2015-06-16 11:16:14'),(121,156,'photographer','2015-06-16 11:38:16','2015-06-16 11:38:16'),(122,157,'photographer','2015-06-16 17:00:27','2015-06-16 17:00:27'),(123,158,'user','2015-06-20 11:44:56','2015-06-20 11:44:56');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'1 Event Per Day','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'2nd Shooter','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Add\'l Hours','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Custom Graphics','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'High-Res','0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Engagement Shoot','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,'Multi. Locations','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Slideshow','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Liability Insurance','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Boudoir Shoot','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Hand-Coloring','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'Toning','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Trash The Dress','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'/images/slider/slide1.jpg','Yes','2015-05-26 03:30:00','2015-05-27 03:27:39'),(2,'/images/slider/slide2.jpg','Yes','2015-05-26 03:30:00','2015-05-27 03:28:56'),(4,'/images/slider/3u6oZOgahiqA.jpg','Yes','2015-05-27 04:17:22','2015-05-27 04:22:19'),(5,'/images/slider/phzXzWZXbG5z.jpg','Yes','2015-05-27 13:21:34','2015-05-27 13:21:38');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'Contemporary','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Photojournalism','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Portraiture','0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Traditional','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Photography services','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (25,'Matthew Charles','administrator@yopmail.com','$2y$10$W5W8HwPUgatr2aHjpsLrBeBuG4/w5bi4A00pOV.edvHi4M0Sa0uFS','1','0ZvGHWGnNTEb8wuayg6o0HVvQkmgc72SnZ4LhVpK4sWKw8sh9G1xxLSfKepz','2015-05-22 17:53:40','2015-05-27 16:03:20'),(26,'gaurav','admin23@yopmail.com','$2y$10$LRUq2VReNMdHJ4yeYkrjyO3xeG8fB7hw8JsCF9dhnnbOzocu5C3ma','1','QfW2NqZkHiQJOw5r3YkQX8uLPi6Cn6ERzfAK13eYRtNqnJz7IIAUq8E2MFwF','2015-05-25 10:55:40','2015-06-21 22:36:02'),(27,'gaurav1','gaurav1@yopmail.com','$2y$10$3PVLPsLp14w5NFeLpFzHhuQh0/Tko3mydMMrHkNgEPZl205p6ik/K','1','sgwI43ZgBCPtuvj3JvvtupWppE7TyQ5r2yqOH2Z8CoxVnuRhPbQPEU0XFw8e','2015-05-25 11:18:18','2015-05-25 11:20:13'),(30,'Steve Smith','smishra2+1@myzealit.com','$2y$10$40u0TEGfL7se3bcqvLabne7dlIuD8X7hRiN/2kR9gTplF6aSoA/Xm','0','t5QTW4xywv54TEQQ8zuQUpm0Q5MotWuSNYMXek4vF6ziFzZ2jiFv5Olyr0ld','2015-05-26 11:37:29','2015-05-26 15:08:07'),(40,'test','gaurav123@yopmail.com','$2y$10$owYJbCPmRsqmZSue7URjVeFsquiVHD97X7p2GvaPNt1c46gPZUkua','1','noZYd7Lx60xQbC1pHKggEcOr3MrCeSqxuE0KTYP3iztOf0IUaYe9lZALCl1X','2015-05-27 15:58:53','2015-05-27 15:59:15'),(41,'test','gaurav1234@yopmail.com','$2y$10$omZhIdIVD4sRkLkOYqN.qeqnalN2CiPCMMyHD7HMATwGU06Cw196O','0','MwjHSjcfmLeYZjuuApaG294DUnr8X72E9HCJIR6OS77L3VmCdlgrzUTyNgQh','2015-05-27 15:59:37','2015-05-27 15:59:56'),(42,'test','gaurav5@yopmail.com','$2y$10$VtiSjN9g/7dV4x9C0f.mT.Z0x7Y9LAntrE6leiu.dLd.veZ.fS.Z2','1','gspbqBx0kZB1YhzfsR3lLvX9ul1YmS7n04NSHHnZ2oK3M6b0bQ4OdWAnVVM4','2015-05-27 16:00:19','2015-05-27 18:15:03'),(43,'','','$2y$10$TYUrXRasKtbynCmfMk7vBOxiM2UjtXHgWL14CqK8yYfKV/YLwcBiK','0','SpR02nBM3fOqWZhhH5xqP6t54jPNx7uhiCxZ9leT11h1oZo05P2UQ9snPWuA','2015-05-27 16:06:04','2015-06-21 22:11:09'),(60,'test','testttt@yopmail.com','$2y$10$gSzL3udlFtuZJpsM.xO.zuUXbD4911IjfDq/RQAcxFhVVxjR8iaTS','1','ovdoXvvhxzAAaA4T3sCd5iTszOI5Emoqt9FcC2HZuyObWLiamQzSjpCqhldR','2015-05-27 18:08:48','2015-05-27 18:35:37'),(61,'tested  ','tested@yopmail.com','$2y$10$N2d.DDAvV61wQfN3uf0MUen2Tj7pJSbJ8rjsDkn/HW9mOp9VgCSza','1','4BuUcNE5k3BX0UaD1MV03rJCiAtOPAcGUmaIJ0kypoRy44lwrtNurkQBIkMX','2015-05-27 18:35:07','2015-05-27 18:38:43'),(66,'test test','testted@yopmail.com','$2y$10$JcsHDIaVjUUfjyeEgxlKOu11WzBKK6qM.uqwOfEyARu9SwuSFaZuW','1','N3WYIVWblA1YmHRkwxWXQjgYfl2veVApnRx1Z7LxctPyWLextxEGHvEC4VbW','2015-05-27 18:41:00','2015-06-10 17:06:04'),(68,'test test','tttested@yopmail.com','$2y$10$OcXTDq9TPVNkIf0ZB2WdtuN5MkQyNSgsSPyzsXrSPW9hjnfabjNuG','0','a3YeFbfZwXG9hYDXsbmjMEnZsOlIxPKt14hbr7BJyBmG51VbRGGzj3EdsfXI','2015-05-27 18:44:50','2015-05-27 18:47:56'),(69,'test','atested@yopmail.com','$2y$10$B9JPNy7L8XqT/47Y6b8/keMH3wW08sxoyDuASm.feCO7Ldzgqsflq','0','zZU4arXjRO5ozstW39fFoFZuEMF2gfO91E4IUs5dZkfDX43e8LLCeYtfWFNy','2015-05-27 18:50:25','2015-05-27 18:50:34'),(70,'atested atested','aatested@yopmail.com','$2y$10$YzCRo8fB2Btf791DT1sYNelg.RY6Wc0FlI/senrGiBJTOmFf.O0ga','0',NULL,'2015-05-27 18:51:02','2015-05-27 18:51:02'),(71,'test','test121@yopmail.com','$2y$10$yfYOXLvOHC/XrUtsLmc0HO9nbcUdYirMf2jZok5/dMKiHdgpUCIzi','0','pReVDETLde62GbJC0gecrdSvFk7W1c9YtggsEMOyGmcLCBkDzFFErgGTtCp0','2015-05-28 17:28:31','2015-05-28 17:29:04'),(72,'test','test000@yopmail.com','$2y$10$.9KxDxGS/az3fHkXyiEpOeY5nOwBLUH9jfjCZ5pCAB5CcJ.ErhoPK','1','5eyx6LH6R2u5nfLXRPLFMv5LEKlRp5VBLSO9YhQRit9sNSkr896Z7rGw8MVf','2015-05-28 17:30:01','2015-05-28 18:10:15'),(76,'ttttttt','a152@yopmail.com','$2y$10$Nx3tSjgh6f.f54Ef..bFMeWSOsHCOa1K8Jg3qBX8uFmpFskwQOxR2','0',NULL,'2015-05-28 17:57:03','2015-05-28 17:57:03'),(77,'aaaaaa aaaaaa','aaaaaa@yopmail.com','$2y$10$UhdFohtHY3l6kgOeBH48put2VoBjUHcYHWgDCKk84b2zsM.d8Sxa2','0','cy9yG7nt2U3aDj7gXFC5Bu0tR80sFA19Z77fJZpFBe0Y3bqpojypCJVAmgRc','2015-05-29 17:49:32','2015-05-29 17:53:53'),(78,'test','aaa@yopmail.com','$2y$10$wCa.miMkr3ufmoegJXrMKOXdliA/fMqNh1sEqVlJBwW1z1cXJ/7Ve','1','SnCdApgrsUB7ubtj4cRHzjy3poN4jlOFElSEcucR1orlYGUeH30ooCmB1s78','2015-05-29 17:55:15','2015-05-29 18:02:44'),(79,'John David','David@yopmail.com','$2y$10$5RezjTq2btUm5KSDOmFDZO7tKXKtm0OG6HKW70Dl4Mj7MjzXQQ6lW','0',NULL,'2015-05-29 18:14:23','2015-05-29 18:14:23'),(80,'Chris Morris','morris@yopmail.com','$2y$10$LfI7Z3BoCQyVgy1c8CgvX.OH.4/Vu3q2lVph21KmYH3tdl3Zmk8lC','1','KeXDy2mYMumfJHmaoG9Cq1pCItjhJ45c1qnn0b2zpm8botNCZYgPs1uvD929','2015-05-29 18:27:12','2015-06-02 12:10:09'),(81,'Charles David','charles@yopmail.com','$2y$10$S9MbYS2PEj00sVSzfkscMuNZwibIygLcljWkIQG0.1CCo2h7imMJm','0',NULL,'2015-05-29 18:31:05','2015-05-29 18:31:05'),(82,'Brad Hogg','brad@yopmail.com','$2y$10$QSLmzdVYTbf4n8aZJ5/7juYhXWLe7PDqnUGDseWkTYBYKVGwo2QQG','0',NULL,'2015-05-29 18:31:56','2015-05-29 18:31:56'),(83,'Nathen Coulter','nathen@yopmail.com','$2y$10$bTV5yyxZDPJwrckdOKVGzuu0hz6wmlDdW.aIiOfNFNS6vzDd7sdS2','0',NULL,'2015-05-29 18:33:03','2015-05-29 18:33:03'),(84,'David Villa','aaaa@yopmail.com','$2y$10$LlBgdEL9zByVpVt42QfDX.qnvlufq6rFbfThnvc8rQlGEN6YkO19S','0',NULL,'2015-05-29 18:35:24','2015-05-29 18:35:24'),(85,'Ryan Ten','aaaaa@yopmail.com','$2y$10$xbA0YzWVguWiDPO1ngPeB.7fhp.Q1xUI4Bq/gm/bZfHQWlX.MrZiW','0',NULL,'2015-05-29 18:37:33','2015-05-29 18:37:33'),(88,'Andrew  Rebet','saaa@yopmail.com','$2y$10$B7zzOuIqlkh1VR0eSyqbw.mynoBKwnDVcjT2e1M2YyAfPtfajjqAm','0',NULL,'2015-05-29 18:39:00','2015-05-29 18:39:00'),(90,'122321 nh514524','soni1234@yopmail.com','$2y$10$5qQ5nw363drPGddDqQ2uAONvhKi3KlY.SE9x.4DHooj6JwgN0qb3m','1','AL59tO9LowhGqUe2znsYmiz53U2mq5y5MjiDHWZsauUgqvOh29lhM05JXEta','2015-06-01 09:59:31','2015-06-02 12:29:32'),(91,'Marcus ','m@yopmail.com','$2y$10$3wJvwUfY1vX7Mg3lwkvUiu6LjcQKNvsk8m78qIJ8HbxVOSv4d097y','1','xExc2AUodJPwHYoCFiUUYfglD5QX2kWtXZsKlEirsxDz1aZotup2JXU7Fzq4','2015-06-01 10:37:55','2015-06-02 11:46:30'),(93,'asfasfasf asfasfasfasfasfas','ase@yopmail.com','$2y$10$9kgNZBEsOg0iC.fH5YwZGe6gAHEcUM0B4QlBs6mxMrabQjDJvlkw6','0','vTSsfORwYwCPV7PugK2palP31oGCOJSIYoiJx2v5cNR4YgDDEPIlFiwfLcVv','2015-06-01 10:42:54','2015-06-01 10:43:37'),(94,'David Miller','d@yopmail.com','$2y$10$Qk2x2BN1xM8wHCrHWs5K2eGY.e7wx5dcVoRqdgZuVP6vA7KBUmmtq','0','OQqvJ9aNNiDvPISSvrW7prKUSiGQNMyuhXfOqNvf0poGtX6aftAl51otlpLA','2015-06-01 10:50:25','2015-06-01 10:53:09'),(95,'tested','saa@yopmail.com','$2y$10$dkaQU73LuZdJvX81VsskyOSWLxl0KFlm4bNPfGIRX8uZu3PJCfSVe','1','ZmBn9SMTBxM9S0Dju8Qxhhpa2yDSUFcLxNHKzIKNinD5zV1ls16dbwk4m3LH','2015-06-01 11:08:44','2015-06-01 11:15:26'),(96,'somil mishra','somil_mishra+1@ymail.com','$2y$10$o9xd9jjXo12Lqd9tzG4HjuCJBpBLzuvMiA.2Wp0Cle2fbzwHKmHuy','0','jXi2PwZhbVAX0eSRRmWZG2Zs8csluzNyQC6MQ7NpT9CHjKmRXv6NLxQowckT','2015-06-02 12:07:57','2015-06-02 12:37:37'),(97,'test','viky12@yopmail.com','$2y$10$AnLEpdhizO2cuN7sUTSt/.P9jp9OY7nVhxVZifBOzMog9guaLC42m','1','AWwMGYF2sthXzC8Dz2yEtKyPK9EGMG3n7vu7RFRQIFdHuQw2mRJDuP1a2SB7','2015-06-02 12:10:12','2015-06-19 11:42:17'),(98,'testttt','aas@yopmail.com','$2y$10$Zoha7gXmeGbZp2EdI4ULEu/FUwKUz6XFjHLnK6BfkZRH7esYGoFxS','0','ct6MRboEEBeo1MC2Y5lXUvzmLLfORAnYU12yHJcWIdyJcg8ogMQkpAK5yyKK','2015-06-02 12:10:25','2015-06-02 12:12:56'),(99,'testtttgg','s@yopmail.com','$2y$10$WRAkQgwvV672cb8iFfkjpueZkgW6oD/gUZ.DU8rxahM9HBMQhRZo6','1','VNqRKUU5CPhfwua6ismoBL58YaOeKzifM5Zm7HM9LacqizsJfyGDJBEssDox','2015-06-02 12:13:34','2015-06-02 12:13:51'),(100,'shipra','shipra2@yopmail.com','$2y$10$oO0T1giVMGUDoIt/pwN4COfS3o3fGtGg60uQLAj4qhr4OBfbTd5ty','1','qiOMNHzckvIVt5kWInYDG56VXEljR3wtH2pOD1O7yeBHTEou6JWyQxnNgM1a','2015-06-02 12:36:00','2015-06-02 13:14:41'),(101,'somil mishra','somil@yopmail.com','$2y$10$EAcVZLm5Uey2Q7KGYfGamO/ue3wP8P/D59BfD4Kfz8WInfyD6js/a','1',NULL,'2015-06-02 12:38:29','2015-06-02 17:53:20'),(102,'1234 5678','abcd@yopmail.com','$2y$10$Nowm0OcdxsZpDTRi1YTP5OuSn8fmdmndaSS9FCLkB3ZZIDbhexrTS','1','2xaKO3KOLvCIM2JxwqDslJB4ugTgl1H2k2AZqUyMXdM37nMQMXbrZ3xq8KLJ','2015-06-02 13:21:58','2015-06-02 13:46:25'),(103,'Latest Photography','Photography@yopmail.com','$2y$10$JRc0Q7aplHLRztviw.vHAOo3p41zC8Z6ZUOO.qtf5Ymnoezw9VAkC','1',NULL,'2015-06-02 15:59:51','2015-06-02 16:01:58'),(104,'Mark Lee','richard.lam@gmail.com','$2y$10$CxPfN/MnIcQxMfhWoi1bYeiLPY7rvT0BgUPd504GBI5/UZQ85FfmK','1','bMafLKOH9OtTWRWTcLo4tQtGmtQ58zW77kByMBUPPAph7mSGLvjllBZ9ywly','2015-06-03 00:02:23','2015-06-21 21:59:55'),(105,'leslie mark','info@bigoone.com','$2y$10$XvINtgVlwdM2ZaI0H17pae61A/UL0LPKXbRKGiggpv0Ef9TvHPlDS','0',NULL,'2015-06-03 05:50:28','2015-06-03 05:50:28'),(107,'Lazar Angelov','lazar@yopmail.com','$2y$10$R7.zTm/eYSgIw0nvjdbpCeF8qeRUlfEmsJLH.WImzeVGwgq7h7/Pe','1','VYnfvifwrkf0vGp1DBvd1IwahV0ehQALMRGFFIvXPsH3Cut3J86cF6eoGcwi','2015-06-03 18:39:39','2015-06-04 02:21:45'),(108,'Matthew Charles','abs@yopmail.com','$2y$10$M0Yh0MNiQJVxv4ONr9l0ruFTqJHo4ZjgGkiM3MJ6uWQlFPsmjsrli','0',NULL,'2015-06-03 18:57:24','2015-06-03 18:57:24'),(109,'First Last','test@test.com','$2y$10$hB1yLHG5h54vTYS8E3Ot/u1SmZjq6HreMhRGTnn9HzBW9w4rDUw/K','0',NULL,'2015-06-08 04:56:10','2015-06-08 04:56:10'),(110,'Firstc Lastc','apple@apples.com','$2y$10$5vFwSfSw5zNHajNHBg.8lexCxaGUPKEuiJaeuDda4kgsl6X86WjCO','0',NULL,'2015-06-08 04:56:45','2015-06-08 04:56:45'),(111,'Firstcss Lastcss','apple@appless.com','$2y$10$XkxOpnb.57uNJ2A9PMl0j.aXlO6181rA.un8keluQSxRwsHTAT1jW','0',NULL,'2015-06-08 04:57:18','2015-06-08 04:57:18'),(112,'13213131','tyagi22@yopmail.com','$2y$10$OGCkSgcEzheynQctln2qZ.7M98oJZXcUV4Y0D8bKCG059nQYYcNLu','0','VVEVpsQwGCoE253HHWy3bnKoVUIBJHT8g4wraJWQSu4wjesbjjKpR0M3aN2Y','2015-06-09 13:17:23','2015-06-09 14:11:57'),(118,'soni','sonipanwar@yopmail.com','$2y$10$EXGNNUcAV5SoH0ZH5fqN/OTww5rFHpiZAbvQdu5n70Gdd2jJWbRF6','0',NULL,'2015-06-09 14:12:28','2015-06-09 14:12:28'),(121,'kldjfklj kjhdskfh','testingpicinsider@yopmail.com','$2y$10$09kSFe67xiuJsu2MC1DbP.2wGOCsMUp8J1xNXIko3PYtJ2DDcFXBW','1',NULL,'2015-06-09 14:51:02','2015-06-09 14:58:17'),(122,'Neha 2','verma@yopmail.com','$2y$10$xYk4gv7Nobt.CZNntiYsJuw2bJ3L0wtczfK1hdzwfKDB7kI/g54fi','1','HXsBHXHkSjfCk0oxGtUrAV5bjbCEdEEPuJw6qIDswX7UJ0NEvVLulI4HvJT5','2015-06-09 15:37:14','2015-06-19 11:41:18'),(123,'132132','shipra@yopmail.com','$2y$10$TxGP8GkhPCXHYhEKtxB3D.On4efd5C6AMu0zXl1FE.EdwPy2l.nu6','1',NULL,'2015-06-09 15:43:05','2015-06-09 15:43:31'),(124,'1234 5678','ricky@yopmail.com','$2y$10$OWJ8XKUH4gww46hP9ptluO/HJhI9ZOMtXzs22FlQJEgWRwMROi4DC','0','lQ6fOtHLm5QGeiPOhmr9b7pRNyhziEpP9XWmoSa5UF0GGvsC5oM5D2PCi8tQ','2015-06-09 15:46:47','2015-06-09 16:43:56'),(125,'1234 5678','ricky12@yopmail.com','$2y$10$cYu7w6hm47Is95ZCLONgYOxgCNN/ybk8SlxmTLFOFKZQBLPeAQtci','1',NULL,'2015-06-09 16:44:51','2015-06-09 16:47:17'),(126,'test','spanwar1@itpathshala.com','$2y$10$YOr1v8r6CQtaDVI8AOmUb.VDaf7hY8QTrJ/aQnzRzU/OfC6uCOV5e','1','ylUFZ5qnUt2udLhdLLuxIzSuOEonfBmbzGEdGOSfcDXkkDGM4Nl9A8bPuVlL','2015-06-10 16:46:15','2015-06-10 17:36:42'),(128,'516321321','shubh12@yopmail.com','$2y$10$.OTdX34muzm5Ic/zW0A19edk7B0XaSrSINhMX3mtZQJBIsDzJKEiK','0',NULL,'2015-06-10 16:56:17','2015-06-10 16:56:17'),(129,'shilpi verma','shilpi@yopmail.com','$2y$10$t68belST1QFXtHt20f35x.XO.l6cyopyQTztWx/Vd/ie9XAt8HTGi','1','bVKCSuXgREDgX6vTk3InNzejR1YTIJCODLmXavBwR8THL6h3AmJ0J73hVcVR','2015-06-10 17:37:50','2015-06-10 17:42:28'),(132,'ricky swain','swain@yopmail.com','$2y$10$eoqIPbtfV5ZsaEpeLmcg.uAF6IdDg4HJ5/Gg3zzZmnm4iRHR1sKpu','1','b2EqQ6Gy2yrNdJ5gskUb6BPc7bkrbyNYcKrGeGg21YUH5hPHtseWGfgKpq0X','2015-06-11 16:01:36','2015-06-11 16:22:15'),(133,'Some Company','somecompany@yopmail.com','$2y$10$Ac6secqdxwPfW16npMvzp.HC0p1UvrT6NXS72/0/z316HpRFSSKN.','1','IHnAA62Cx45HKaaHKmBkKYS9QmzsTZT1ou553vLQVqciiHYMO68lJSimezsH','2015-06-11 16:20:50','2015-06-11 16:35:23'),(135,'sonal tyagi','sonal@yopmail.com','$2y$10$zpK1.q5B.nZn///oRzR1b.E9hHpa.1tn5VfG3ZYcMmPWMpwQm.P1C','1','lerAJ7yw60lRkzacJ44mwPGdlYxIPRwZniPm91n4KK304InK573zFamjADRs','2015-06-11 16:23:27','2015-06-11 17:49:32'),(137,'test test','ks67@gmail.com','$2y$10$pNj7peR/0YrJVzH2qw6bNerp1Q4Dro4RGkJa1qcfBHNVzuoJ7e95m','0','ah9wmEpG5RkvsXRVVdTtfu4XP0QOkXN96pNxTfntVDXzScoABEYpF9iIlRHg','2015-06-11 16:44:27','2015-06-11 17:14:49'),(139,'somethigsomething some','something1245@yopmail.com','$2y$10$hepaxVvu7BUKeBKSBpvGl.nanNb.BJRknVqmBblxldynvIUrA7ETG','0',NULL,'2015-06-11 18:20:55','2015-06-11 18:20:55'),(140,'web app','webappstore2015@gmail.com','$2y$10$UyAX.ttqX76CYDJfvKGKeuL0yZwiWKV92PRjyTZ5NHdv89.p0KBU2','1','Ub1smvvOVSJscrnF9NjF2Vpk9iy6B0d731KPgzMz9A0RmEcWyalkncfslDgx','2015-06-12 12:15:50','2015-06-22 06:59:13'),(141,'Someone','xyzz@yopmail.com','$2y$10$Ws584KEBpQir7BvnPTlkrOof8Xd.bEZTDRS4ijZvtiyvuhdEM/00y','0',NULL,'2015-06-12 17:46:04','2015-06-12 17:46:04'),(142,'R Dizzle','vpn@vpnauthority.com','$2y$10$iKp9qB2Hq52Pz90drhRPfOlfHgo1BmN1YfIu0NyAajpBkcB8a4fNm','1','qIeOOv4CebYoINTCiEev8F8rXgu3lZEH0XvxdCoVeLU3YgUseaJp8DQMC91f','2015-06-14 16:35:57','2015-06-14 20:40:16'),(143,'shilpa','ks130865@gmail.com','$2y$10$jPZyQTuRbYVTDJdedmL/ougX/Khluo04mFHAmmU5cv3UlnYgCojsC','1','uEQgkiqjobM8fUeuYWKybEWfP023yoaSOqsbwIOATAKdyyQalpFXzxahsdhg','2015-06-15 14:54:37','2015-06-15 16:04:08'),(144,'gfshfv ghfvh','panwar56@yopmail.com','$2y$10$SnGkol7I4ckGWy4cUFVOAOdaoDAiypGfY2tWoV1yyLMlMZVxQLTYO','0','N3dsw1kXAe8dLVtaArIPK8DtfXdATHWjzvdAnt8bvoSnVJEhzYvNkhgCAXOz','2015-06-15 15:00:25','2015-06-15 15:06:11'),(145,'kriti sharma','seesharma25@gmail.com','$2y$10$s16s3oEaaNkpQsiTDpsKpO6PN4bQEpDlK.Y3T5laaysuT0hir20Ii','1','8c3m0FAewbRnEWIUeDtmFnWCyFhZ8CYS1DO6EIZAuz4SzLNKrcBYEkkwV9yI','2015-06-15 15:02:42','2015-06-15 15:18:13'),(146,'shilpa','shilpa23@yopmail.com','$2y$10$ZagxItdhyogek1.5XJvMAeYf4AaT6XNUjM6m/hOOEgJRdRClXxgDa','0',NULL,'2015-06-15 15:08:38','2015-06-15 15:08:38'),(147,'viky','viky56@yopmail.com','$2y$10$cCiXp4zB5efBGS1DjL0HBeCbGFpN8JBmYlQkS25Ap83xDvw4EO/6y','1','UnMovL8XGfwKCgUmk0EtdOdIAmRQaC9enDTviJ2gRl3mxVn3O4UGv0si3ssB','2015-06-15 15:13:15','2015-06-16 10:38:07'),(148,'test','ks13086@gmail.com','$2y$10$D/VRwrzA4cyNW47Cq/iYSOInUZKleKG1CA6vZc7e/8zZWmibNJ.7C','0','sIPw3abO286QUMAyjneEfRlW64Cy9F4nUv8FWkKaTN93R2LWDHF0i3gLN3zO','2015-06-15 15:18:41','2015-06-15 15:27:59'),(149,'test test','shilpa@gmail.com','$2y$10$ABqoKgv0ob6cJ36bfHEeLOCdg8ZKv3/aBLGwn1LW/43iDNRZyCj4u','0','IYeEzDZuNox3RsLJ9gXiWXw9gu6aKOn8r76JpQvOxk20KCb4ZCyq4yKLfk6S','2015-06-15 15:28:31','2015-06-15 15:34:10'),(150,'test parashar','shilpaparashar3@gmail.com','$2y$10$DA4GTz30cwZkH6PG9ARaa.VaGVm9Mf4p3ymnFGN3qf3nzaI3znN1C','1','wIRgA2Rc5WSYNz8p7HyVYAkTxsJDK9BaSivIe6Xheq1rnKOX5awEFNnVS0i8','2015-06-15 15:36:55','2015-06-15 16:03:19'),(152,'sv','vbcb@yopmail.com','$2y$10$cfpo9.KFeEx5k0CkhmGGI.0ipvbfeznoqfvWivtbXzHvvb2Oe4hgO','0',NULL,'2015-06-15 16:13:33','2015-06-15 16:13:33'),(153,'sjkhkjdzh','khan_shahrukh89@yopmail.com','$2y$10$kgY46goVPU5qg/sfSOxd6eAfSxaPSQ2PWp2pxOoRid0YLQAX6N2oO','0',NULL,'2015-06-15 16:17:55','2015-06-15 16:17:55'),(155,'hello','hello@yopmail.com','$2y$10$tEaDz/oambVx/XSj4BeD8O79nrWUNaJ38j1asn5ntbbHlXItnXpWm','1','yy9Q4NisYntIMVRVnFS9wzYiCmusmGlWQTIXS7pSYTE3q2z3CoDDePjp9W4r','2015-06-16 11:16:14','2015-06-16 11:35:11'),(156,'hello app','helloapp@yopmail.com','$2y$10$eUZsxgXLzkKM7yTw51vYkOMeLtlrixd0446IGTXrn8de70S/bPlNC','1','kXPlASWmNWGFUmje3FB9B3XOUDrWkbcQnithxCF5W1wUTwGlZ3oNZeu1CFvs','2015-06-16 11:38:16','2015-06-17 10:15:34'),(157,'Radhey','radhey.mzit@gmail.com','$2y$10$/M5fF.C0MZT6C0Fykgbp4ez6QJMHSH3KUPBDs4dZA.VPETbmp9WiS','1',NULL,'2015-06-16 17:00:27','2015-06-16 17:00:27'),(158,'consultant','ssodhi@myzealit.com','$2y$10$3e5dO.ZFdXTj/BNgagYone7T4jMMSxxF91gOwSjkO1lPHVq5duYg.','1','K42MBEBiqSZ19EBmlxvblIcFt3aADg0hdPR8Mu8SjAi0VKu8W607j7647e2g','2015-06-20 11:44:55','2015-06-20 12:07:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-22  2:01:31
