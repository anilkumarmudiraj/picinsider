<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotographerStylesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photographer_styles', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('photographer_id')->unsigned();
            $table->foreign('photographer_id')->references('user_id')->on('photographers')->onDelete('cascade');
            $table->integer('style_id')->unsigned();
            $table->foreign('style_id')->references('id')->on('styles')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photographer_styles');
	}

}
