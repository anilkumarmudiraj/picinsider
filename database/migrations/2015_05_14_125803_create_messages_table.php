<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('to_id')->unsigned();
            $table->integer('from_id')->unsigned();
            $table->string('to_name')->nullable();
            $table->string('from_name')->nullable();
            $table->string('email')->nullable();
            $table->text('message')->nullable();
            $table->string('connect', 64)->nullable();
            $table->foreign('to_id')->references('user_id')->on('photographers')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
