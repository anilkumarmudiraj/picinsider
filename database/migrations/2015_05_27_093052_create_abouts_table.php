<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('abouts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->text('slide');
            $table->text('onslider');
            $table->text('heading1');
            $table->text('para1');
            $table->string('person1_photo', 64);
            $table->string('person1_name',64);
            $table->string('person1_position',64);
            $table->string('person2_photo', 64);
            $table->string('person2_name',64);
            $table->string('person2_position',64);
            $table->string('person3_photo', 64);
            $table->string('person3_name',64);
            $table->string('person3_position',64);
            $table->string('person4_photo', 64);
            $table->string('person4_name',64);
            $table->string('person4_position',64);
            $table->string('person5_photo', 64);
            $table->string('person5_name',64);
            $table->string('person5_position',64);
            $table->string('person6_photo', 64);
            $table->string('person6_name',64);
            $table->string('person6_position',64);
            $table->string('hading2',64);
            $table->text('para2');
            $table->string('facebook',64);
            $table->string('twitter',64);
            $table->string('pinterest',64);
            $table->string('email',64);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('abouts');
	}

}
