<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invites', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('invited_by')->unsigned();
            $table->integer('invite_to')->unsigned();
            $table->integer('gig_id')->unsigned();
            $table->foreign('invited_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('invite_to')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('gig_id')->references('id')->on('gigs')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invites');
	}

}
