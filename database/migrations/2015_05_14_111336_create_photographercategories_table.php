<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotographercategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photographercategories', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 64);
            $table->integer('photographer_id')->unsigned();
            $table->foreign('photographer_id')->references('user_id')->on('photographers')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photographercategories');
	}

}
