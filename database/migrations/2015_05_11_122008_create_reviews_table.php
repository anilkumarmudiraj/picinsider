<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviews', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('subject', 64)->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('user_name', 64)->nullable();
            $table->integer('stars')->nullable();
            $table->text('review')->nullable();
            $table->integer('photographer_id')->unsigned();
            $table->foreign('photographer_id')->references('user_id')->on('photographers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviews');
	}

}
