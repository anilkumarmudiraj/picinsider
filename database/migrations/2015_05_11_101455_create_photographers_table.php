<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotographersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photographers', function(Blueprint $table)
		{
			$table->increments('id');
            $table->text('name');
            $table->text('company_name')->nullable();
            $table->string('phone_number',15)->nullable();
            $table->text('website')->nullable();
            $table->string('city', 64)->nullable();
            $table->char('established')->nullable();
            $table->string('studio',3)->nullable();
            $table->string('price', 15)->nullable();
            $table->string('under500',3)->nullable();
            $table->string('under1000',3)->nullable();

            $table->string('facebook',64)->nullable();
            $table->string('twitter', 64)->nullable();
            $table->string('pinterest', 64)->nullable();
            $table->string('gplus', 64)->nullable();
            $table->string('instagram', 64)->nullable();
            $table->tinyInteger('certified')->nullable();
            $table->text('brand')->nullable();
            $table->text('description')->nullable();
            $table->text('photo')->nullable();
            $table->text('logo')->nullable();
            $table->string('featured')->default('No');
            $table->string('subscribe',64)->nullable();
            $table->string('notification', 64)->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photographers');
	}

}
