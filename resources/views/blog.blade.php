@extends('layout.base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 m7">
                <img src="{{$blog->photo}}" class="img-responsive" style="position:relative;">
                <div class="banner-top">
                    <p style="margin-left:20px">{{$blog->title}}</p>
                    <span style="margin-left:20px">January 21, 2015 By {{$blog->author}}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {!! $blog->body !!}
                </div>
        <div class="row m5">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <a href="#" class="btn btn-sm btn-blue mb5"><span class="fa fa-facebook"></span>  Share on Facebook</a>
                <a href="#" class="btn btn-sm btn-lblue mb5"><span class="fa fa-twitter"></span> Twitter</a>
                <a href="#" class="btn btn-sm btn-red mb5"><span class="fa fa-pinterest"></span> Pinterest</a>

                <a href="#" class="btn btn-sm btn-gray mb5"><span class="fa fa-envelope"></span> E-mail</a>

            </div>
        </div>
        <div class="row">

            <div class="col-md-8">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="/images/photographer-view.png" width="80" height="60" alt="Picinsider"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <textarea class="form-control" rows="3"></textarea>
                        <div class="write_review m4"><button type="button" class="btn btn-default">Write a Review</button></div>
                    </div></div>
            </div>


            <div class="col-md-8 bo-dashed">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="/images/photographer-view.png" width="80" height="60" alt="Picinsider"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Best Photographer in the neighbourhood</h4>
                        <span>Chris from Toronto</span>
                        <p>Like - Reply - 2 minutes ago</p>
                    </div></div>
            </div>

            <div class="col-md-8">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="/images/photographer-view.png" width="80" height="60" alt="Picinsider"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Best Photographer in the neighbourhood</h4>
                        <span>Chris from Toronto</span>
                        <p>Like - Reply - 2 minutes ago</p>
                    </div></div>
            </div>
        </div>
    </div>
    </div>
@stop