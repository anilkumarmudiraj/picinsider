@extends('layout.base')

@section('content')
    <div class="container photo_tips">
        <p></p>
        <div class="row">
            @foreach($blogs as $blog)
            <div class="col-md-12">
                <img src="{{$blog->photo}}" class="img-responsive" >
                <h3>{{$blog->title}}</h3>
                <p><strong>Description :</strong> </p>
                <?php echo strip_tags(substr($blog->body,'0', '250'));?> <a href="/blog/{{$blog->id}}/{{\Illuminate\Support\Str::slug($blog->title)}}">keep reading</a>
            </div>
            @endforeach
        </div>
    </div>
    @stop