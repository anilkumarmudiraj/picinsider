@extends('layout.base')
<?php
use Illuminate\Support\Str;
        use App\Providers\AppServiceProvider;
?>
@section('content')

    <div class="container">
<hr />
        <h4 class="success">Gig has been successfully posted. You can invite photographers by clicking on their profile and hitting Invite Photographer. Following are some suggestions.</h4>
<hr />
        <div class="row toronto">
            @foreach($randomPhotographers as $photographer)
            <div class="col-sm-6 col-lg-3">
                <div class="imageOuter" style="margin:0; width:265px; height:180px;">
                    <a class="image" href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">
                        <img class="imgborder" alt="" src="/uploads/photographers/{{$photographer->photo}}" width="265px" height="180px">
                    </a>
                </div>
                <div class="imagebottom index-view">
                    <ul>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                    </ul>
                    <span><a href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">{{$photographer->name}}</a></span>

                    <p>
                        <?php echo strip_tags(substr($photographer->description, 0, 60));?>
                        <a href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">read more...</a>
                    </p>
                </div>
            </div>
                @endforeach
            </div>
    </div>

    @stop