@extends('layout.base')
<?php
use Illuminate\Support\Str;
?>
@section('content')

    <div id="myCarousel" class="carousel slide " data-ride="carousel">
        <!-- Indicators -->

        <div class="carousel-inner " role="listbox">
            <div class="item active">
                <img class="first-slide" src="/images/slider/sld1.jpg" alt="First slide">
            </div>
            @foreach($sliders as $slider)
                <div class="item">
                    <img class="first-slide" src="{{$slider->location}}" alt="Second slide">
                </div>
                @endforeach

                        <!--banner_form -->
                <div class="container midd_form">
                    <div class="col-sm-51 banner_form ">
                        <h2>I want to find a Photographer</h2>
                        <!--<form class="form-inline find_ptg" role="form">-->
                        {!! Form::open(['class' => 'form-inline find_ptg', 'method' => 'GET', 'url' => 'search']) !!}
                        <div class="form-group">
                            <label for="sel1" class="widdl">FOR:</label>
                            <select class="form-control" id="sel1" name="occasion">
                                @foreach($categories as $category)
                                    <option value="{{$category->name}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cn" class="labelf">NEAR:</label>
                            <input type="text" name="city" class="typeahead form-control" id="cities1" placeholder="City Name" autocomplete="off">
                        </div>

                        <button type="submit" class="fptg_submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        <!--</form>-->
                        {!! Form::close() !!}
                    </div>
                </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="container">
        <div class="row toronto">
            <h2>Featured Photographer in <span>Toronto</span></h2>
            @foreach($featuredPhotographers as $featuredPhotographer)
                <div class="col-sm-6 col-lg-3">
                    <div class="imageOuter" style="margin:0; width:265px; height:180px;">
                        <a class="image" href="/photographer/{{$featuredPhotographer->user_id}}/<?php echo Str::slug($featuredPhotographer->name);?>">
                            <img class="imgborder" alt="" src="/uploads/photographers/{{$featuredPhotographer->photo}}" width="265px" height="180px">
                        </a>
                    </div>
                    <div class="imagebottom index-view">
                        <ul>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        </ul>
                        <span><a href="/photographer/{{$featuredPhotographer->user_id}}/<?php echo Str::slug($featuredPhotographer->name);?>">{{$featuredPhotographer->name}}</a></span>

                        <p>
                                <?php echo strip_tags(substr($featuredPhotographer->description, 0, 60));?>
                               <a href="/photographer/{{$featuredPhotographer->user_id}}/<?php echo Str::slug($featuredPhotographer->name);?>#review_list">read more...</a>
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="midd_part"></div>
        <div class="row toronto">
            <h2>Recent Reviews in <span>Toronto</span></h2>
            @foreach($reviewedPhotographers as $photographer)
                <div class="col-sm-6 col-lg-3">
                    <div class="imageOuter" style="margin:0; width:265px; height:180px;">
                        <a class="image" href="#">

                            <a href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">
                                <img class="imgborder" alt="" src="/uploads/photographers/{{$photographer->photo}}" width="265px" height="180px"></a>
                        </a>
                    </div>
                    <div class="imagebottom index-view">
                        <ul>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        </ul>
                        <span><a href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">{{$photographer->name}}</a></span>

                        <p>
                            @foreach($photographer->mainreview as $reviews)
                                <?php echo strip_tags(substr($reviews->review, 0, 60));?><a
                                        href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>#review_list">read more...</a>
                            @endforeach
                        </p>

                        <p>
                            @foreach($photographer->photographercategory as $category)
                                {{$category->category}}
                            @endforeach
                        </p>

                    </div>
                </div>
            @endforeach()
        </div>

        <div class="midd_part"></div>
        <div class="row toronto">
            <?php $hasValue = count($latestPhotographers);?>
            @if ($hasValue == 0)
                <h2>No new Photographers</h2>
            @else
                <h2>New Photographers in <span>Toronto </span></h2>
                @foreach($latestPhotographers as $photographer)
                    <div class="col-sm-6 col-lg-3">
                        <div class="imageOuter" style="margin:0; width:265px; height:180px;">
                            <a class="image" href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">

                                <img class="imgborder" alt="" src="/uploads/photographers/{{$photographer->photo}}" width="265px" height="180px">
                            </a>
                        </div>
                        <div class="imagebottom index-view">
                            <ul>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            </ul>
                            <span><a href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>">{{$photographer->name}}</a></span>
                            <p>
                                @foreach($photographer->mainreview as $reviews)
                                    <?php echo strip_tags(substr($reviews->review, 0, 60));?>
                                        <a href="/photographer/{{$photographer->user_id}}/<?php echo Str::slug($photographer->name);?>#review_list">read more...</a>
                                @endforeach
                            </p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>
@stop
