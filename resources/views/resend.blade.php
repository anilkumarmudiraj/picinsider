@extends('layout.base')

@section('content')
    <p></p>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Information</div>

                    <div class="panel-body">

                        @if(isset($message))
                            <h4>{{$message}}</h4>
                            @else
                            <h4> Have you lost your way ?</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop