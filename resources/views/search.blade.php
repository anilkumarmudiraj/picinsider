@extends('../layout.base')
<?php
use Illuminate\Support\Str;
?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="wedding_search col-md-12">
                <h4>Search results: Photographer > {{$city}} > {{$occasion}}</h4>
                <div class="bs-example">
                    <ul class="nav nav-tabs show-hide">
                        <li class="active"><a data-toggle="tab" href="#sectionA">Show Filter</a></li>
                        <li><a data-toggle="tab" href="#sectionB">Hide Filter</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="sectionA" class="tab-pane fade in active">
                            <div class="col-md-12 tab_filter">

                                <div class="col-md-3 form-group">
                                    <label for="exampleInputEmail1">Sort by:</label>
                                    <select class="form-control">
                                        <option>Best matches</option>
                                        <option>Highest rated</option>
                                        <option>Latest reviewed</option>
                                        <option># of reviews</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="exampleInputEmail1">Experience:</label>
                                    <select class="form-control">
                                        <option>0</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="exampleInputEmail1">Price Range:</label>

                                    <select name="price" class="form-control">
                                        <option value="">Select upon budget</option>
                                        <option value="200">Upto $200</option>
                                        <option value="500">Between $200-500</option>
                                        <option value="1000">Between $500-1000</option>
                                        <option value="2000">$1000 and more</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="exampleInputEmail1">Other Filters:</label>
                                    <select name="other" class="form-control">
                                        <option value="">Flexibility</option>
                                        <option value="500">Willing to work on Gigs under $500</option>
                                        <option value="1000">Willing to work on Gigs under $1000</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="sectionB" class="tab-pane fade">
                            <h3>Hide Filter</h3>
                            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id
                                gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta
                                ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit
                                sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus
                                adipiscing.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
@if(count($results) <1)
    <h3> No results matching your criteria</h3>
                @else
                @foreach($results as $result)
                <div class="wedding_spark">
                    <div class="write_review">
                        @if(Auth::check())
                        <button type="button" class="btn btn-default" data-toggle="modal" id="write_review" data-target="#exampleModal" data-review-user_id="{{$result->user_id}}">Write a Review</button>
                            <!--review code-->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                                <span class="fa fa-close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group">
                                                    <label for="subject" class="control-label">Subject :</label>
                                                    <input type="text" class="form-control" id="review_subject" placeholder="Subject">
                                                    <input type="hidden" id="review_photographer_id" value="">
                                                    <input type="hidden" id="review_token" value="{{ csrf_token() }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="name">Name :</label>
                                                    <input type="text" class="form-control" id="review_user_name" placeholder="Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="name">Review :</label>
                                                    <textarea class="form-control" rows="3" id="review_review" placeholder="Review"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="name">Rating :</label>
                                                    <select id="review_rating" class="form-control">
                                                        @for($i=1; $i <=5; $i++)
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default close2" data-dismiss="modal">Close</button>
                                            <button type="button" id="write-review" class="btn btn-success">Post Review</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--review code-->
                            <script language="javascript">
                                $(document).on("click", "#write_review", function () {
                                    var myBookId = $(this).data('review-user_id');
                                    $(".modal-body #review_photographer_id").val( myBookId );
                                    $('#addBookDialog').modal('show');
                                });
                            </script>
                        @endif



                    </div>
                    <div class="col-md-3 wedding_spic row">
                       <a href="/photographer/{{$result->user_id}}/<?php echo Str::slug($result->name);?>"> <img src="/uploads/photographers/{{$result->photo}}"></a>
                    </div>
                    <div class="col-md-9 wedding_catalist">
                        <h3><a href="photographer/{{$result->user_id}}/<?php echo Str::slug($result->name);?>">{{$result->company_name}}</a></h3>
                        <ul class="search_spark_reating">
                            @foreach($result->mainreview as $mainreview)
                                @for($i=1; $i <= $mainreview->stars; $i++)
                                    <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                @endfor
                            @endforeach
                        </ul>
                        <p><?php echo count($result->reviews);?> reviews <span>$$$</span></p>
                        <ul class="categories">
                            <li>Categories :
                                @if($result->photographercategory)
                                @foreach($result->photographercategory as $category)
                                <span>{{$category->name}} </span>
                                    @endforeach
                                    @endif
                            </li>
                            @foreach($result->mainreview as $mainreview)
                            <li>{{$mainreview->subject}}</li>
                            <li>“ {{$mainreview->review}}”</li>
                                @endforeach
                        </ul>

                    </div>
                </div>
@endforeach
                @endif

                <div class="review-pagetab" data-example-id="review-pagination">
                    <nav>
                        <ul class="pagination">
                            <?php echo $results->render();?>
                            </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-3 search_sidebar">
                <div class="con_view">
                    <h3 class="tips">Tips <span>to Consider</span></h3>
                    <ul>
                        <li>1. Contact several photoraphers since quantities are always limited.</li>
                        <li>2. Smaller photographer may be wiling to charge less than arger stores which have larger
                            overhead.
                        </li>
                    </ul>
                </div>
                <!--<div class="con_view">

                    <h3 class="re_view">Recently <span>Viewed</span></h3>
                    <ul>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a>
                                </li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>

                    </ul>

                </div>-->
            </div>
        </div>
    </div>
@stop