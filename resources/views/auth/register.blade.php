@extends('layout.base')

@section('content')
<p></p>
    <div class="container-fluid  business one">
	<div class="row">
	<!-- <div class="col-md-12"><h2>Create a user account in 30 seconds</h2></div> -->
	<div class="col-md-6">
        	<h4>Registration is free! It allows you to:</h4>
            <ul class="benefit-lists">
            	<li>- Post project for photographers to bid so you get what you want at the BEST pricing</li>
            	<li>- Write reviews on your experience</li>
            	<li>- Directly contact photographers</li>
            	<li>- Receive tips on what to look for in a photographer</li>
            </ul>
        </div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
	            <div class="text-center" style="  margin-top: 14px;">Login with <a href="/facebook"><img src="/images/oauth-facebook.png"></a></div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name<span style="color: red">*</span></label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name">
                                <script language="javascript">
                                    $('#name').bind('keyup blur',function(){
                                                var node = $(this);
                                                node.val(node.val().replace(/[^a-zA-Z\s]*$/,'') ); }
                                    );
                                </script>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address<span style="color: red">*</span></label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password<span style="color: red">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password<span style="color: red">*</span></label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
            <br />
		</div>
	</div>
</div>
@endsection
