<!DOCTYPE html>
<html lang="en">
<head>
    <title>PicInsider</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico">
    {!! HTML::style('/bootstrap-3.3.4-/css/bootstrap.css') !!}
    {!! HTML::style('/style/style.css') !!}
    {!! HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') !!}
    {!! HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') !!}
    {!! HTML::style('/fonts.css') !!}
    {!! HTML::style('http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
    {!! HTML::style('/style/flexslider.css') !!}

    {!! HTML::style('/font-awesome/css/font-awesome.min.css') !!}
    <!-- type ahead file inclusion -->
    <!-- {!! HTML::script('/typeahead/assets/css/bootstrap.min.css') !!} -->
    {!! HTML::script('http://code.jquery.com/jquery-1.8.0.min.js') !!}
    {!! HTML::script('https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js') !!}
    {!! HTML::script('/typeahead/assets/js/bootstrap.min.js') !!}
    {!! HTML::script('/typeahead/assets/js/jquery.mockjax.js') !!}
    {!! HTML::script('/typeahead/src/bootstrap-typeahead.js') !!}
    {!! HTML::script('/js/jquery.validate.min.js') !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        function countChar(val) {
            var len = val.value.length;
            if (len >= 140) {
                val.value = val.value.substring(0, 140);
            } else {
                $('#charNum').text(140 - len + ' characters left');
            }
        }
    </script>


    {!! HTML::script('/js/typeahead.js') !!}

    <?php $path = Request::path();

    if(!preg_match('!photographer/[0-9]/[A-Za-z]!',$path) && $path != '/')
        {
        // echo '<script src="http://code.jquery.com/jquery-1.5.js"></script>';
        }
    ?>
    @if($path == 'secondpage')
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
        {!! HTML::script('/js/phonefunction.js') !!}
        <script type="text/javascript">
            var jQuery_1_9_1 = $.noConflict(true);
            var jQuery_1_8_0 = $.noConflict(true);
        </script>
    @elseif($path == 'post-project')
        <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css"
              rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>

    @endif

</head>
<body>
<div class="container">
    <div class="col-sm-4 col-lg-6 logo"><a href="/"><img src="/images/logo.jpg"></a></div>
    <div class="col-sm-8 col-lg-6 search_box">
        <div class="search">
            {!! Form::open(['method' => 'GET', 'url' => 'topsearch']) !!}
            <div class="form-group seach_top">

                <input type="text" name="photographer" class="form-control" id="byphoto" placeholder="By Photographer"
                       autocomplete="off">
            </div>
            <div class="form-group seach_top">

                <input type="text" name="city" class="typeahead form-control" placeholder="By City near Toronto"
                       autocomplete="off">
            </div>

            <button type="submit" class="btn btn-info btn-ts ">
                <span class="glyphicon glyphicon-search"></span> Search
            </button>
            {!! Form::close() !!}
        </div>
        @if(!Auth::check())
            <div class="attention">Attention photographer: <span><a href="/photographer/register">Click here to add your
                        business</a></span></div>
        @endif

    </div>
</div>
<div class="menu">
    <div class="container ">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav nav_menu">
                        <?php $allcategories = Helper::getallcategories();?>
                        <li class="dropdown-toggle" data-toggle="dropdown" id = "dropdownmenu"><a href="javascript:void(0);">Catagories</a></li>
                        <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="showAndhide">
                         @foreach($allcategories as $getCategory)
                          <!--<li><a href="search?occasion={{$getCategory['name']}}">{{$getCategory['name']}}</a></li>-->
			<li><a href="{{ url('search?occasion='.$getCategory['name']) }}">{{$getCategory['name']}}</a></li>
                           @endforeach      
                        </ul>    
                    </ul>
                    <ul class="nav navbar-nav nav_menu">
                        @if(Auth::check())
                            <li><a href="/post-project">Post a Gig</a></li>
                        @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if(!Auth::check())
                            <li><a href="/register">Sign Up</a></li>
                            <li><a href="/login">Sign In</a></li>
                        @endif
                        @if(Auth::check())
                            <li><a href="/logout">Sign Out {{Auth::user()->name}}</a></li>
                            @if(Auth::user()->role->role == 'photographer')
                                <li><a href="/photographer/dashboard">My Profile </a></li>
                            @else
                                <li><a href="myprofile">My Profile </a></li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
@yield('content')
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 footer_list">
                <ul>
                    <li><a href="javascript:void(0);">Users</a></li>
                    <li><a href="javascript:void(0);">Post a Project</a></li>
                    <li><a href="javascript:void(0);">Write a Review</a></li>
                    @if(Auth::check())
		    <li><a href="myprofile">Manage Your Account</a></li>
		    @endif
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 footer_list">
                <ul>
                    <li><a href="javascript:void(0);">Categories</a></li>
                    <li><a href="javascript:void(0);">Wedding</a></li>
                    <li><a href="javascript:void(0);">Engagement</a></li>
                    <li><a href="javascript:void(0);">Portraits</a></li>
                    <li><a href="javascript:void(0);">Newborn</a></li>
                    <li><a href="javascript:void(0);">Other</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 footer_list">
                <ul>
                    <li><a href="javascript:void(0);">Photographer</a></li>
		    @if(Auth::check())                    
		    <li><a href="myprofile">Manage Your Account</a></li>
                    @endif
		    <li><a href="javascript:void(0);">Become Certified</a></li>
                    <li><a href="javascript:void(0);">Claim a Business</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 footer_list">
                <ul>
                    <li><a href="javascript:void(0);">General</a></li>
                    <li><a href="/about-us">About Us</a></li>
                    <li><a href="javascript:void(0);">Photographer Tips</a></li>
                    <li><a href="/contact-us">Contact Us</a></li>
                    <li><a href="/faq">Help/FAQ’s</a></li>
                </ul>
                <ul class="fo_blog">
                    <li><a href="/blog">Blog</a></li>
                    <li><a href="javascript:void(0);">In the Press</a></li>
                    <li><a href="javascript:void(0);">Tell a Friend</a></li>
                </ul>
            </div>
        </div>
        <div class="row footer_two">
            <div class="col-lg-7 footer_menu">
                <ul>
                    <li><a href="/city/toronto">Toronto</a></li>
                    <li><a href="/city/edmonton">Edmonton</a></li>
                    <li><a href="/city/vancouver">Vancouver</a></li>
                    <li><a href="/city/montreal">Montreal</a></li>
                    <li><a href="/city/calagary">Calgary</a></li>
                    <li><a href="javascript:void(0);">More cities</a></li>
                </ul>
            </div>
            <div class="col-lg-5">
                <ul class="footer_media">
                    <li>FOLLOW US</li>
                    <li><a href="javascript:void(0);"></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-facebook-square fa-2x"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-twitter fa-2x"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-pinterest-p fa-2x"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-google-plus fa-2x"></i></a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-instagram fa-2x"></i></a></li>
                </ul>
            </div>
        </div>

    </div>
</div>
<div class="footer_bottom">
    <div class="container">
        <div class="row footer_last">
            <div class="col-sm-6 footer_logo">Copyright ©PicInsider. All rights reserved.</div>
            <div class="col-sm-6 ">
                <ul class="site_map">
                    <li><a href="/sitemap.xml">Sitemap </a></li>
                    <li><a href="/privacy-policy">Privacy Policy </a></li>
                    <li><a href="/terms-conditions">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
{!! HTML::script('/js/functions.js') !!}
<script language="javascript">
$("#dropdownmenu").click(function(){
    $("#showAndhide").toggle();
});
    $('#typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 3,
        limit: 8
    }, {
        source: function (q, cb) {
            return $.ajax({
                dataType: 'json',
                type: 'get',
                url: 'http://gd.geobytes.com/AutoCompleteCity?callback=json&q=' + q,
                chache: false,
                success: function (data) {
                    var result = [];
                    $.each(data, function (index, val) {
                        result.push({
                            value: val
                        });
                    });
                    cb(result);
                }
            });
        }
    });

</script>


<script language="javascript">
    $('#projectcities').typeahead({
        source: [
            @foreach($cities as $city)
            {id: {{$city->id}}, name: '{{$city->name}}'},
            @endforeach
        ]
    });
</script>

<script language="javascript">
    $('#byphoto').typeahead({
        source: [
            @foreach($autocompletePhotographers as $acPhotographer)
            {id: {{$acPhotographer->id}}, name: '{{$acPhotographer->name}}'},
            @endforeach
        ]
    });
</script>

<script language="javascript">
    $('#postproject').typeahead({
        source: [
            @foreach($autocompletePhotographers as $acPhotographer)
            {id: {{$acPhotographer->id}}, name: '{{$acPhotographer->name}}'},
            @endforeach
        ]
    });
</script>

{!! HTML::script('js/jquery.flexslider.js') !!}

<script type="text/javascript">
    $(function () {
        //SyntaxHighlighter.all();
    });
    $(window).load(function () {
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 94,
            itemMargin: 10,
            asNavFor: '#slider'
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
</script>
</body>
</html>
