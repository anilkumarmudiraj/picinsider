@extends('admin.layout.base')

@section('content')

    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>Administrator</h1>
                <h2 class="">About Us</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">TABLES</a></li>
                    <li class="active">Statictable</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->
                <div class="row">
                    <div class="col-md-10">
                        <div class="block-web">
                            <div class="header">
                                <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                                <h3 class="content-header">Details of About us page</h3>
                            </div>
                            <div class="porlets-content">
                                    {!! Form::open(['class' => 'form-horizontal row-border']) !!}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Text for slider</label>
                                        <div class="col-sm-9">
                                            <textarea name="onslide" rows="4" class="form-control">{{$aboutus->onslider}}</textarea>
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Text for first heading</label>
                                        <div class="col-sm-9">
                                            <textarea name="heading1" rows="4" class="form-control">{{$aboutus->heading1}}</textarea>
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Text for first paragraph</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control ckeditor" name="para1" rows="6">{{$aboutus->para1}}</textarea>
                                        </div>
                                    </div><!--/form-group-->


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Person one name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="person1_name" value="{{$aboutus->person1_name}}">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Person one position</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="person1_position" value="{{$aboutus->person1_position}}">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Person two name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="person2_name" value="{{$aboutus->person2_name}}">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Person two position</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="person2_position" value="{{$aboutus->person2_position}}">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Person three name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="person3_name" value="{{$aboutus->person3_name}}">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Person three position</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="person3_position" value="{{$aboutus->person3_position}}">
                                        </div>
                                    </div><!--/form-group-->


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Text for second heading</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control ckeditor" name="heading2" rows="6">{{$aboutus->heading2}}</textarea>
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Text for second paragraph</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control ckeditor" name="para2" rows="6">{{$aboutus->para2}}</textarea>
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Facebook page</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="facebook" value="{{$aboutus->facebook}}" placeholder="do not add http://facebook.com only supply page id or name">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Twitter handle</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="twitter" value="{{$aboutus->twitter}}" placeholder="do not add http://twitter.com only supply handle">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Pinterest username </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="pinterest" value="{{$aboutus->pinterest}}" placeholder="do not add http://facebook.com only supply name or id">
                                        </div>
                                    </div><!--/form-group-->

                                    <div class="bottom">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </div><!--/form-group-->
                                {!! Form::close() !!}
                            </div><!--/porlets-content-->
                        </div><!--/block-web-->
                    </div><!--/col-md-6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/contentpanel-->
    @stop


<script type="text/javascript" src="/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="/admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="/admin/js/form-components.js"></script>
<script type="text/javascript" src="/admin/plugins/input-mask/jquery.inputmask.min.js"></script>
<script type="text/javascript" src="/admin/plugins/input-mask/demo-mask.js"></script>
<script type="text/javascript" src="/admin/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="/admin/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="/admin/plugins/ckeditor/ckeditor.js"></script>
