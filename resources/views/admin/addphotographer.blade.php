@extends('admin.layout.base')

@section('content')
    <div class="contentpanel">
        <p></p>
        <div class="container clear_both padding_fix">
            <div class="row">
                <div class="col-md-10">
                    <div class="block-web">
                        <div class="header">
                            <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                            <h3 class="content-header">Add a Photographer</h3>
                        </div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="porlets-content">
                            {!! Form::open(['class' => 'form-horizontal row-border', 'files' => 'true']) !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Full Name <span style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" placeholder="Full name of the photographer" id="name" value="{{old('name')}}">
                                    <script language="javascript">
                                        $('#name').bind('keyup blur',function(){
                                                    var node = $(this);
                                                    node.val(node.val().replace(/[^a-zA-Z\s]*$/,'') ); }
                                        );
                                    </script>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Company Name <span style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="company_name" class="form-control" placeholder="Company name of the photographer" value="{{old('company_name')}}">
                                </div>
                            </div><!--/form-group-->


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email address <span style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control" placeholder="Email address used for email conversations" value="{{old('email')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Password <span style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Contact number" value="{{old('phone')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Website</label>
                                <div class="col-sm-9">
                                    <input type="text" name="website" class="form-control" placeholder="Website if any" value="{{old('website')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9">
                                    <input type="text" name="city" class="typeahead form-control" placeholder="City of residence" autocomplete="off" value="{{old('city')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Established</label>
                                <div class="col-sm-9">
                                    <input type="number" min="1900" max="2015" name="established" class="form-control" placeholder="Started in the year" value="{{old('established')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Studio</label>
                                <div class="col-sm-9">
                                    <select name="studio" class="form-control">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Price</label>
                                <div class="col-sm-9">
                                    <select name="price" class="form-control">
                                        <option value="0-1000">$0-1000</option>
                                        <option value="1000-5000">$$1000-5000</option>
                                        <option value="5000-10000">$$$5000+</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Projects Under $500 ?</label>
                                <div class="col-sm-9">
                                    <select name="under500" class="form-control">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Projects under $1000 ?</label>
                                <div class="col-sm-9">
                                    <select name="under1000" class="form-control">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Facebook</label>
                                <div class="col-sm-9">
                                    <input type="text" name="facebook" class="form-control" placeholder="Facebook username" value="{{old('facebook')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Twitter</label>
                                <div class="col-sm-9">
                                    <input type="text" name="twitter" class="form-control" placeholder="Twitter username" value="{{old('twitter')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Pinterest</label>
                                <div class="col-sm-9">
                                    <input type="text" name="pinterest" class="form-control" placeholder="Pinterest username" value="{{old('pinterest')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Google Plus</label>
                                <div class="col-sm-9">
                                    <input type="text" name="gplus" class="form-control" placeholder="Google plus username" value="{{old('gplus')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Instagram</label>
                                <div class="col-sm-9">
                                    <input type="text" name="instagram" class="form-control" placeholder="Instagram username" value="{{old('instagram')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Certified ?</label>
                                <div class="col-sm-9">
                                    <select name="certified" class="form-control">
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Favorite Lens Brand</label>
                                <div class="col-sm-9">
                                    <input type="text" name="brand" class="form-control" placeholder="Camera Lens" value="{{old('brand')}}">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="description">{{old('description')}}</textarea>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Photo <span style="color:red">*</span></label>
                                <div class="col-sm-9">
                                    <input type="file" name="photo" class="form-control" accept="image/jpeg,image/png" placeholder="Cover Photo">
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Featured ?</label>
                                <div class="col-sm-9">
                                    <select name="featured" class="form-control">
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Subscribe to mailing list ?</label>
                                <div class="col-sm-9">
                                    <select name="subscribe" class="form-control">
                                        <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Send Gig notifications ?</label>
                                <div class="col-sm-9">
                                    <select name="notifications" class="form-control">
                                        <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div><!--/form-group-->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Country</label>
                                <div class="col-sm-9">
                                    <select name="country">
                                        @foreach($countries as $country)
                                            <option value="{{$country->name}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div><!--/form-group-->
                            <div class="bottom">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="/administrator"><button type="button" class="btn btn-default">Cancel</button></a>
                            </div><!--/form-group-->
                            {!! Form::close() !!}
                        </div><!--/porlets-content-->
                    </div><!--/block-web-->
                </div><!--/col-md-6-->
            </div>
        </div>
    </div>
@stop