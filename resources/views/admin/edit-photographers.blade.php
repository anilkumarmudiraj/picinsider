@extends('admin.layout.base')

@section('content')

<div class="contentpanel">
    <!--\\\\\\\ contentpanel start\\\\\\-->
    <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
            <h1>Administrator</h1>
            <h2 class="">Edit {{$info->name}}</h2>
        </div>
        <div class="pull-right">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Photographers</a></li>
                <li><a href="#">Edit</a></li>
                <li class="active">{{$info->id}}</li>
            </ol>
        </div>
    </div>
    <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->


        <div class="row">
            <div class="col-md-10">
                <div class="block-web">
                    <div class="header">
                        <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                        <h3 class="content-header">We have following details of {{$info->name}}</h3>
                    </div>
                    <div class="porlets-content">
                        {!! Form::open(['class' => 'form-horizontal row-border', 'files' => 'true']) !!}
                        @if(isset($message)) <h3 class="text-center"> {{$message}}</h3> @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label">User Id</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{$info->id}}" name="userid" readonly>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name <span style="color:red">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" value="@if(isset($info->photographer->name)){{$info->photographer->name}}@else{{$info->name}}@endif" placeholder="Name">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company Name <span style="color:red">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="company_name" value="@if(isset($info->photographer->company_name)){{$info->photographer->company_name}}@endif" placeholder="Company Name">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail <span style="color:red">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control" name="email" value="{{$info->email}}" placeholder="Email address">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="phone" value="@if(isset($info->photographer->phone_number)){{$info->photographer->phone_number}}@endif" placeholder="Phone number">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Website</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="website" value="@if(isset($info->photographer->website)){{$info->photographer->website}}@endif" placehodler="website">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" name="city" class="form-control" value="@if(isset($info->photographer->city)){{$info->photographer->city}}@endif" placeholder="city">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Established</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="established" value="@if(isset($info->photographer->established)){{$info->photographer->established}}@endif" placeholder="Year established">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Studio</label>
                            <div class="col-sm-9">
                                <select name="studio" class="form-control">
                                    @if($info->photographer->studio == 'Yes')
                                    <option value="Yes" selected="selected">Yes</option>
                                    <option value="No">No</option>
                                        @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                        @endif
                                </select>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Average Price</label>
                            <div class="col-sm-9">
                                <select name="price" class="form-control">
                                    @if($info->photographer->price == '100-250')
                                    <option value="100-250" selected="selected">$100-250</option>
                                        <option value="250-500">$250-500</option>
                                        <option value="500-1000">$500-1000</option>
                                        <option value="1000-2000">$1000-2000</option>
                                    @elseif($info->photographer->price == '250-500')
                                        <option value="100-250">$100-250</option>
                                        <option value="250-500" selected="selected">$250-500</option>
                                    <option value="500-1000">$500-1000</option>
                                    <option value="1000-2000">$1000-2000</option>
                                        @elseif($info->photographer->price == '500-1000')
                                        <option value="100-250">$100-250</option>
                                        <option value="250-500">$250-500</option>
                                        <option value="500-1000" selected="selected">$500-1000</option>
                                        <option value="1000-2000">$1000-2000</option>
                                        @else
                                        <option value="100-250">$100-250</option>
                                        <option value="250-500">$250-500</option>
                                        <option value="500-1000">$500-1000</option>
                                        <option value="1000-2000" selected="selected">$1000-2000</option>
                                        @endif

                                </select>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Looking for gigs under 500?</label>
                            <div class="col-sm-9">
                                <select name="under500" class="form-control">
                                    @if($info->photographer->under500 == 'Yes')
                                        <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                    @endif
                                </select>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Looking for gigs under 1000?</label>
                            <div class="col-sm-9">
                                <select name="under1000" class="form-control">
                                    @if($info->photographer->under1000 == 'Yes')
                                        <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                    @endif
                                </select>
                            </div>
                        </div><!--/form-group-->
<!-- Facebook -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Facebook</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="facebook" value="@if(isset($info->photographer->facebook)){{$info->photographer->facebook}}@endif" placeholder="Facebook username or user id">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Twitter</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="twitter" value="@if(isset($info->photographer->twitter)){{$info->photographer->twitter}}@endif" placeholder="Twitter handle">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Google Plus</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="gplus" value="@if(isset($info->photographer->gplus)){{$info->photographer->gplus}}@endif" placeholder="Google plus username or user id">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Pinterest</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="pinterest" value="@if(isset($info->photographer->pinterest)){{$info->photographer->pinterest}}@endif" placeholder="Pinterest username">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Instagram</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="instagram" value="@if(isset($info->photographer->instagram)){{$info->photographer->instagram}}@endif" placeholder="Instagram username">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Certified ?</label>
                            <div class="col-sm-9">
                                <select name="certified" class="form-control">
                                    @if(isset($info->photographer->certified) && ($info->photographer->certified == 1))
                                        <option value="1" selected="selected">Yes</option>
                                        <option value="0">No</option>
                                    @else
                                        <option value="1">Yes</option>
                                        <option value="0" selected="selected">No</option>
                                    @endif
                                </select>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Favorite Lens</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="brand" value="@if(isset($info->photographer->brand)){{$info->photographer->brand}}@endif" placeholder="Favorite camera lens">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control">@if(isset($info->photographer->description)){{$info->photographer->description}}@endif</textarea>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Featured ?</label>
                            <div class="col-sm-9">
                                <select name="featured" class="form-control">
                                    @if(isset($info->photographer->featured) && ($info->photographer->featured == 'Yes'))
                                    <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                    <option value="No" selected="selected">No</option>
                                        @endif
                                </select>

                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Subscribe to newsletter ?</label>
                            <div class="col-sm-9">
                                <select name="subscribe" class="form-control">
                                    @if(isset($info->photographer->subscribe) && ($info->photographer->subscribe == 'Yes'))
                                        <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                    @endif
                                </select>

                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Want Gig Notifications ?</label>
                            <div class="col-sm-9">
                                <select name="notification" class="form-control">
                                    @if(isset($info->photographer->notification) && ($info->photographer->notification == 'Yes'))
                                        <option value="Yes" selected="selected">Yes</option>
                                        <option value="No">No</option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                    @endif
                                </select>

                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Current Cover Photo</label>
                            <div class="col-sm-9">
                                <img src="/uploads/photographers/{{$info->photographer->photo}}" width="450px" height="300px">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cover Photo</label>
                            <div class="col-sm-9">
                                <input type="file" name="photo"><small>Choose only if you want to replace the current photo</small>
                            </div>
                        </div><!--/form-group-->

                        <div class="bottom">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div><!--/form-group-->
                        {!! Form::close() !!}
                    </div><!--/porlets-content-->
                </div><!--/block-web-->
            </div><!--/col-md-6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/contentpanel-->
@stop