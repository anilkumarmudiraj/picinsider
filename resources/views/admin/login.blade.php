<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Picinsider Admin Dashboard </title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    {!! HTML::style('/admin/css/font-awesome.css') !!}
    {!! HTML::style('/admin/css/bootstrap.min.css') !!}
    {!! HTML::style('/admin/css/animate.css') !!}
    {!! HTML::style('/admin/css/admin.css') !!}
</head>
<body class="light_theme  fixed_header left_nav_fixed">
<div class="wrapper">
    <!--\\\\\\\ wrapper Start \\\\\\-->

    <div class="login_page">
        <div class="login_content">
            <div class="panel-heading border login_heading">sign in now</div>
            <!-- <form role="form" class="form-horizontal">-->
                {!! Form::open(['class' => 'form-horizontal']) !!}
                @if(Session::has('message'))
                    {{Session::get('message')}}
                @endif
                <div class="form-group">

                    <div class="col-sm-10">
                        <input type="text" placeholder="Username" id="inputEmail3" name="email" class="form-control">
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-sm-10">
                        <input type="password" placeholder="Password" id="inputPassword3" name="password" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class=" col-sm-10">
                        <div class="checkbox checkbox_margin">
                            <label class="lable_margin">
                                <input type="checkbox"><p class="pull-left"> Remember me</p></label>
                                <button class="btn btn-default pull-right" type="submit">Sign in</button>
                            </div>
                    </div>
                </div>
{!! Form::close() !!}
            <!--</form>-->
        </div>
    </div>
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Compose New Task</h4>
            </div>
            <div class="modal-body"> content </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
<script language="javascript">
    alert('Invalid credentials');
</script>
@endif


{!! HTML::script('/admin/js/jquery-2.1.0.js') !!}
{!! HTML::script('/admin/js/bootstrap.min.js') !!}
{!! HTML::script('/admin/js/common-script.js') !!}
{!! HTML::script('/admin/js/jquery.slimscroll.min.js') !!}
</body>
</html>
