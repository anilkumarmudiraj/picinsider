@extends('admin.layout.base')

@section('content')
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>Media Manager</h1>
                <h2 class="">Subtitle goes here...</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">APPS</a></li>
                    <li class="active">Media Manager</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->
            <div class="file_Manager">
                <div class="file_Manager_option">
                    <ul>
                        <li> <a href="#">
                                <input type="checkbox">
                                Select All</a> </li>
                        <li> <a href="#"><i class="fa fa-envelope-o"></i>Email</a> </li>
                        <li> <a href="#"><i class="fa fa-download"></i>Download</a> </li>
                        <li> <a href="#"><i class="fa fa-pencil"></i>Edit</a> </li>
                        <li> <a href="#"><i class="fa fa-trash-o"></i>Delete</a> </li>
                        <li class="file_type"> Show: <a href="#">Document</a> <a href="#">Audio</a> <a href="#">Image</a> <a href="#">Video</a> </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        @foreach($sliders as $slider)
                        <div class="col-xs-6 col-sm-4 col-md-3">
                            <div class="thumb">
                                <div class="thumb_image"><img src="{{$slider->location}}" width="150px" height="150px"/></div>
                                <br />
                                Added: <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($slider->created_at))->diffForHumans() ?>
                                <br />
                                <input type="hidden" value="{{ csrf_token() }}" id="csrftoken">
                                @if($slider->active == 'Yes')
                                <a class="deactivate" data-deactivate_id="{{$slider->id}}" ><i class="fa fa-times"></i>Deactivate</a>
                                @else
                                    <a class="activate" data-activate_id="{{$slider->id}}" ><i class="fa fa-check"></i>Activate</a>
                                @endif
                                <a class="delete" data-delete_id="{{$slider->id}}"><i class="fa fa-trash-o"></i>Delete</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @if(isset($message))
                <script language="javascript"> alert('{{$message}}');</script>
                @endif
                <div class="col-sm-3">
                    <div class="file_sidebar">
                        {!! Form::open(['url' => '/administrator/sliderupload', 'files' => 'true']) !!}
                        <label>Upload slider </label><input name="slider" data-browse-class="btn btn-o btn-default" type="file" class="btn btn-primary btn-block upload_btn">
                        <input type="submit" class="form-control" value="Upload">
                        {!! Form::close() !!}



                        <br/>
                    </div>
                </div>
            </div>
        </div>
        <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
    </div>
    <!--\\\\\\\ inner end\\\\\\-->
    </div>
    <!--\\\\\\\ wrapper end\\\\\\-->

    <script src="/admin/js/jquery-2.1.0.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/common-script.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/plugins/checkbox/zepto.js"></script>
    <script src="/admin/plugins/checkbox/icheck.js"></script>
    <script src="/js/functions.js"></script>

    <script src="/admin/js/jPushMenu.js"></script>
    <script src="/admin/js/side-chats.js"></script>

    </body>
    </html>

@stop