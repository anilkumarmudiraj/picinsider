@extends('admin.layout.base')

@section('content')
    <div class="contentpanel">
        <p></p>
    <div class="container clear_both padding_fix">
    <div class="row">
        <div class="col-md-10">
            <div class="block-web">
                <div class="header">
                    <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                    <h3 class="content-header">Add a general user</h3>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="porlets-content">
        {!! Form::open(['class' => 'form-horizontal row-border']) !!}
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Full Name <span style="color:red">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email address <span style="color:red">*</span></label>
                            <div class="col-sm-9">
                                <input type="email" name="email" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password <span style="color:red">*</span></label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9">
                                <input type="text" name="phone" id="phone" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                <textarea name="address" class="form-control"></textarea>
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" name="city" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">State</label>
                            <div class="col-sm-9">
                                <input type="text" name="state" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zip</label>
                            <div class="col-sm-9">
                                <input type="text" name="zip" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Country</label>
                            <div class="col-sm-9">
                                <select name="country">
                                    @foreach($countries as $country)
                                    <option value="{{$country->name}}">{{$country->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div><!--/form-group-->
                        <div class="bottom">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div><!--/form-group-->
                    {!! Form::close() !!}
                </div><!--/porlets-content-->
            </div><!--/block-web-->
        </div><!--/col-md-6-->
    </div>
    </div>
    </div>
    @stop