@extends('admin.layout.base')

@section('content')
    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>help</h1>
                <h2 class="">Subtitle goes here...</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">EXTRA</a></li>
                    <li class="active">Help</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->

            @foreach($reviews as $review)
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-group accordion accordion-color" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading ">
                                <h4 class="panel-title">
                                    <a class="collapsed caret_color" style="background:#fff; color:#000;" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$review->id}}">
                                        <i class="fa fa-angle-right"></i>
                                        {{$review->subject}} by {{$review->user->name}}
                                    </a>
                                </h4>
                            </div>
                            <div style="height: 0px;" id="collapse{{$review->id}}" class="panel-collapse collapse">
                                <div class="panel-body help_color" style="color:#000;">
                                    <input type="hidden" value="{{csrf_token()}}" id="token">
                                    <div id="reviewSubject" style="color: #000;">Subject : {{$review->subject}}</div>
                                        <div id="review">Review: {{$review->review}} <input type="hidden" id="review_id" value="{{$review->id}}"></div>
                                        <div id="editButton"><button class="btn btn-info" id="showeditbox">Edit</button></div>
                                        <div id="editbox"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                @endforeach
        </div>
        <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <script src="/admin/js/jquery-2.1.0.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/common-script.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/plugins/checkbox/zepto.js"></script>
    <script src="/admin/plugins/checkbox/icheck.js"></script>
    <script src="/js/functions.js"></script>

    <script src="/admin/js/jPushMenu.js"></script>
    <script src="/admin/js/side-chats.js"></script>

    </body>
    </html>

@stop