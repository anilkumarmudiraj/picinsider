@extends('admin.layout.base')
<?php
use Illuminate\Support\Facades\Request;
?>
@section('content')
    <!--\\\\\\\left_nav end \\\\\\-->

    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>Administrator</h1>
                <h2 class="">List of users</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">TABLES</a></li>
                    <li class="active">Statictable</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel default blue_title h2">
                        <div class="panel-heading">Displaying <span class="semi-bold"><?php echo count($users);?><?php $path = Request::path();?>
                                @if($path == 'ADMINISTRATOR/PHOTOGRAPHERS' || $path == 'administrator/photographers')
                                    Photographers</span>

                                    <div class="pull-right dis1">{!! Form::open(['url' => '/administrator/photographer-search','method' => 'GET']) !!} <input type="text" name='searchstring' class="form-control"> <input class="srch1" type="submit" value="Search">{!! Form::close() !!}</div>
                                @else
                                    Users </span>
                                @endif
                         </div>
                        <div class="panel-body">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Member Since</th>
                                    @if($path == 'ADMINISTRATOR/PHOTOGRAPHERS' || $path == 'administrator/photographers')
                                        <th>Featured</th>
                                    @endif
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>@if($user->confirmed == 0) Unconfirmed @else Confirmed @endif</td>
                                        <td>{{$user->role->role}}</td>
                                        <td><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($user->created_at))->diffForHumans() ?></td>
                                        @if($path == 'ADMINISTRATOR/PHOTOGRAPHERS' || $path == 'administrator/photographers')
                                            <td>
                                                @if(isset($user->photographer))
                                                    @if($user->photographer->featured =='Yes')
                                                        <div class="checkbox switch">
                                                            <label><input type="checkbox" class="featured" value="{{$user->id}}" checked="checked"><span class="cs-place"><span class="fa fa-check cs-handler"></span></span></label></div>
                                                        <input type="hidden" id="status" value="No">
                                                    @else
                                                        <div class="checkbox switch"><label><input type="checkbox" value="{{$user->id}}" class="featured"><span class="cs-place"><span class="fa fa-check cs-handler"></span></span></label></div>
                                                        <input type="hidden" id="status" value="Yes">
                                                    @endif
                                                    <input type="hidden" id="token" value="{{ csrf_token() }}">

                                                @endif
                                            </td>
                                            @endif
                                        <td>
                                            @if($user->role->role == 'user' || $user->role->role == 'admin')
                                                <a href="/administrator/user/edit/{{$user->id}}"><button class="btn btn-primary btn-md mb10"> Edit </button></a>
                                                <button id="delete_user" data-target="#exampleModal" data-toggle="modal" data-delete-user_id="{{$user->id}}" class="btn btn-danger btn-md mb10"> Delete </button>

                                            @else
                                                <a href="/administrator/reviews/{{$user->id}}"><button class="btn btn-info btn-md mb10">Reviews</button></a>
                                                <a href="/administrator/photographer/edit/{{$user->id}}"><button class="btn btn-primary btn-md mb10"> Edit </button></a>
                                                <button id="delete_user" data-target="#exampleModal" data-toggle="modal" data-delete-user_id="{{$user->id}}" class="btn btn-danger btn-md mb10"> Delete </button>
                                            @endif
                                        </td>
                                        <!--review code-->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                                            <span class="fa fa-times"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                                <h5>Are you sure you want to delete this user ?</h5>
                                                        <input type="hidden" value="" id="modaldeleteuserid">
                                                        <input type="hidden" value="{{ csrf_token() }}" id="token">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default close2" data-dismiss="modal">No</button>
                                                        <button type="button" id="deleteUser" class="btn btn-success">Yes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--review code-->
                                        <script language="javascript">
                                            $(document).on("click", "#delete_user", function () {
                                                var deleting = $(this).data('delete-user_id');
                                                $(".modal-body #modaldeleteuserid").val( deleting );
                                                $('#addBookDialog').modal('show');
                                            });
                                        </script>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                                <?php echo $users->render();?>
                            </div>
                        </div>
                        <div></div>
                    </section>
                </div>
            </div>

        </div>
        <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
    </div>
    <!--\\\\\\\ inner end\\\\\\-->
    </div>
    <!--\\\\\\\ wrapper end\\\\\\-->

    <!-- Modal -->
    <script src="/admin/js/jquery-2.1.0.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/common-script.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/js/jPushMenu.js"></script>
<script>

$('#deleteUser').click(function() {
	console.log('button clicked');
	var deleting = document.getElementById('modaldeleteuserid').value;
	var token = document.getElementById('token').value;
	$.post('/administrator/deleteUser', {'deleting': deleting, '_token': token}, function(data) {
		var parsed = JSON.parse(data);
		if (parsed == 'success') {
			alert('User deleted successfully');
			location.reload(true);
		}
	});
});
</script>
<!--    <script src="/js/functions.js"></script>-->
    </body>
    </html>
@stop
