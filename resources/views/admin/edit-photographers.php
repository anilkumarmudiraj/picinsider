@extends('admin.layout.base')

@section('content')

<div class="contentpanel">
    <!--\\\\\\\ contentpanel start\\\\\\-->
    <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
            <h1>Administrator</h1>
            <h2 class="">Edit {{$info->name}}</h2>
        </div>
        <div class="pull-right">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">TABLES</a></li>
                <li class="active">Statictable</li>
            </ol>
        </div>
    </div>
    <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->


        <div class="row">
            <div class="col-md-10">
                <div class="block-web">
                    <div class="header">
                        <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                        <h3 class="content-header">We have following details of {{$info->name}}</h3>
                    </div>
                    <div class="porlets-content">
                        {!! Form::open(['class' => 'form-horizontal row-border']) !!}
                        @if(isset($message)) <h3 class="text-center"> {{$message}}</h3> @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label">User Id</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="userid" value="{{$info->id}}" disabled="">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" value="{{$info->name}}">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="email" value="{{$info->email}}">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="phone" value="@if(isset($info->profile->phone)) {{$info->profile->phone}} @endif">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                <textarea name="address" class="form-control">@if(isset($info->profile->address)){{$info->profile->address}}@endif</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">City</label>
                            <div class="col-sm-9">
                                <input type="text" name="city" class="form-control" value="@if(isset($info->profile->city)) {{$info->profile->city}} @endif">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">State</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="state" value="@if(isset($info->profile->state)) {{$info->profile->state}} @endif">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zip/Postal code</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="zip" value="@if(isset($info->profile->zip)) {{$info->profile->zip}} @endif">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Select Box</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="source" name="country">
                                    @if(isset($info->profile->country))
                                    @foreach($countries as $country)
                                    @if($country->name == $info->profile->country)
                                    <option selected="selected" value="{{$country->name}}"> {{$country->name}} </option>
                                    @else
                                    <option value="{{$country->name}}"> {{$country->name}} </option>
                                    @endif
                                    @endforeach
                                    @endif
                                    @foreach($countries as $country)
                                    <option value="{{$country->name}}"> {{$country->name}} </option>
                                    @endforeach
                                </select>
                            </div><!--/col-sm-9-->
                        </div><!--/form-group-->

                        <div class="bottom">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div><!--/form-group-->
                        {!! Form::close() !!}
                    </div><!--/porlets-content-->
                </div><!--/block-web-->
            </div><!--/col-md-6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/contentpanel-->
@stop