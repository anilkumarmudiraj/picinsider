@extends('admin.layout.base')

@section('content')
    <div class="contentpanel">
        <p></p>
    <div class="container clear_both padding_fix">
    <div class="row">
        <div class="col-md-10">
            <div class="block-web">
                <div class="header">
                    <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                    <h3 class="content-header">Help/FAQ’s</h3>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="porlets-content">
        {!! Form::open(['action' => array('Admin\AdminController@processFaq')]) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Question <span style="color:red"></span></label>
                            <div class="col-sm-9">
                                <input type="text" name="question" class="form-control">
                            </div>
                        </div><!--/form-group-->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Answer</label>
                            <div class="col-sm-9">
                                <textarea name="answer" class="form-control"></textarea>
                            </div>
                        </div><!--/form-group-->
                        <div class="bottom">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div><!--/form-group-->
                    {!! Form::close() !!}

                </div><!--/porlets-content-->
            </div><!--/block-web-->
        </div><!--/col-md-6-->
    </div>
    </div>
    </div>
    @stop