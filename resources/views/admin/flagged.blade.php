@extends('admin.layout.base')

@section('content')
    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>Administrator</h1>
                <h2 class="">List of users</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">TABLES</a></li>
                    <li class="active">Statictable</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel default blue_title h2">
                        <div class="panel-heading">Displaying <span class="semi-bold"><?php echo count($flagged);?>
                        </span> </div>
                        <div class="panel-body">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Flag ID</th>
                                    <th>Review ID</th>
                                    <th>Review Subject</th>
                                    <th>Review by</th>
                                    <th>Rating</th>
                                    <th>Review</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($flagged as $flag)
                                    <tr>
                                        <td>{{$flag->id}}</td>
                                        <td>{{$flag->review_id}}</td>
                                        <td>{{$flag->review->subject}}</td>
                                        <td>{{$flag->review->user_name}}</td>
                                        <td>{{$flag->review->stars}} stars</td>
                                        <td>{{$flag->review->review}}</td>
                                        <td>
                                            <button id="deletereview" data-target="#reviewModal" data-toggle="modal" data-delete-review_id="{{$flag->review_id}}" class="btn btn-danger btn-md mb10"> Delete Review</button>
                                            <button id="deleteflag" data-target="#flagModal" data-toggle="modal" data-delete-flag_id="{{$flag->id}}" class="btn btn-danger btn-md mb10"> Delete Flag</button>
                                        </td>

                                    </tr>
                                    <!-- delete review code-->
                                    <div class="modal fade" id="reviewModal" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                                        <span class="fa fa-close"></span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <h3>Are you sure you want to delete this review ?</h3>
                                                    <input type="hidden" value="" id="modaldeletereviewid">
                                                    <input type="hidden" value="{{ csrf_token() }}" id="token">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default close2" data-dismiss="modal">No</button>
                                                    <button type="button" id="deleteReview" class="btn btn-success">Yes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--delete review code-->
                                    <script language="javascript">
                                        $(document).on("click", "#deletereview", function () {
                                            var review = $(this).data('delete-review_id');
                                            $(".modal-body #modaldeletereviewid").val( review );
                                            $('#reviewModal').modal('show');
                                        });
                                    </script>

                                    <!--delete flag code-->
                                    <div class="modal fade" id="flagModal" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                                        <span class="fa fa-close"></span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <h3>Are you sure you want to delete this flag ?</h3>
                                                    <input type="hidden" value="" id="modaldeleteflagid">
                                                    <input type="hidden" value="{{ csrf_token() }}" id="token">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default close2" data-dismiss="modal">No</button>
                                                    <button type="button" id="deleteFlag" class="btn btn-success">Yes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--delete flag code-->
                                    <script language="javascript">
                                        $(document).on("click", "#deleteflag", function () {
                                            var flag = $(this).data('delete-flag_id');
                                            $(".modal-body #modaldeleteflagid").val( flag );
                                            $('#flagModal').modal('show');
                                        });
                                    </script>

                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                                <?php echo $flagged->render();?>
                            </div>
                        </div>
                        <div></div>
                    </section>
                </div>
            </div>

        </div>
        <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
    </div>
    <!--\\\\\\\ inner end\\\\\\-->
    </div>
    <!--\\\\\\\ wrapper end\\\\\\-->

    <!-- Modal -->
    <!-- Modal -->
    <script src="/admin/js/jquery-2.1.0.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/common-script.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/js/jPushMenu.js"></script>
    <script src="/js/functions.js"></script>

@stop