@extends('admin.layout.base')

@section('content')

    <!--\\\\\\\left_nav end \\\\\\-->

    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>Administrator</h1>
                <h2 class="">List of blogs</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="/">Website</a></li>
                    <li><a href="/administrator">Administrator</a></li>
                    <li class="active">Blogs</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->


            <div class="row">
                <div class="col-lg-12">
                    <section class="panel default blue_title h2">
                        <div class="panel-heading">Displaying <span class="semi-bold"><?php echo count($blogs);?><?php $path = Request::path();?>
                                    Blogs</span>
                            <div class="pull-right dis1"> {!! Form::open(['url' => '/administrator/photographer-search','method' => 'GET']) !!} <input type="text" name='searchstring' class="form-control"> <input class="srch1" type="submit" value="Search">{!! Form::close() !!}</div>
                            <a href="/administrator/write-blog"><button class="btn btn-info">Write Blog</button></a>
                        </div>
                        <div class="panel-body">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Blog ID</th>
                                    <th>Title</th>
                                    <th>Story</th>
                                    <th>Photo</th>
                                    <th>Author</th>
                                    <th>Published at</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td>{{$blog->id}}</td>
                                        <td>{{$blog->title}}</td>
                                        <td>{!! strip_tags(substr($blog->body, '0', '100')) !!}</td>
                                        <td><img src="{{$blog->photo}}" width="180px" height="100px"></td>
                                        <td>{{$blog->author}}</td>
                                        <td><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($blog->created_at))->diffForHumans() ?></td>
                                        <td>
                                                <a href="/administrator/edit/blog/{{$blog->id}}"><button class="btn btn-primary btn-md mb10"> Edit </button></a>
                                                <button id="delete_blog" data-target="#exampleModal" data-toggle="modal" data-delete-blog_id="{{$blog->id}}" class="btn btn-danger btn-md mb10"> Delete </button>
                                        </td>
                                        <!--review code-->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                                            <span class="fa fa-close"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4>Are you sure you want to delete this blog ?</h4>
                                                        <input type="hidden" value="" id="modaldeleteuserid">
                                                        <input type="hidden" value="{{ csrf_token() }}" id="token">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default close2" data-dismiss="modal">No</button>
                                                        <button type="button" id="deleteUser" class="btn btn-success">Yes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--review code-->
                                        <script language="javascript">
                                            $(document).on("click", "#delete_user", function () {
                                                var deleting = $(this).data('delete-blog_id');
                                                $(".modal-body #modaldeleteuserid").val( deleting );
                                                $('#addBookDialog').modal('show');
                                            });
                                        </script>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">
                                <?php echo $blogs->render();?>
                            </div>
                        </div>
                        <div></div>
                    </section>
                </div>
            </div>

        </div>
        <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
    </div>
    <!--\\\\\\\ inner end\\\\\\-->
    </div>
    <!--\\\\\\\ wrapper end\\\\\\-->

    <!-- Modal -->
    <script src="/admin/js/jquery-2.1.0.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/common-script.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/js/jPushMenu.js"></script>

    <script src="/js/functions.js"></script>
    </body>
    </html>
    @stop