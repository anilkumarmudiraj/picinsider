@extends('admin.layout.base')
@section('content')
    <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
            <div class="pull-left page_title theme_color">
                <h1>Components</h1>

                <h2 class="">Subtitle goes here...</h2>
            </div>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">FORMS</a></li>
                    <li class="active">Components</li>
                </ol>
            </div>
        </div>
        <div class="container clear_both padding_fix">
            <!--\\\\\\\ container  start \\\\\\-->

            <!-- details of blog -->

            <div class="row">
                <div class="col-md-12">
                    <div class="block-web">
                        <div class="header">
                            <div class="actions"><a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a
                                        class="close-down" href="#"><i class="fa fa-times"></i></a></div>
                            <h3 class="content-header">Blog details</h3>
                        </div>
                        <div class="porlets-content">
                                {!! Form::open(['class' => 'form-horizontal row-border', 'files' => 'true']) !!}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Blog Title</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" value="{{$blog->title}}" name="title">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Author</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" value="{{$blog->author}}" name="author">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Photo</label>

                                    <div class="col-md-8">
                                        <img src="{{$blog->photo}}" width="600px" height="300px">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-8">
                                        <h5>Select file for upload
                                            <div class="btn btn-o btn-default btn-file bc10">
                                                <i class="glyphicon glyphicon-folder-open"></i>
                                                &nbsp;Browse Images
                                                <input type="file" id="1431954496675" data-browse-label="Browse Images" data-browse-class="btn btn-o btn-default" accept="image/jpeg,image/png" data-show-remove="false" data-show-caption="true" data-show-upload="false" multiple="true" name="image" class="file">
                                            </div>
                                        </h5>
                                        <small style="color:red">Choose only if you want to change</small>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="block-web" style="display: inline-block; width: 100%;">
                        <div class="header">
                            <div class="actions"> <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a><a href="#" class="close-down"><i class="fa fa-times"></i></a> </div>
                            <h3 class="content-header">Story</h3>
                        </div>
                        <div class="porlets-content">
                            <div class="form">
                                <!--<form action="#" class="form-horizontal">-->
                                    <div class="form-group">
                                        <label class="col-sm-2 col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control ckeditor" name="body" rows="6">{{$blog->body}}</textarea>
                                        </div>
                                    </div>
                                <!--</form>-->
                            </div>
                        </div><!--/porlets-content-->

                    </div><!--/block-web-->
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-success">
                        <a href="/administrator/blogs"></a><button type="button" value="Save" class="btn btn-danger">Cancel</button></a>
                    </div>
                    {!! Form::close() !!}
                </div><!--/col-md-12-->
            </div><!--/row-->
        </div>
        <!--\\\\\\\ content panel end \\\\\\-->
    </div>

    <script src="/admin/js/jquery-2.1.0.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/common-script.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="/admin/plugins/toggle-switch/toggles.min.js"></script>
    <script src="/admin/plugins/checkbox/zepto.js"></script>
    <script src="/admin/plugins/checkbox/icheck.js"></script>
    <script src="/admin/js/icheck-init.js"></script>
    <script src="/admin/js/jquery.slimscroll.min.js"></script>
    <script src="/admin/js/icheck.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript"
            src="/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="/admin/js/form-components.js"></script>
    <script type="text/javascript" src="/admin/plugins/input-mask/jquery.inputmask.min.js"></script>
    <script type="text/javascript" src="/admin/plugins/input-mask/demo-mask.js"></script>
    <script type="text/javascript" src="/admin/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
    <script type="text/javascript" src="/admin/plugins/dropzone/dropzone.min.js"></script>
    <script type="text/javascript" src="/admin/plugins/ckeditor/ckeditor.js"></script>

    </body>
    </html>
@stop