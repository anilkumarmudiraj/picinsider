@extends('layout.base')
<?php
use Illuminate\Support\Str;
?>
@section ('content')

<div class="container">
    <div class="row">
        <div class="col-md-7 spark_photo">
        <h2>FAQ</h2>
        </div>
    </div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
    <div class="bs-example" data-example-id="collapse-accordion">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php foreach($faq as $allfaq){?>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" onclick="return showAndHide(<?php echo $allfaq['id'];?>)">
          <h4 class="panel-title">
            <a role="button" >
              <?php echo $allfaq['question'];?>
            </a>
          </h4>
        </div>
        <div id="collapseOne_<?php echo $allfaq['id'];?>" style="display: none;">
          <div class="panel-body">
              <?php echo $allfaq['answer'];?>
          </div>
        </div>
      </div>
<?php }?>
    </div>
  </div>

    </div>
    </div>
</div>
   @stop

<script>
      function showAndHide(toggleit){
        $('#collapseOne_'+toggleit).toggle();
      }
  </script>
