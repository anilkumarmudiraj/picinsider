@extends('../layout.base')

@section('content')

    <div class="container business one">
        <div class="row">
            <div class="col-md-12"><h2>Register your photography business in less than 5 minutes</h2></div>
            <div class="col-md-6">
                <h4><strong>Registration is free and offers many benefits:</strong></h4>
                <ul class="benefit-lists">
                    <li>- Increase your earnings potential</li>
                    <li>- Build a recognizable brand</li>
                    <li>- Manage your business profile</li>
                    <li>- Receive reviews and respond in realtime</li>
                    <li>- Use dashboard tool to get more visibility & reviews</li>
                    <li>- Learn about new photography gigs</li>
                    <li>- Receive tips to grow your busines</li>
                </ul>
            </div>
            <div class="col-md-6">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open() !!}
                    <div class="form-group">
                        <label>Company name <span style="color: red">*</span></label>
                        <input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" id="company_name">
                        <script language="javascript">
                            $('#company_name').bind('keyup blur',function(){
                                        var node = $(this);
                                        node.val(node.val().replace(/[^0-9a-zA-Z\s]*$/,'') ); }
                            );
                        </script>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>First name <span style="color: red">*</span> </label>
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" id="first_name">
                                <script language="javascript">
                                    $('#first_name').bind('keyup blur',function(){
                                                var node = $(this);
                                                node.val(node.val().replace(/[^a-zA-Z\s]*$/,'') ); }
                                    );
                                </script>
                            </div>
                            <div class="col-md-6">
                                <label>Last name <span style="color: red">*</span> </label>
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" id="last_name">
                                <script language="javascript">
                                    $('#last_name').bind('keyup blur',function(){
                                                var node = $(this);
                                                node.val(node.val().replace(/[^a-zA-Z\s]*$/,'') ); }
                                    );
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Contact email address <span style="color: red">*</span> </label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label>Password <span style="color: red">*</span> </label>
                        <input type="password" class="form-control" name="password">
                    </div>
                    <div class="form-group">
                        <label>Confirm password <span style="color: red">*</span> </label>
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Country <span style="color: red">*</span></label>
                                <select class="form-control" style="width:300px;" name="country">
                                    @foreach($countries as $country)
                                    <option value="{{$country->name}}">{{$country->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <p class="text-center"><em>By creating an account, you agree to PicInsider’s <br> Terms of Service and Privacy Policy.</em></p>
                    <div class="form-group">
                        {{--<a href="#" class="btn btn-default enq_form"><i class="fa fa-user-plus"></i> Register</a>--}}
                        <button type="submit" class="btn btn-default enq_form"><i class="fa fa-user-plus"></i>Register</button>
                        <p class="text-right registered"><em>Already registered? <a href="/login">Login</a></em></p>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop