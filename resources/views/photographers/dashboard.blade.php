@extends('../layout.base')

@section('content')
    {!! HTML::script('/js/phonefunction.js') !!}
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br/>

                <div class="bs-example">
                    <ul class="nav nav-tabs tab">
                        <li><a data-toggle="tab" href="#dashboard">Company dashboard</a></li>
                        <li><a data-toggle="tab" href="#profile">Profile</a></li>
                        <li><a data-toggle="tab" href="#reviews">Reviews</a></li>
                        <li><a data-toggle="tab" href="#photos">Photos</a></li>
                        <li><a data-toggle="tab" href="#gigs">Gigs</a></li>
                        <li><a data-toggle="tab" href="#messages">Messages</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <!-- pasting here -->
                @foreach($getProfile as $profile)
                    <div class="tab-content tablist">
                        <div class="tab-pane fade in active" id="dashboard">
                            <div class="reviews_list">
                                <h3>About Us</h3>
                                <p>{{$profile->description}}</p>
                            </div>
                        </div>
                        <div id="profile" class="tab-pane fade">
                            <h3>Your Details
                                <div class="pull-right small">
                                    <button data-toggle="modal" data-target="#myModal2" id="delete_user"
                                            class="btn btn-danger btn-md mb10"> Click here to change your password
                                    </button>
                                </div>
                            </h3>
                            <hr/>

                            {!! Form::open(['class' => 'form-horizontal']) !!}
                            <div class="form-group">

                                <label class="col-sm-4 control-label">Name</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="name" value="{{$profile->name}}">
                                    <input type="hidden" value="{{$profile->user_id}}" name="userid">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Company Name</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="company_name" value="{{$profile->company_name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="email" name="email" value="{{$profile->user->email}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Phone</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" id="phone" name="phone" value="@if(isset($profile->phone_number)){{$profile->phone_number}}@endif">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Website</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="website" value="@if(isset($profile->website)){{$profile->website}}@endif">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">City</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="city" value="@if(isset($profile->city)){{$profile->city}}@endif">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Established</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="established" value="@if(isset($profile->established)){{$profile->established}}@endif">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Studio ?</label>
                                <div class="col-sm-8">
                                    <select name="studio" class="form-control">
                                        @if($profile->studio =='Yes')
                                        <option value="Yes" selected="selected">Yes</option>
                                            <option value="No">No</option>
                                        @else
                                            <option value="Yes">Yes</option>
                                        <option value="No" selected="selected">No</option>
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Average price</label>
                                <div class="col-sm-8">
                                    <!-- <input class="form-control" type="text" name="price" value="@if(isset($profile->price)){{$profile->price}}@endif"> -->
                                    <select name="price" class="form-control">
                                        <option value="0-1000">$ ($0 – $1000)</option>
                                        <option value="1000-2000">$$ ($1000 – $5000)</option>
                                        <option value="2000-3000">$$$ ($5000+)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Interested for gigs under $500 ?</label>
                                <div class="col-sm-8">
                                    <select name="under500" class="form-control">
                                        @if($profile->under500 =='Yes')
                                            <option value="Yes" selected="selected">Yes</option>
                                            <option value="No">No</option>
                                        @else
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">No</option>
                                        @endif
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-4 control-label">Interested for gigs under $1000 ?</label>
                                <div class="col-sm-8">
                                    <select name="under1000" class="form-control">
                                        @if($profile->under1000 =='Yes')
                                            <option value="Yes" selected="selected">Yes</option>
                                            <option value="No">No</option>
                                        @else
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">No</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Facebook</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="facebook" value="{{$profile->facebook}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Twitter</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="twitter" value="{{$profile->twitter}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pinterest</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="pinterest" value="{{$profile->pinterest}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Google Plus</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="gplus" value="{{$profile->gplus}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Instagram</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="instagram" value="{{$profile->instagram}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Favorite brand of lens/camera</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="brand" value="{{$profile->brand}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Self Description</label>
                                <div class="col-sm-8">
                                    <textarea name="description" class="form-control" rows="4">{{$profile->description}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Upload photo</label>
                                <img src="{{ $profile->photo }}" style="width: 130px; margin-left: 16px;">
                                <div class="col-sm-8" style="margin-left: 292px;">
                                    <div class="btn btn-o btn-default btn-file bc10"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse Images <input type="file" id="1431954496675" data-browse-label="Browse Images" data-browse-class="btn btn-o btn-default" accept="image/jpeg,image/png" data-show-remove="false" data-show-caption="false" data-show-upload="false" multiple="true" name="image" class="file"><small>Please select photo only if you want to change</small></div>
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Subscribe to newsletter ?</label>
                                <div class="col-sm-8">
                                    <select name="subscribe" class="form-control">
                                        @if($profile->subscribe =='Yes')
                                            <option value="Yes" selected="selected">Yes</option>
                                            <option value="No">No</option>
                                        @else
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">No</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Want notifications for new gig ?</label>
                                <div class="col-sm-8">
                                    <select name="notification" class="form-control">
                                        @if($profile->notification =='Yes')
                                            <option value="Yes" selected="selected">Yes</option>
                                            <option value="No">No</option>
                                        @else
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">No</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8">
                                    <input type="submit" value="Update" class="btn btn-success pull-right">
                                </div>
                            </div>

                        </div>



                        <!-- Modal for password update -->
                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form id="change_password">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                                    </div>
                                    <div class="modal-body db">
                                        <div class="form-group db">
                                            <label class="col-sm-6 control-label">Current Password </label>
                                            <div class="col-sm-6">
                                                <input type="password" id="current_password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">New Password </label>
                                            <div class="col-sm-6">
                                                <input type="password" name="password" id="new_password" class="form-control">
                                                <input type="hidden" value="{{ csrf_token() }}" id="csrftoken">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">Confirmed Password </label>
                                            <div class="col-sm-6">
                                                <input type="password" name="password_again" id="confirm_password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="error" id="message"></div>
                                        </div>

                                    </div>
                                    <div class="modal-footer m10">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="updatePassword">Save changes</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <!-- Modal for password update ends
                             My account section starts -->

                        <div id="reviews" class="tab-pane fade">
                            @foreach($profile->reviews as $review)
                                <div class="reviews_list" id="review_list">
                                    <h4>{{$review->subject}}</h4>

                                    <div class="tab_rating"><span>{{$review->user_name}}
                                            - <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($review->created_at))->diffForHumans() ?></span>
                                        <ul class="tab_spark_rating">
                                            @for($i=1; $i<= $review->stars; $i++)
                                                <li><a href="#"><span class="glyphicon glyphicon-star"
                                                                      aria-hidden="true"></span></a></li>
                                            @endfor
                                        </ul>

                                    </div>
                                    <p>{{$review->review}} id is {{$review->id}}</p>

                                    @foreach($review->ratinglikes as $ratinglike)
                                        Rating {{$ratinglike->likes}}
                                    @endforeach
                                    <p class="like_btn">2 out of 3 people found this helpful. @if(Auth::check())Did you
                                        find this helpful?
                                        <a class="likeButton btn btn-default like"
                                           data-review_id="{{$review->id}}"><span
                                                    class="glyphicon glyphicon-thumbs-up"></span></a>
                                        <a class="dislikeButton btn btn-default unlike"
                                           data-review_id="{{$review->id}}"><span
                                                    class="glyphicon glyphicon-thumbs-down"></span></a>
                                        <input type="hidden" id="liker_id" value="{{Auth::user()->id}}">
                                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                                        @endif
                                    </p>

                                    <p class="com_res">Company response: Thank you Chris for your kind comments. We look
                                        forward to working with you on future engagements.</p>

                                    <div id="message"></div>
                                    <button class="reportReviewButton btn btn-danger pull-right"
                                            data-review_flag_id="{{$review->id}}">Report this review
                                    </button>
                                    <input type="hidden" value="{{ csrf_token() }}"/>
                                    <br/>
                                </div>
                            @endforeach
                            <div class="review-pagetab" data-example-id="review-pagination">
                                <nav>
                                    <ul class="pagination">
                                    </ul>
                                </nav>
                            </div>
                        </div>


                        <!-- My account section ends -->
                        <!-- Photos section starts -->
                        <div class="tab-pane fade" id="photos">
                            {!! Form::open(['url' => '/photographer/manage-photos', 'files' => 'true']) !!}
                            <div class="">
                                <div class="pg-dash-in">
                                    <h5>Select file for upload</h5><small>(You may select multiple files for upload)</small>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                    <input type="file" name="images[]" multiple="true"></div>
                                        <div class="col-sm-4">
                                        <input type="submit" class="btn btn-success" value="Upload"></div>
                                        </div>
                                </div>
<p></p>
                                    @foreach($profile->photos as $photo)
                                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                            <a href="#" class="thumbnail">
                                                <img alt="" src="https://s3-us-west-2.amazonaws.com/artisanbucket/uploads/{{$photo->photo_url}}" class="img-responsive" style="width:265px; height:180px">
                                            </a>
                                        </div>
                                    @endforeach
                            </div>
                            {!! Form::close() !!}

                        </div>
                        <!-- photos section ends -->
                        <div id="gigs" class="tab-pane fade">
                            <h3>Section D</h3>

                            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id
                                gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta
                                ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit
                                sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus
                                adipiscing.</p>
                        </div>

                        <div id="messages" class="tab-pane fade">
                            <h3>Messages</h3>
                            @if(count($profile->messages) > 0)
                                @foreach($profile->messages as $message)
                                    Message From : {{$message->from_name}}
                                    <br />
                                    Email : {{$message->email}}
                                    <br />
                                    Connect via : {{$message->connect}}
                                    <br />
                                    Message : {{$message->message}}
                                @endforeach
                            @else
                                <h3> No messages for you</h3>
                            @endif
                        </div>
                    </div>
                    <!-- pastig ends -->
            </div>
            @endforeach
            <div class="col-md-3">
                <div class="pg-dashboard-right">
                    <div class="con_view">
                        <h3 class="tips">Updatae <span>your profile </span></h3>

                        <div class="pg-dashboard-right-in">
                            <img src="/images/pg-d1.png" width="169" height="106" alt="">

                            <div class="clearfix"></div>
                            <p>Users read your profile when deciding who to hire.</p>

                            <p>Your profile is 33% complete </p>

                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0"
                                     aria-valuemax="100" style="width: 33%;">
                                    <span class="sr-only">33% Complete</span>
                                </div>
                            </div>
                            <a href="#profile"><input type="button" value="UPDATE PROFILE"/></a>
                        </div>

                    </div>
                </div>
                <div class="con_view">
                    <h3 class="tips">Tips <span>to Consider</span></h3>
                    <ul>
                        <li>1. Contact several photoraphers since quantities are always limited.</li>
                        <li>2. Smaller photographer may be wiling to charge less than arger stores which have larger
                            overhead.
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
@stop