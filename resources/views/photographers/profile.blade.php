@extends('../layout.base')
<?php
use Illuminate\Support\Str;
?>
@section('content')
    {!! HTML::style('/lib/jquery.raty.css') !!}
    {!! HTML::script('/lib/jquery.raty.js') !!}
    {!! HTML::script('/js/phonefunction.js') !!}
    <script src='https://www.google.com/recaptcha/api.js'></script>
        <div class="container">
        <div class="row spark_one">
            <div class="col-md-7 spark_photo">
                <h2>{{$information->company_name}}</h2>
                <ul class="spark_rating">
                    <?php $ratings = round($rating); ?>
                    @for($i=1;$i<=$ratings; $i++)
                    <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                    @endfor
                    <li><?php echo count($reviews);?> review(s)</li>
                </ul>
                <div class="spark_buttons">
                    @if(Auth::check() && Auth::user()->role->role != 'photographer')
                    <button type="button" class="btn btn-default glyphicon glyphicon-plus" data-toggle="modal" data-dismiss="#inviteModal" data-target="#inviteModal"><div class="inviteButton">Invite to gig</div></button>
                    @endif
                   <a href="#contactPhotgrapher"> <button type="button" class="btn btn-default sam"><span>Send a Message</span></button></a>
                    <a href="{{$information->website}}" target="_blank"><button type="button" class="btn btn-default vmw"><span>Visit My Website</span></button></a>
                    <button type="button" class="btn btn-default atf" id = "myFavorites"><span>Add To Favorites</span></button>
                </div>
            </div>
            <div class="col-md-5">

                <div class="wreview">
                    <button type="button" class="btn btn-default wreview_btn" data-toggle="modal" data-target="#exampleModal" id="write_review" data-review-user_id="{{$information->user->id}}">Write A Review</button>
                </div>

                @if(Auth::check())
                <!-- Invite to gig modal -->
                    <!--review code-->
                    <div class="modal fade" id="inviteModal" tabindex="-1" role="dialog"
                         aria-labelledby="inviteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                        <span class="fa fa-times"></span></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="subject" class="control-label">Select Gig  :</label>
                                            <select name="gigs" class="form-control" readonly id="gig_id">
                                                @foreach($gigsByUser as $gig)
                                                    <option name="{{$gig->short}}" value="{{$gig->id}}">{{$gig->short}}</option>
                                                @endforeach
                                            </select>
                                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                                            <input type="hidden" id="photographer_id" value="{{$information->user_id}}">
						 <input type="hidden" id="user_id" value="{{Auth::user()->id}}">

                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Close</button>
                                    <button type="button" class="gig-invite btn btn-success">Invite</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- invite to gig code ends -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close1" data-dismiss="modal" aria-label="Close">
                                        <span class="fa fa-close"></span></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="subject" class="control-label">short description :</label>
                                            <input type="text" class="form-control" id="review_subject" placeholder="Max 140 characters">
                                            <input type="hidden" id="review_photographer_id" value="{{$information->user_id}}">
                                            <input type="hidden" id="review_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="name">Name :</label>
                                            <input type="text" class="form-control" id="review_user_name" value="{{Auth::user()->name}}" placeholder="Name" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="name">Review :</label>
                                            <textarea class="form-control" rows="3" id="review_review" placeholder="Please provide a detailed description of your experience including photographer’s attitude, pricing, style and punctuality"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="name">Rating :</label>
                                            <div class="stars"></div>
                                            <script type="text/javascript">$('div.stars').raty();</script>
                                        </div>
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="6Ldj-ggTAAAAAJiE99nsgM2ENsKMyUj0DQMBQQ0_"></div>
                                        </div>
                                        <div id="review_response"></div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Close</button>
                                    <button type="button" id="write-review" class="btn btn-success">Post Review</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--review code-->
                    <script language="javascript">
                        $(document).on("click", "#write_review", function () {
                            var myBookId = $(this).data('review-user_id');
                            $(".modal-body #review_photographer_id").val( myBookId );
                            $('#addBookDialog').modal('show');
                        });
                    </script>

                @else
<!-- login modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close1 pull-right" data-dismiss="modal" aria-label="Close">
                                        <span class="fa fa-close"></span></button>
                                    <h4>Please login/register</h4>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="subject" class="control-label">Email :</label>
                                            <input type="email" class="form-control" id="modalEmail" placeholder="Email address">
                                            <input type="hidden" id="review_token" value="{{ csrf_token() }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="name">Password :</label>
                                            <input type="password" class="form-control" id="modalPassword" value="" placeholder="Password">
                                        </div>
                                        <div id="modalMessage"></div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <a href="/register"><button type="button" class="btn btn-default">Register</button></a>
                                    <button type="button" id="modalLogin" class="btn btn-success">Login</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- login modal ends here -->
                    @endif()

                    <div class="wreview">
                        </div>
                <div class="share_bookmark spark_buttons">
                    <button type="button" class="btn btn-default share"><span>Share</span></button>
                    <button type="button" class="btn btn-default bookm"><span>Bookmark</span></button>
                </div>
            </div>
        </div>
        <div class="row spark_two">
            <div class="col-md-7 col-md-52">
                <div class="col-sm-6 spark_pic1 row"><img src="/uploads/photographers/{{$information->photo}}"/></div>
                <div class="col-sm-6 spark_pic1_btn row">
                    <ul>
                        <li>Estabilished : {{$information->established}}</li>
                        @if(isset($photographercategories))
                        <li>Type of Photography</li>
                        <li>
                            @foreach($photographercategories as $categories)
                            <button type="button" class="btn btn-default">{{$categories->name}}</button>
                            @endforeach
                        </li>
                            @endif
                    </ul>
                    <ul>
                        <li>Favorite brands : {{$information->brand}}</li>
                        @if(isset($services))
                        <li>Other Services</li>
                        <li>
                            @foreach($services as $service)
                            <button type="button" class="btn btn-default">{{$service->name}}</button>
                            @endforeach
                        </li>
                            @endif
                    </ul>
                </div>
                <div class="col-md-12 row spark_media">
                    <ul>
                        <li><a href="https://facebook.com/{{$information->facebook}}" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a></li>
                        <li><a href="https://twitter.com/{{$information->twitter}}" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
                        <li><a href="https://pinterest.com/{{$information->pinterest}}" target="_blank"><i class="fa fa-pinterest-p fa-2x"></i></a></li>
                        <li><a href="https://plus.google.com/{{$information->gplus}}" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a></li>
                        <li><a href="https://instagram.com/{{$information->instagram}}" target="_blank"><i class="fa fa-instagram fa-2x"></i></a></li>
                    </ul>
                </div>
                <div style="clear:both;"></div>
                @if($information->certified == '1')
                <div class="certified_ph">
                    <img src="/images/photo_tips/certified.png"/>
                    <h3>Certified Photographer</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit euismod aliqua dolorem setum sentral pengobatan sinsei bah ra ngoros mugo iki kontes seng dadi menang.</p>
                </div>
                    @endif

            </div>
            <div class="col-md-5 col-md-47">
                <!---Slider-->


                <div id="main" role="main">
                    <section class="slider">
                        <div id="slider" class="flexslider main_slider">
                            <ul class="slides ">
                                <li>
                                    <img src="/images/slider/sld1.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld2.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld3.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld4.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld5.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld6.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld7.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld8.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld9.jpg" />
                                </li>
                                <li>
                                    <img src="/images/slider/sld10.jpg" />
                                </li>

                            </ul>
                        </div>
                        <div id="carousel" class="flexslider">
                            <ul class="slides thumb">
                                <li>
                                    <img src="/images/slider/sld1_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld2_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld3_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld4_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld5_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld6_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld7_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld8_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld9_thumb.jpg">
                                </li>
                                <li>
                                    <img src="/images/slider/sld10_thumb.jpg">
                                </li>
                            </ul>
                        </div>

                        <!-- endfi -->
                    </section>
                    <aside>
                    </aside>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="bs-example">
                    <ul class="nav nav-tabs tab">
                        <li class="active"><a data-toggle="tab" href="#aboutus">About Us</a></li>
                        <li><a data-toggle="tab" href="#reviews">Reviews (<?php echo count($reviews);?>)</a></li>
                        <li><a data-toggle="tab" href="#photos">Photos</a></li>
                        <li><a data-toggle="tab" href="#faq">Faq</a></li>
                        @if(Auth::check() && Auth::user()->id != $information->user_id)
                        <li class="write_review pull-right"><button type="button" data-toggle="modal" data-target="#exampleModal" id="write_review" data-review-user_id="{{$information->user_id}}" class="btn btn-default">Write a Review</button></li>
                            @endif
                    </ul>
                    <div class="tab-content tablist">
                        <div id="reviews" class="tab-pane fade in">
                            @foreach($reviews as $review)
                            <div class="reviews_list" id="review_list">
                                <h4>{{$review->subject}}</h4>
                                <div class="tab_rating"><span>{{$review->user_name}} - <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($review->created_at))->diffForHumans() ?></span>
                                    <ul class="tab_spark_rating">
                                        @for($i=1; $i<= $review->stars; $i++)
                                            <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                        @endfor
                                    </ul>

                                </div>
                                <p>{{$review->review}} id is {{$review->id}}</p>
                                <?php $totalLikes = 0; ?>
                                @foreach($review->ratinglikes as $ratinglike)
                                    <?php $totalLikes += $ratinglike->likes; ?>
                                @endforeach
                                <p class="like_btn"><span id="total_likes">{{ $totalLikes }}</span> people found this helpful. @if(Auth::check())Did you find this helpful?
                                    <a class="likeButton btn btn-default like" data-review_id="{{$review->id}}"><span class="glyphicon glyphicon-thumbs-up"></span></a>
                                    <a class="dislikeButton btn btn-default unlike" data-review_id="{{$review->id}}"><span class="glyphicon glyphicon-thumbs-down"></span></a>
                                                                                              <input type="hidden" id="liker_id" value="{{Auth::user()->id}}">
                                                                                              <input type="hidden" id="token" value="{{ csrf_token() }}">
                                                                                              @endif
                                </p>
                                <p class="com_res">Company response: Thank you Chris for your kind comments. We look forward to working with you on future engagements.</p>
                                <div id="message"></div>
                                @if(Auth::check() && $information->user_id == Auth::user()->id)
                                <button class="reportReviewButton btn btn-danger pull-right" data-review_flag_id="{{$review->id}}">Report this review</button>
                                <input type="hidden" value="{{ csrf_token() }}" />
                                <br />
                                @endif
                            </div>
                            @endforeach
                            <div class="review-pagetab" data-example-id="review-pagination">
                                <nav>
                                    <ul class="pagination">
                                    </ul>
                                </nav>
                            </div>
                        </div>

                        <div id="aboutus" class="tab-pane fade in active">
                            <p></p>
                            <p>{{$information->description}}</p>
                        </div>
                        <div id="photos" class="tab-pane fade">
                            <p></p>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#">
                                    <img class="img-responsive" src="http://placehold.it/400x300" alt="">
                                </a>
                            </div>
                        </div>
                        <div id="faq" class="tab-pane fade">
                            <h3>Section D</h3>
                            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div id="contactPhotgrapher" class="sidebar_form">
                    <h2>Contact this Photographer  </h2>
                    <form role="form" id="contact_us">
                        @if(Auth::check())
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" value="{{Auth::user()->name}}">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" id="email" placeholder="{{Auth::user()->email}}">
                            <input type="hidden" id="to_id" value="@if(!empty($information->user_id)){{$information->user_id}}@endif">
                            <input type="hidden" id="to_name" value="@if(!empty($information->name)){{$information->name}}@endif">
                            <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                        </div>
                        @else
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Full Name">
                                <input type="hidden" id="to_id" value="@if(!empty($information->user_id)){{$information->user_id}}@endif">
                                <input type="hidden" id="to_name" value="@if(!empty($information->name)){{$information->name}}@endif">
                                <input type="hidden" id="token" value="{{ csrf_token() }}">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email@email.com">
                            </div>
                            @endif
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" id="phone_num" placeholder="Phone number">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="comment" rows="3" id="comment" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group">
                            <select name="connect" class="form-control" id="connect">
                                <!--<option value=""></option>-->
                                <option value="Send email">Send me an email</option>
                                <option value="Call me">Call me</option>
                                <!--<option value="Internal Message">Message me on Pic Insider</option>-->
                            </select>
                        </div>
                        <button type="submti" id="sendMessage" class="btn btn-default enq_form">Send My Message</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="midd_part"></div>
        <div class="row toronto">
            @if(count($suggested) > 0 )
                <h2>Checkout other Pic<span>Insider</span> Photographers</h2>
                @foreach($suggested as $suggest)
            <div class="col-sm-6 col-lg-3">
                <div class="imageOuter" style="margin:0; width:265px; height:180px;">
                    <a class="image"  href="/photographer/{{$suggest->user_id}}/<?php echo Str::slug($suggest->name);?>">
                        <img class="imgborder" alt="" src="/uploads/photographers/{{$suggest->photo}}" width="265px" height="180px">
                    </a>
                </div>
                <div class="imagebottom">
                    <ul>
                        <li><a href="/photographer/{{$suggest->user_id}}/<?php echo Str::slug($suggest->name);?>"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                    </ul>
                    <span><a href="/photographer/{{$suggest->user_id}}/<?php echo Str::slug($suggest->name);?>">{{$suggest->company_name}}</a></span>
                    <p>Favorite brand: {{$suggest->brand}}<br/>
                        Categories:@foreach($suggest->photographercategory as $category) {{$category->name}},@endforeach</p>
                </div>
            </div>
            @endforeach
            @endif
            </div>
        </div>
    </div>

<script>
$('#myFavorites').click(function() {
        console.log('button clicked');
        var photographerId = document.getElementById('photographer_id').value;
        var userId = document.getElementById('user_id').value;
	var token = document.getElementById('token').value;
    $.ajax({
	url: '/photographer/favphotographer/'+userId+'/'+photographerId,
	type: "post",
	data: {_token: token},
	      success: function(data){
        	alert(data);
      }
    }); 
        
});

</script>


 @stop

