@extends('../layout.base')

@section('content')

    <div class="container business one">
        <div class="row">
            <div class="col-md-12"><h2>Login to explore the world of possibilities</h2></div>
            <div class="col-md-6">
                <h4><strong>Registration is free and offers many benefits:</strong></h4>
                <ul class="benefit-lists">
                    <li>- Increase your earnings potential</li>
                    <li>- Build a recognizable brand</li>
                    <li>- Manage your business profile</li>
                    <li>- Receive reviews and respond in realtime</li>
                    <li>- Use dashboard tool to get more visibility & reviews</li>
                    <li>- Learn about new photography gigs</li>
                    <li>- Receive tips to grow your busines</li>
                </ul>
            </div>
            <div class="col-md-6">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open() !!}
                <div class="form-group">
                    <label>Email address</label>
                    <input type="text" class="form-control" name="email" id="email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <p class="text-center"><em>By creating an account, you agree to PicInsider’s <br> Terms of Service and Privacy Policy.</em></p>
                <div class="form-group">
                    {{--<a href="#" class="btn btn-default enq_form"><i class="fa fa-user-plus"></i> Register</a>--}}
                    <button type="submit" class="btn btn-default enq_form"><i class="fa fa-user-plus"></i>Register</button>
                    <p class="text-right registered"><em>Already registered? <a href="/login">Login</a></em></p>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop