@extends('../layout.base')

@section('content')

    <div class="container business one">
        <div class="row">
            <div class="col-md-12"><h2>Complete your profile - {{$name}}</h2></div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['class' => 'form-horizontal', 'files' => 'true']) !!}
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 txt-align control-label">Website (if applicable) :</label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="website" placeholder="www.sparkphotography.com" value="{{ old('website') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 txt-align control-label">Year established :</label>

                <div class="col-sm-9">
                    <input type="number" min="1900" max="2015" class="form-control" id="established" name="year" value="{{ old('year') }}" placeholder="YYYY">
                    <script language="javascript">
                        $('#established').bind('keyup blur',function(){
                                    var node = $(this);
                                    node.val(node.val().replace(/[^0-9\s]*$/,'') ); }
                        );
                    </script>
                </div>
            </div>


            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 txt-align control-label">Company Phone No. :</label>

                <div class="col-sm-9">
                    <input type="text" class="phone form-control" name="phone" placeholder="212-222-2222" value="{{ old('phone') }}" id="phone">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 txt-align control-label">Postal/Zip Code :</label>

                <div class="col-sm-9">
                    <input type="text" class="form-control" name="zip" placeholder="M5M2K2" value="{{ old('zip') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 txt-align control-label">City/Town : <span style="color:red">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="typeahead form-control" name="city" autocomplete="off">
                </div>
            </div>


            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 txt-align control-label">Speciality<span style="color:red;">*</span> (check all that apply)
                    :</label>

                <div class="col-md-6">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" name="field[]" value="Wedding"> Wedding
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox2" name="field[]" value="Event"> Event
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" name="field[]" value="product"> Product
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox4" name="field[]" value="Engagement"> Engagement
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox5" name="field[]" value="New born"> Newborn
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 txt-align control-label">Favourite Brand or Lens :</label>

                <div class="col-sm-9">
                    <input type="text" name="brand" class="form-control" value="{{ old('brand') }}" placeholder="Brand of camera or lens">
                </div>
            </div>


            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 txt-align control-label">Studio Available :</label>

                <div class="col-sm-9">
                    <select name="studio" class="form-control">
                        <option value="Yes">Yes</option>
                        <option value="No" selected="selected">No</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 txt-align control-label">Price (avg full day shoot) :</label>

                <div class="col-sm-9">
                    <select name="price" class="form-control">
                        <option value="0-1000">$ ($0 – $1000)</option>
                        <option value="1000-2000">$$ ($1000 – $5000)</option>
                        <option value="2000-3000">$$$ ($5000+)</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 txt-left control-label">Social media (please specify username)
                    :</label>
            </div>

            <div class="form-group">
                <div class="col-sm-3 txt-right"><span class="fa fa-facebook-square"></span> Facebook :</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="facebook" value="{{ old('facebook') }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-3 txt-right"><span class="fa fa-twitter"></span> Twitter :</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="twitter" value="{{ old('twitter') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 txt-right"><span class="fa fa-instagram"></span> Instagram :</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="instagram" value="{{ old('instagram') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 txt-right"><span class="fa fa-google-plus"></span> Google Plus :</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="gplus" value="{{ old('gplus') }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-3 txt-right"><span class="fa fa-pinterest"></span> Pinterest :</div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="pinterest" value="{{ old('pinterest') }}">
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-3 txt-align"></div>
                <div class="col-sm-9">
                    <div class="file-input file-input-new"><input type="hidden">

                        <div class="file-preview ">
                            <div class="file-preview-status text-center text-success"></div>
                            <div class="file-preview-thumbnails"></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="btn btn-o btn-default btn-file bc10"><i class="glyphicon glyphicon-folder-open"></i> Upload Main profile photo
                            <input name="photo" type="file" class="file" multiple data-show-upload="false" data-show-caption="false" data-show-remove="false"
                                   accept="image/jpeg,image/png" data-browse-class="btn btn-o btn-default"
                                         data-browse-label="Browse Images" id="1431954496675"></div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="col-sm-3"></div>
                <div class="col-sm-7">
                    <div class=" lh1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="" name="under500" class="checkbox1" value="Yes">
                                Willing to work on gigs under $500
                            </label>
                        </div>
                    </div>
                    <div class=" lh1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="" name="under1000" class="checkbox1" value="Yes">
                                Willing to work on gigs under $1000
                            </label>
                        </div>
                    </div>
                    <div class=" lh1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="" class="checkbox1" name="tips" value="Yes">
                                Yes, I want to receive offers and tips from
                                PicInsider I may un-subscribe at any time
                            </label>
                        </div>
                    </div>

                    <div class=" lh1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="" class="checkbox1" name="notification" value="Yes">
                                Get notified of new gigs posted in your city
                            </label>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-default enq_form"> save and continue</button>
                </div>

            </div>


            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <p class="text-center"><em>PicInsider Privacy Promise: We are very concerned about privacy
                            and will never share your email, phone, postal code / zip code with anyone.
                            See our privacy policy and terms of use.</em></p>
                </div>

            </div>
            {!! Form::close() !!}

        </div>
    </div>

@stop