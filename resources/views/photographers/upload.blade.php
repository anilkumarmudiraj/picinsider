@extends('../layout.base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-menu">
                    <ul>
                        <li><a href="#">company dashboard</a></li>
                        <li><a href="#">profile</a></li>
                        <li><a href="#">Reviews</a></li>
                        <li><a href="#">Gigs</a></li>
                        <li><a href="#">messages</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="pg-dash">
                    <!--photo uploader-->
                    {!! Form::open(['files' => 'true']) !!}
                    <div class="">
                        <div class="pg-dash-in">
                            <h5>Select file for upload <div class="btn btn-o btn-default btn-file bc10"> <i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse Images <input type="file" id="1431954496675" data-browse-label="Browse Images" data-browse-class="btn btn-o btn-default" accept="image/jpeg,image/png" data-show-remove="false" data-show-caption="false" data-show-upload="false" multiple="true" name="image" class="file"></div> <input type="submit" class="btn btn-success" value="Upload"></h5>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="tab-pane fade active in" id="photos">
                        <p></p>
                        @foreach($photos as $photo)
                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                            <a href="#" class="thumbnail">
                                <img alt="" src="https://s3-us-west-2.amazonaws.com/artisanbucket/uploads/{{$photo->photo_url}}" class="img-responsive" style="width:265px; height:180px">
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <!--photo uploader-->
                </div>
            </div>

        </div>


    </div>
    @stop