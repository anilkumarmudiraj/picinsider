@extends('../layout.base')

@section('content')
    <div class="container business one">
        <div class="row">
            <div class="col-md-12"><h3>Thank you for registering.  Below are some optional details which will provide clients with additional information. While these details are optional, we highly recommended filling these information to enhance your profile.  You can also update this information later.</h3></div>
                {!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 txt-align control-label">Business description :</label>

                    <div class="col-sm-9">
                        <textarea class="form-control" rows="4" name="description" placeholder="max of (1500 characters)"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label">Photography style :</label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographystyle[]" value="1">Contemporary
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" name="photographystyle[]" value="2">Photojournalism
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" name="photographystyle[]" value="3">Portraiture
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label"></label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographystyle[]" value="4"> Traditional
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" name="photographystyle[]" value="5"> Photography services
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label">Photography services :</label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographyservices[]" value="1"> 1 Event Per Day
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" name="photographyservices[]" value="2"> 2nd Shooter
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" name="photographyservices[]" value="3"> Add'l Hours
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label"></label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographyservices[]" value="4"> Custom Graphics
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" name="photographyservices[]" value="5"> High-Res
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" name="photographyservices[]" value="6"> Engagement Shoot
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label"></label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographyservices[]" value="7"> Multi. Locations
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" name="photographyservices[]" value="8"> Slideshow
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" name="photographyservices[]" value="9"> Liability Insurance
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label"></label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographyservices[]" value="10"> Boudoir Shoot
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" name="photographyservices[]" value="11"> Hand-Coloring
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" name="photographyservices[]" value="12"> Toning
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label"></label>

                    <div class="col-sm-9">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="photographyservices[]" value="13"> Trash The Dress
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label">Languages :</label>

                    <div class="col-sm-9">
                        <input type="postal" class="form-control" id="inputPassword3" placeholder="seperated by comma" name="languages">

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 txt-align control-label">Interests :</label>

                    <div class="col-sm-9">
                    <textarea class="form-control" rows="2" name="interests" placeholder="What are your interests? People want to deal with people who are human. Separate each with a comma"></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-5">
                        <input type="submit" class="btn btn-default enq_form" value="Save and Continue">
                    </div>

                </div>


                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <p class="text-center"><em>PicInsider Privacy Promise: We are very concerned about privacy
                                and wil never share your email, phone, postal code / zip code with anyone.
                                See our privacy policy and terms of use.</em></p>
                    </div>

                </div>
            {!! Form::close() !!}

        </div>
    </div>

@stop