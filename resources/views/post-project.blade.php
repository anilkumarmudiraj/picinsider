@extends('layout.base')

@section('content')
<p></p>
    <div class="container business one">
        <div class="row">
            <div class="col-md-12"><h2>Post a Project</h2></div>
                {!! Form::open(['class' => 'form-horizontal col-md-12']) !!}
            @if(isset($message))
            <div class="form-group">
                <span class="pull-right success">{{$message}}</span>
            </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 txt-align control-label" >Short Gig Description <span style="color: red">*</span></label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="inputEmail3" autocomplete="off" onkeyup="countChar(this)" name="short" value="{{old('short')}}" placeholder="Maximum 140 characters"><div id="charNum"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 txt-align control-label">Select a Category <span style="color: red">*</span><br /> <small>Maximum two</small> </label>
                    <div class="col-sm-5">
                        @foreach($categories as $category)
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="category[]" value="{{$category->name}}"> {{$category->name}}
                        </label>
                        @endforeach
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 txt-align control-label">City <span style="color: red">*</span></label>

                    <div class="col-sm-6">
                        <input type="text" class="typeahead form-control" name="city" autocomplete="off" value="{{old('city')}}">
                    </div>
                </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 txt-align control-label">Date <span style="color: red">*</span></label>
                <div class="col-sm-6">
                    <div class="input-group date" id="datetimepicker1">
                        <input type="text" class="form-control" data-format="yyyy-MM-dd" name="date" value="{{old('date')}}"/>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                        <script type="text/javascript">
                            $(function () {
                                $("#datetimepicker1").datetimepicker({
                                       format: 'DD/MM/YYYY'

                                });
                            });
                        </script>
                    </div>
                </div>
            </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 txt-align control-label">Time Requirements <span style="color: red">*</span></label>
                    <div class="col-md-6">
                        <select name="requirements" class="form-control">
                            <option value="Morning">Morning</option>
                            <option value="Afternoon">Afternoon</option>
                            <option value="Evening">Evening</option>
                            <option value="Allday">All day</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 txt-align control-label" for="inputEmail3">Detailed Gig Description <span style="color: red">*</span></label>
                    <div class="col-md-6">
                        <textarea rows="4" name="description" class="form-control" placeholder="The more details that are provided will help generate more accurate quotes and photographer interest.Include details such as location specifics, what you are looking for, and links to inspirational photos">{{old('description')}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 txt-align control-label">Budget <span style="color: red">*</span></label>

                    <div class="col-sm-6">
                        <select class="form-control" name="budget">
                            <option value="Not Specified">Not Specified</option>
                            <option value="100-200">Between $100-200</option>
                            <option value="200-500">Between $200-500</option>
                            <option value="500-1000">Between $500-1000</option>
                            <option value="1000-2000">$1000 and above</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-5">
                        <button type="submit" class="btn btn-default enq_form"> Post Gig</button>
                    </div>

                </div>


                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <p class="text-center"><em>PicInsider Privacy Promise: We are very concerned about privacy
                                and will never share your email, phone, postal code / zip code with anyone.
                                See our privacy policy and terms of use.</em></p>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

    </div>
@stop