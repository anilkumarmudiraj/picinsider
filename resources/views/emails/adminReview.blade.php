<html>
<head>
    <title>PicInsider</title>
    <?php
    use Illuminate\Support\Str;
    ?>
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 24px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <h3>There is a new review on the website</h3>
        <div class="quote">
            <p>Dear {{$username}},</p>
            <p>{{$reviewed_by}} recently reviewed {{$photographer_name}} on Pic Insider.</p>
            <p>Subject of the review : {{$subject}}</p>
            <p>Review Message : {{$body}}</p>
            <p><a href="http://45.55.232.120/photographer/{{$photographer_id}}/<?php echo Str::slug($photographer_name);?>">Click here! to check out the gig</a> </p>
        </div>
    </div>
</div>
</body>
</html>
