<html>
<head>
    <title>PicInsider</title>

    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
            margin-bottom: 40px;
        }

        .quote {
            font-size: 24px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Recover your password on PicInsider !</div>
        <div class="quote">
            <p>You have placed a request to recover your password on Pic Insider.</p>
            <!--<p>To recover your account please <a href="http://localhost:8000/reset?id={{$id}}&key={{$key}}">Click here!</a> </p>-->
		<p>To recover your account please <a href="{{ url('/reset?id='.$id.'&key='.$key)}}">Click here!</a> </p>
        </div>
    </div>
</div>
</body>
</html>
