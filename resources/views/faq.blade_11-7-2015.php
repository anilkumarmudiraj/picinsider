<!DOCTYPE html>
<html lang="en">
<head>
  <title>PicInsider</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap-3.3.4-/css/bootstrap.css">
  <link rel="stylesheet" href="style/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="fonts.css" type="text/css" charset="utf-8" />
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="style/flexslider.css" type="text/css" media="screen" />


</head>
<body>
<div class="container">
	<div class="row">
        <div class="col-lg-6 logo"><a href="#"><img src="images/logo.jpg"></a></div>
        <div class="col-lg-6 search_box">
        <div class="search">
        <form role="form">
        <div class="form-group seach_top">

          <input type="email" class="form-control" id="byphoto" placeholder="By Photographer">
        </div>
        <div class="form-group seach_top">

          <input type="password" class="form-control " id="bycity" placeholder="By City near Toronto">
        </div>

        <button type="button" class="btn btn-info btn-ts">
              <span class="glyphicon glyphicon-search"></span> Search
            </button>
      </form>
      </div>
      <div class="attention">Attention photographer: <a href="#"><span>Click here to add your business</span></a></div>
    </div>
    </div>
</div>
<div class="menu">
<div class="container ">
<nav class="navbar navbar-inverse">
  <div class="">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav nav_menu">
        <li><a href="#">Catagories</a></li>
        <li><a href="#">Write a Review</a></li>
        <li><a href="#">Post a Project</a></li>
        </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Sign Up</a></li>
        <li><a href="#">Login</a></li>
      </ul>
    </div>
  </div>
</nav>
</div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-7 spark_photo">
        <h2>FAQ</h2>
        </div>
    </div>


<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
    <div class="bs-example" data-example-id="collapse-accordion">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php foreach($faq as $allfaq){?>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" onclick="return showAndHide(<?php echo $allfaq['id'];?>)">
          <h4 class="panel-title">
            <a role="button" >
              <?php echo $allfaq['question'];?>
            </a>
          </h4>
        </div>
        <div id="collapseOne_<?php echo $allfaq['id'];?>" style="display: none;">
          <div class="panel-body">
              <?php echo $allfaq['answer'];?>
          </div>
        </div>
      </div>
<?php }?>
    </div>
  </div>

    </div>
    </div>
</div>



<div class="footer">
<div class="container">
<div class="row">
<div class="col-sm-6 col-lg-3 footer_list">
    <ul>
        <li><a href="#">Users</a></li>
        <li><a href="#">Post a Project</a></li>
        <li><a href="#">Write a Review</a></li>
        <li><a href="#">Manage Your Account</a></li>
    </ul>
</div>
<div class="col-sm-6 col-lg-3 footer_list">
    <ul>
        <li><a href="#">Categories</a></li>
        <li><a href="#">Wedding</a></li>
        <li><a href="#">Engagement</a></li>
        <li><a href="#">Portraits</a></li>
        <li><a href="#">Newborn</a></li>
        <li><a href="#">Other</a></li>
    </ul>
</div>
<div class="col-sm-6 col-lg-3 footer_list">
    <ul>
        <li><a href="#">Photographer</a></li>
        <li><a href="#">Manage Your Account</a></li>
        <li><a href="#">Become Certified</a></li>
        <li><a href="#">Claim a Business</a></li>
    </ul>
</div>
<div class="col-sm-6 col-lg-3 footer_list">
<ul>
        <li><a href="#">General</a></li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Photographer Tips</a></li>
        <li><a href="#">Contact Us</a></li>
        <li><a href="#">Help/FAQ’s</a></li>
    </ul>
    <ul class="fo_blog">
        <li><a href="#">Blog</a></li>
        <li><a href="#">In the Press</a></li>
        <li><a href="#">Tell a Friend</a></li>
    </ul>
</div>
</div>
<div class="row footer_two">
<div class="col-lg-7 footer_menu">
<ul>
<li><a href="#">Toronto</a></li>
<li><a href="#">Edmonton</a></li>
<li><a href="#">Vancouver</a></li>
<li><a href="#">Montreal</a></li>
<li><a href="#">Calgary</a></li>
<li><a href="#">More cities</a></li>
</ul>
</div>
<div class="col-lg-5">
<ul class="footer_media">
<li>FOLLOW US</li>
<li><a href="#"><i class="fa fa-facebook-square fa-2x"></i></a></li>
<li><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
<li><a href="#"><i class="fa fa-pinterest-p fa-2x"></i></a></li>
<li><a href="#"><i class="fa fa-google-plus fa-2x"></i></a></li>
<li><a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
</ul>
</div>
</div>

</div>
</div>
<div class="footer_bottom">
<div class="container">
<div class="row footer_last">
<div class="col-sm-6 footer_logo">Copyright ©PicInsider. All rights reserved.  </div>
<div class="col-sm-6 ">
<ul class="site_map">
<li><a href="#">Sitemap </a></li>
<li><a href="#">Privacy Policy </a></li>
<li><a href="#">Terms and Conditions</a></li>
</ul>
</div>
</div>
</div>
</div>
<script defer src="js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 94,
        itemMargin:10,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });

      function showAndHide(toggleit){
        $('#collapseOne_'+toggleit).toggle();
      }
  </script>
</body>
</html>