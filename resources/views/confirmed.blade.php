@extends('../layout.base')

@section('content')
    <p></p>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Information</div>


                    <div class="panel-body">
                        @if(Auth::check())
                            Thank you for verifying your email address. Please <a href="/login">click here</a> to login.  In your Photographer dashboard, you will be able to update your profile, upload images to your gallery and also bid on Gigs in your area.
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
@stop