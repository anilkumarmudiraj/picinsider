@extends('layout.base')
@section('content')
    <p></p>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Information</div>


                    <div class="panel-body">
                        @if(Auth::check())
                        <h4>Your account is not active yet, we have sent you an activation email please use that to activate your account. If you have not received the mail yet please <a href="/resend/{{Auth::id()}}">click here</a> to send the email again</h4>
                            @else
                            <h4> We think you landed on this page by mistake please use the menu above to find your navigation.</h4>
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
    @stop
