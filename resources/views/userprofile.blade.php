@extends('layout.base')
<?php
use Illuminate\Support\Str;
?>
@section ('content')

    <div class="container">
        <div class="row">
            <div class="col-md-7 spark_photo">
                <h2>DashBoard</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="bs-example">
                    <ul class="nav nav-tabs tab">
                        <li class="active"><a data-toggle="tab" href="#reviews">My GIGS</a></li>
                        <li><a data-toggle="tab" href="#myaccount">My Account</a></li>
                        <li><a data-toggle="tab" href="#photos">Favorite photographers</a></li>
                        <a href="/post-project"><li class="write_review pull-right"><button type="button" class="btn btn-default">POST A NEW GIG</button></li></a>
                    </ul>
                    <div class="tab-content tablist">
                        <div id="reviews" class="tab-pane fade in active">
                            @foreach ($userDetails->gigs as $gig)
                            <div class="reviews_list">
                                <h3>{{$gig->short}} ({{$gig->date}})</h3><div class="pull-right"><a href="/gig/delete/{{$gig->id}}"<button class="btn btn-info">Delete this gig</button></a><a href="/gig/edit/{{$gig->id}}"<button class="btn btn-info">Edit this gig</button></a></div>
                                <div class="tab_rating"><span>Posted on {{$gig->created_at}} under @foreach($gig->gigcategories as $category) {{$category->category}}@endforeach</span><br/>
                                    <span> 13 people already placed a bid on this</span>
                                </div>
                                <p>{{$gig->description}}</p>
                            </div>
                            @endforeach

                        </div>

                        <!-- My account section starts -->
                        <div id="myaccount" class="tab-pane fade">
                            <h3>Your Details <div class="pull-right small"><button data-toggle="modal" data-target="#myModal2" id="delete_user" class="btn btn-danger btn-md mb10"> Click here to change your password </button></div></h3>
                            <hr />

                                {!! Form::open(['role' => 'form', 'class' => 'form-horizontal']) !!}
                                <div class="form-group">

                                    <label class="col-sm-4 control-label">Name</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="name" value="{{$userDetails->name}}">
                                    </div>
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-4 control-label">Email</label>

                                    <div class="col-sm-8">
                                        <input class="form-control" type="email" name="email" value="{{$userDetails->email}}">
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <input type="submit" value="Update" class="btn btn-success pull-right">
                                        </div>
                                </div>

                            {!! Form::close() !!}
                        </div>

                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
				<form id="change_password">                        
			            <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                                    </div>
                                    <div class="modal-body db">
                                        <div class="form-group db">
                                        <label class="col-sm-6 control-label">Current Password </label>
                                            <div class="col-sm-6">
                                                <input type="password" id="current_password" class="form-control">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-6 control-label">New Password </label>
                                            <div class="col-sm-6">
                                                <input type="password" id="new_password" class="form-control">
                                                <input type="hidden" value="{{ csrf_token() }}" id="csrftoken">
                                            </div>
                                        </div>

					<div class="form-group">
                                            <label class="col-sm-6 control-label">Confirmed Password </label>
                                            <div class="col-sm-6">
                                                <input type="password" name="password_again" id="confirm_password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="error" id="message"></div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="updatePassword">Save changes</button>
                                    </div>
				</form>
                                </div>
                            </div>
                        </div>
                        <!-- My account section ends -->

                        <div id="photos" class="tab-pane fade">
                            <h3>Favorite photographers</h3>
<?php $pdo = DB::connection()->getPdo();
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $results = DB::select('select DISTINCT photographer_id, id, user_id  from favorite_photographers');
//print_r( $results);
// print_r($getAllFavPhotographer);?>
                           <!-- <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>
                        --></div>
                        <div id="faq" class="tab-pane fade">
                            <h3>Section D</h3>
                            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <img src="images/atnanta.jpg" alt="Atlanta" width="290" height="255"/>
            </div>
        </div>

        <div class="row toronto">
            <div class="panel">
                <h2>RECENTLY VIEWED <span>PhotographerS</span>
                    <div class="pull-right">
                        <!--<button type="button" class="btn btn-default pull-left bg10 m1">VIEW ALL PHOTOGRAPHERS</button>

                        <button type="button" class="btn btn-default pull-right bg10">INVITE ALL PHOTOGRAPHERS</button>-->
                    </div>
                </h2>

            </div>

            @if(count($recentviews) < 1)
                <h4>You have not visited any photographers yet. We have thousands of photgraphers on our website, start exploring.</h4>
                @else

            @foreach($recentviews as $recentviews)
            <div class="col-sm-6 col-lg-3">
                <div class="imageOuter" style="margin:0">
                    <a class="image" href="/photographer/{{$recentviews->photographer_id}}/{{\Illuminate\Support\Str::slug($recentviews->photographer->name)}}">
                        <img class="imgborder" alt="" src="/uploads/photographers/{{$recentviews->photographer->photo}}" style="width:256px; height:196px">
                    </a>
                </div>
                <div class="imagebottom">
                    <ul>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                    </ul>
                    <span><a href="/photographer/{{$recentviews->photographer_id}}/{{\Illuminate\Support\Str::slug($recentviews->photographer->name)}}">{{$recentviews->photographer->name}}</a></span>
                    <p>Favorite brand: {{$recentviews->brand}}<br/>
                        Categories: @foreach($recentviews->photographer->photographercategory as $category){{$category->name}} @endforeach</p>
                </div>
            </div>
            @endforeach
@endif
        </div>
    </div>
    @stop
