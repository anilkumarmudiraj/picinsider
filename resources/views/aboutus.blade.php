@extends('layout.base')

@section('content')

    <div class="container photo_tips">

        <div class="row">

            <div class="col-md-12">

                <img src="{{$aboutus->slide}}" class="img-responsive po" >
                <div class="banner-top">
                </div>
                <p class="banner-bottom">{{$aboutus->onslider}}</p>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>{!! $aboutus->heading1 !!}</h3>
            </div>

            <div class="col-md-12">
                {!! $aboutus->para1 !!}
            </div>

        </div>

        <div class="row">
            <div class="col-md-4 tips_logo_list">
                <div class="logo_outer">
                    <div class="tips_logo"><a href="#"><img src="{{$aboutus->person1_photo}}" alt="Picinsider" class="img-circle"></a></div></div>
                <div class="richard">
                    <h4>{{$aboutus->person1_name}}</h4>
                    <h3>{{$aboutus->person1_position}}</h3>
                </div>
            </div>

            <div class="col-md-4 tips_logo_list">
                <div class="logo_outer">
                    <div class="tips_logo"><a href="#"><img src="{{$aboutus->person2_photo}}" alt="Picinsider" class="img-circle"></a></div></div>
                <div class="richard">
                    <h4>{{$aboutus->person2_name}}</h4>
                    <h3>{{$aboutus->person2_position}}</h3>
                </div>
            </div>
            <div class="col-md-4 tips_logo_list">
                <div class="logo_outer">
                    <div class="tips_logo"><a href="#"><img src="{{$aboutus->person3_photo}}" alt="Picinsider" class="img-circle"></a></div></div>
                <div class="richard">
                    <h4>{{$aboutus->person3_name}}</h4>
                    <h3>{{$aboutus->person3_position}}</h3>
                </div>
            </div>
        </div>


        <div class="row m9">
            <div class="col-md-9">
                <h3>{!! $aboutus->heading2 !!}</h3>
                {!! $aboutus->para2 !!}
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 p1">
                    <a href="{{$aboutus->facebook}}" target="_blank" class="btn btn-sm btn-blue mb5"><span class="fa fa-facebook"></span>  Share on Facebook</a>
                    <a href="{{$aboutus->twitter}}" target="_blank" class="btn btn-sm btn-lblue mb5"><span class="fa fa-twitter"></span> Twitter</a>
                    <a href="{{$aboutus->pinterest}}" target="_blank" class="btn btn-sm btn-red mb5"><span class="fa fa-pinterest"></span> Pinterest</a>

                    <a href="#" class="btn btn-sm btn-gray mb5"><span class="fa fa-envelope"></span> E-mail</a>

                </div>
            </div>

            <div class="col-md-3 search_sidebar panel-body">

                <div class="con_view">

                    <h3 class="re_view">Recently <span>Viewed</span></h3>
                    <ul>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            </ul>
                            <p class="review_num">10 reviews</p>

                        </li>

                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>
                        <li>
                            <img src="images/photo_tips/review_pic1.jpg">
                            <h5>Photographer</h5>
                            <ul class="search_spark_reating">
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></a></li>
                            </ul>
                            <p class="review_num">10 reviews</p>
                        </li>



                    </ul>

                </div>
            </div>

        </div>
    </div>
    @stop