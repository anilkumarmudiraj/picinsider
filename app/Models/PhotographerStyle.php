<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotographerStyle extends Model {

	public function styles()
    {
        return $this->hasMany('App\Models\Style', 'id');
    }

}
