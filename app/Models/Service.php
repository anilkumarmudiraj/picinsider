<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

	public function photographers()
    {
        return $this->belongsTo('\App\Models\Photographers');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service');
    }

}
