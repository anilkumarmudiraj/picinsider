<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GigCategory extends Model {

    public function gig()
    {
        return $this->belongsTo('App\Models\Gig');
    }

}
