<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recentview extends Model {

    public function photographer()
    {
        return $this->belongsTo('App\Photographer','photographer_id', 'user_id');
    }
}
