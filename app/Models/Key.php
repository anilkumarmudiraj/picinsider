<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    protected $fillable = ['key'];
    public function user()
    {
        return $this->belongsTo('\App\Models\User');
    }

    public static function confirmation($id, $key)
    {
        $response = '';
        $checkKey = Key::where('key', '=', $key)
            ->where('user_id', '=', $id)
            ->count();

        if($checkKey > 0 )
        {
            /*$confirmUser = User::where('id', '=', $id)->update(['confirmed' => '1']);*/
            $confirmUser = \App\User::find($id);
            $confirmUser->confirmed = '1';
            $confirmUser->save();
            $response = 'confirmed';
        }
        else
        {
            $response = 'invalid_details';
        }

        return $response;

    }
}
