<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	public function photographer()
    {
        return $this->belongsTo('\App\Models\Photographers');
    }

}
