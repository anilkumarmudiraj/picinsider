<?php namespace App;


use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use Illuminate\Database\Query\Builder;

/**
 * Class Photographer
 * @package App
 */
class Photographer extends Model
{

    protected $fillable = ['name', 'company_name', 'phone_number', 'website', 'city', 'established', 'studio', 'price', 'under500', 'under1000', 'facebook', 'gplus', 'twitter', 'pinterest', 'instagram', 'certified', 'brand', 'description', 'photo', 'logo', 'featured', 'subscribe', 'notification',];


    public static function newlead()
    {
        $twodays       = Carbon::today()
                               ->subDays(2);
        $photographers = Photographer::whereHas('user', function ($q) {
            $q->where('confirmed', '=', '1');
        })
                        ->with('mainreview')
                        ->orderBy('id', 'DESC')
                        ->whereNotNull('photo')
                        ->take(4)
                        ->get();

        return $photographers;
    }

    public function reviews()
    {
        return $this->hasMany('\App\Review', 'photographer_id', 'user_id');
    }

    public function mainreview()
    {
        return $this->hasMany('\App\Review', 'photographer_id', 'user_id')
                    ->latest()
                    ->take(1);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function photographercategory()
    {
        return $this->hasMany('\App\Models\Photographercategories', 'photographer_id', 'user_id');
    }

    public function photographerservices()
    {
        return $this->hasMany('\App\Models\PhotographerService', 'photographer_id', 'id');
    }

    public function photographerstyles()
    {
        return $this->hasMany('App\Models\PhotographerStyle', 'photographer_id', 'id');
    }

    public function invites()
    {
        return $this->hasMany('App\Models\Invite', 'invite_to', 'user_id');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo','photographer_id','user_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message', 'to_id', 'user_id');
    }

    public static function suggested($id)
    {
        $result = Photographer::whereHas('user', function ($q) {
            $q->where('confirmed', '=', '1');
        })
            ->whereNotNull('photo')
            ->whereNotIn('user_id', [$id])
            ->with('photographercategory')
            ->take(8)
        ->get();

        return $result;
    }

    /**
     * @param $id
     */
    public static function photographerInformation($id)
    {
        $result = Photographer::where(array('user_id' => $id))
            ->with('photos')
            ->with('reviews')
            ->with('photographerservices')
            ->with('reviews.ratinglikes')
            ->with('photographercategory')
            ->first();
         return $result;
    }
}
