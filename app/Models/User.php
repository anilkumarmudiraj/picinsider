<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


    public function role()
    {
        return $this->hasOne('App\Models\Role');
    }

    public function key()
    {
        return $this->hasOne('App\Models\Key');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function photographer()
    {
        return $this->hasOne('\App\Photographer');
    }

    public function gigs()
    {
        return $this->hasMany('\App\Models\Gig');
    }

    public function recentviews()
    {
        return $this->hasMany('\App\Models\Recentview')->groupBy('photographer_id')->take(8);
    }
}
