<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model {

	public function photographer()
    {
        return $this->hasOne('App\Photographer');
    }

}
