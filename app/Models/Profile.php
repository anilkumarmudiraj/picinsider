<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

    protected $fillable = ['phone', 'address', 'city', 'state', 'zip', 'country'];

    public static function findOrCreate($id)
    {
        $obj = static::find($id);
        return $obj ?: new static;
    }
}
