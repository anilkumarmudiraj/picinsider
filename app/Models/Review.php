<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Review extends Model
{

    /**
     * @return mixed
     */

    public static function featured()
    {
        $response = Photographer::with('mainreview')
            ->where('featured', '=', 'Yes')
            ->whereNotNull('photo')
            ->take(4)
            ->get();
        $response->shuffle();
        return $response;
    }

    public static function recent()
    {
        $timeNow = Carbon::now();
        $threeDays = Carbon::now()->subDays(2);
        $response = Photographer::whereHas('user',function($q)
        {
            $q->where('confirmed', '=', '1');
        })
            ->with('mainreview')
            ->with('photographercategory')
            ->whereNotNull('photo')
            ->take(4)
            ->get();

        return $response;
    }


    public function photographers()
    {
        return $this->belongsTo('\App\Photographer');
    }

    public function ratinglikes()
    {
        return $this->hasMany('App\Models\RatingLike');
    }

    public function flags()
    {
        return $this->hasMany('\App\Models\Flag');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
