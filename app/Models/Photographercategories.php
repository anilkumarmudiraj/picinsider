<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photographercategories extends Model
{
    public function photographers()
    {
        return $this->belongsTo('\App\Photographer');
    }
}
