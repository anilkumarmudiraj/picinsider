<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mail;

class Generalfunction extends Model
{
    public static function customMail($key, $id, $email, $username, $subject)
    {
        $response = '';
        if(Mail::send('welcome', ['key' => $key, 'id' => $id], function($message) use($email, $username, $subject)
        {
            $message->to($email, $username)->subject('Welcome to PicInsider!');
        }))
        {
            $response = 'success';
        }
        else
        {
            $response = 'failed';
        }

        return $response;
    }

    public static function resetMail($key, $id, $email, $username, $subject)
    {
        $response = '';
        if(Mail::send('emails.reset', ['key' => $key, 'id' => $id], function($message) use($email, $username, $subject)
        {
            $message->to($email, $username)->subject('Password recovery email');
        }))
        {
            $response = 'success';
        }
        else
        {
            $response = 'failed';
        }

        return $response;
    }
}
