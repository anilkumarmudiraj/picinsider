<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gig extends Model {

	public function user()
    {
        return $this->hasOne('App\User');
    }

    public function gigcategories()
    {
        return $this->hasMany('App\Models\GigCategory');
    }

}
