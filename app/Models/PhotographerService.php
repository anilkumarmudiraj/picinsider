<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotographerService extends Model {

	public function services()
    {
        return $this->hasMany('App\Models\Service', 'id');
    }

}
