<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'short' => 'required|min:1|max:140',
            'category' => 'required',
            'city' => 'required',
            'date' => 'required',
            'requirements' => 'required',
            'description' => 'required|min:20|max:1000',
            'budget' => 'required'
		];
	}

}
