<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/** Limit the access of following pages only to guests */

Route::get('/test', function(){
    // echo Hash::make('password');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', 'LoginController@showLogin');

    Route::post('/login', 'LoginController@doLogin');

    Route::post('/modalLogin', 'LoginController@modalLogin');

    Route::get('/register', 'LoginController@showRegister');

    Route::post('/register', 'LoginController@doRegister');

    Route::get('/forgot-password', 'PasswordController@showForgotPassword');

    Route::post('/forgot-password', 'PasswordController@doForgotPassword');

    Route::get('facebook', 'LoginController@facebook');

    Route::get('callback', 'LoginController@callback');

    Route::get('/photographer/register', 'LoginController@showPhotographerRegister');

    Route::post('/photographer/register', 'LoginController@doPhotographerRegister');

    /**
     *
     * Photographers and Users share the same login page
     *
     **/

    // Route::get('/photographer/login', 'LoginController@showPhotographerLogin');

    // Route::post('/photographer/login', 'LoginController@doPhotographerLogin');

});


/** Limit the access of following pages only to guests or users with confirmed account */

Route::get('/', 'IndexController@index');

Route::group(['middleware' => 'confirmed'], function () {

    Route::get('/photographer/reset', 'LoginController@showPhotographerReset');

    Route::post('/photographer/reset', 'LoginController@doPhotographerReset');

    Route::get('photographer/dashboard', 'PhotographerController@dashboard');

    Route::post('photographer/dashboard', 'PhotographerController@doDashboard');

    Route::get('photographer/manage-photos', 'PhotographerController@showUpload');

// Route::post('photographer/favphotographer', 'PhotographerController@favphotographer');

//Route::resource('photographer','PhotographerController');

    Route::post('photographer/manage-photos', 'PhotographerController@doUpload');

    /** Routes for login and register ends here */

    Route::get('home', 'IndexController@home');

    Route::post('post-review', 'PhotographerController@postReview');

    Route::post('like-review', 'PhotographerController@likeReview');

    Route::post('dislike-review', 'PhotographerController@dislikeReview');

    Route::get('post-project', 'UserController@postProject');

    Route::post('post-project', 'UserController@processProject');

    Route::get('gig-posted', 'UserController@gigPosted');

    Route::post('inviteToGig', 'UserController@inviteToGig');

    Route::get('myprofile', 'ProfileController@showProfileCompletePage');

    Route::post('myprofile', 'ProfileController@saveProfileCompletePage');

    Route::post('modalupdatePassword', 'PasswordController@modalUpdate');

    Route::post('flagReview', 'PagesController@markFlag');

    Route::get('gig/edit/{id}', 'UserController@showGigEdit');

    Route::post('gig/edit/{id}', 'UserController@processGigEdit');

    Route::get('gig/delete/{id}', 'UserController@deleteGig');

});
Route::get('logout', 'LoginController@logout');

Route::get('/unconfirmed', 'LoginController@unconfirmed');

Route::get('/sendmail', 'IndexController@mail');

Route::get('/confirm', 'LoginController@confirm');

Route::get('/resend/{id}', 'LoginController@resend');

Route::get('/photographer/{id}/{name}', 'PhotographerController@profile');

Route::post('sendMessage', 'MessageController@sendMessage');

Route::get('reset', 'PasswordController@showReset');

Route::post('/reset', 'PasswordController@doReset');

Route::get('/manage/{id}/{name}', 'PhotographerController@showManage');

Route::get('search', 'SearchController@index');

Route::get('topsearch', 'SearchController@topSearch');

Route::post('photographer/favphotographer/{userid}/{photographerId}', 'PhotographerController@favphotographer');

Route::post('photographer/myfavphotographer/{userid}', 'PhotographerController@myfavphotographer');


//Route::resource('photographer/{userid}/{photographerId}','PhotographerController');


/**
 * ***************************************
 *                                       *
 *                                       *
 *        ADMINISTRATOR ROUTES           *
 *        -------------------            *
 *                                       *
 * ***************************************/


Route::get('administrator/login', 'Admin\AdminController@login');

/** The following routes are available only to administrators */
Route::group(['middleware' => 'isAdmin'], function () {

    Route::get('administrator', 'Admin\AdminController@index');

    Route::get('administrator/all-users', 'Admin\AdminController@users');

    Route::get('administrator/general-users', 'Admin\AdminController@generalUsers');

    Route::get('administrator/photographers', 'Admin\AdminController@photographers');

    Route::get('administrator/photographer-search', 'Admin\AdminController@searchPhotographers');

Route::get('administrator/faq', 'Admin\AdminController@showFaq');

Route::post('administrator/faq', 'Admin\AdminController@processFaq');

    Route::get('administrator/user/edit/{id}', 'Admin\AdminController@editUser');

    Route::post('administrator/user/edit/{id}', 'Admin\AdminController@processEdit');

    Route::get('administrator/user/delete/{id}', 'Admin\AdminController@editUser');

    Route::get('administrator/photographer/edit/{id}', 'Admin\AdminController@editPhotographer');

    Route::post('administrator/photographer/edit/{id}', 'Admin\AdminController@processPhotographerEdit');

    Route::get('administrator/photographer/delete/{id}', 'Admin\AdminController@users');

    Route::get('administrator/add-user', 'Admin\AdminController@showAddUser');

    Route::post('administrator/add-user', 'Admin\AdminController@processAddUser');

    Route::get('administrator/add-photographer', 'Admin\AdminController@showAddPhotographer');

    Route::post('administrator/add-photographer', 'Admin\AdminController@processAddPhotographer');

    Route::get('administrator/main-sliders', 'Admin\AdminController@showSliders');

    Route::post('administrator/deactivateSlider', 'Admin\AdminController@deactivateSlider');

    Route::post('administrator/activateSlider', 'Admin\AdminController@activateSlider');

    Route::post('administrator/deleteSlider', 'Admin\AdminController@deleteSlider');

    Route::post('administrator/sliderupload', 'Admin\AdminController@sliderUpload');

    Route::get('administrator/about-us', 'Admin\AdminController@aboutUs');

    Route::post('administrator/about-us', 'Admin\AdminController@processaboutUs');

    Route::get('/administrator/flagged-reviews', 'Admin\AdminSecondController@showFlagged');

    Route::post('administrator/deleteUser', 'Admin\AdminController@deleteUser');

    Route::post('administrator/deleteReview', 'Admin\AdminController@deleteReview');

    Route::post('administrator/deleteFlag', 'Admin\AdminController@deleteFlag');

    Route::get('administrator/write-blog', 'Admin\BlogController@showWriteBlog');

    Route::post('administrator/write-blog', 'Admin\BlogController@processWriteBlog');

    Route::get('administrator/edit/blog/{id}', 'Admin\BlogController@showEdit');

    Route::post('administrator/edit/blog/{id}', 'Admin\BlogController@doEdit');

    Route::get('/administrator/blogs', 'Admin\BlogController@blogs');

    Route::post('administrator/changeStatus', 'Admin\AdminSecondController@changeStatus');

    Route::get('/administrator/reviews/{userid}', 'Admin\ReviewController@index');

    Route::get('/administrator/edit/review/{reviewid}', 'Admin\ReviewController@showReview');

    Route::post('saveEditedReview', 'Admin\ReviewController@processReview');
	
//	Route::get('faq', 'FaqController@faq');

});

Route::get('administrator/login', 'Admin\AdminController@showLogin');

Route::post('administrator/login', 'Admin\AdminController@doLogin');

Route::get('secondpage', 'PhotographerController@check');

Route::post('secondpage', 'PhotographerController@docheck');

Route::get('lastpage', 'PhotographerController@showLastPage');

Route::post('lastpage', 'PhotographerController@processLastPage');

Route::get('about-us', 'PagesController@aboutus');

Route::get('blog/{id}/{name}', 'BlogController@blog');

Route::get('blogs', 'BlogController@blogs');

 Route::get('faq', 'FaqController@faq');


Route::get('check-relation', function()
{
    $getvalues = \App\Photographer::where('id', '3')->with('photographerstyles.styles')->get();
    return $getvalues;
});
