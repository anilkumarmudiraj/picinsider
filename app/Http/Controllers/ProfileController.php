<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Profile;
use App\User;

use Illuminate\Http\Request;

class ProfileController extends CustomController
{

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function showProfileCompletePage()
    {
        if (Auth::check()) {
            $userDetails = User::where('id', Auth::user()->id)
                ->with('gigs')
                ->with('recentviews')
                ->with('recentviews.photographer')
                ->with('recentviews.photographer.photographercategory')
                ->with('gigs.gigcategories')
                ->first();

            $profileDetails = Profile::where('user_id', Auth::user()->id)->first();

            $countries = \App\Country::all();

            $recentviews = $userDetails->recentviews->take(4);

            return view('userprofile', compact(['userDetails', 'profileDetails', 'countries', 'recentviews']))->with([
                'cities' => $this->cities(),
                'autocompletePhotographers' => $this->photographers()
            ]);
        } else {
            return redirect()->to('/');
        }
    }

    public function saveProfileCompletePage(Request $request)
    {
        $updateUser = User::find(Auth::user()->id);
        $updateUser->name = $request->name;
        $updateUser->email = $request->email;
        $updateUser->save();

        $checkExistence = Profile::where('user_id', Auth::user()->id)->count();
        if ($checkExistence < 1) {
            $saveProfile = new Profile();
            $saveProfile->phone = $request->phone;
            $saveProfile->address = $request->address;
            $saveProfile->city = $request->city;
            $saveProfile->state = $request->state;
            $saveProfile->zip = $request->zip;
            $saveProfile->country = $request->country;
            $saveProfile->user_id = Auth::user()->id;
            $saveProfile->save();
        } else {
            $saveProfile = Profile::where('user_id', Auth::user()->id)->first();
            $saveProfile->phone = $request->phone;
            $saveProfile->address = $request->address;
            $saveProfile->city = $request->city;
            $saveProfile->state = $request->state;
            $saveProfile->zip = $request->zip;
            $saveProfile->country = $request->country;
            $saveProfile->save();
        }

        return redirect()->to('myprofile');
    }
}
