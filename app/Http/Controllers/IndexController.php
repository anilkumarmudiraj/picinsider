<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Photographer;
use App\User;
use App\Category;
use App\Review;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class IndexController extends CustomController
{

    public function index()
    {
        /**
         *
         * Find all the categories from the category table
         */

        $categories = Category::all();

        /**
         *
         *  Find all the photographers who joined us
         *  two days ago from now
         *
         */

        $latestPhotographers = Photographer::newlead();


        /**
         *
         * Find all the photographers marked as featured.
         *
         */

        $featuredPhotographers = Review::featured();

        /**
         *
         * Get list of recently reviewed photographers
         */

        $reviewedPhotographers = Review::recent();
        /**
         *
         * Get the location of main sliders from the database
         *
         */

        $sliders = \App\Models\Slider::where('active', '=', 'Yes')->get();


        /**
         *  Return the data to index view data for cities
         *  and autocompletePhotographers is fetched from
         *  the functions in CustomController
         */

        return view('index', compact(
            'categories', 'latestPhotographers', 'featuredPhotographers',
            'reviewedPhotographers', 'autocompletePhotographers',
            'sliders'
        ))
            ->with([
                'cities' => $this->cities(),
                'autocompletePhotographers' => $this->photographers()
            ]);
    }

    /**
     *
     * This is a test function only and is not required in the project
     */

    public function home()
    {
        return view('home')->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }

    public function check()
    {
        return view('amazon');
    }

    public function doamazon(Request $request)
    {
        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        try
        {
        Storage::put('uploads/'.$file->getFilename().'.'.$extension,  File::get($file));
        }
        catch(Exception $e)
        {
            return $e;
        }

    }


    public function maps()
    {
        $get = json_decode(Laracurl::get('https://maps.googleapis.com/maps/api/place/autocomplete/json?delh&key=AIzaSyD4thzi6oPEyKPT64qT5SczpCJ4JBrrvdM'));
        return $get;
    }
}
