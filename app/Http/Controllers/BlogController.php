<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Blog;

use Illuminate\Http\Request;

class BlogController extends CustomController {

	public function blog($id)
    {
        $blog = Blog::where('id', $id)->first();
        return view('blog', compact('blog'))
            ->with([
                'cities' => $this->cities(),
                'autocompletePhotographers' => $this->photographers()
            ]);
    }

    public function blogs()
    {
        $blogs = Blog::paginate(3);
     return view('blogs', compact('blogs'))
         ->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

}
