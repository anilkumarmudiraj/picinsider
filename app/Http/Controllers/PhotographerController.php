<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Gig;
use App\Models\Photographercategories;
use App\Models\PhotographerService;
use App\Models\PhotographerStyle;
use App\Models\RatingLike;
use App\Models\FavoritePhotographer;
use App\Models\Recentview;
use App\Photographer;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Review;
use Auth;
use App\User;
use App\Models\Photo;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Mail\Mailer;

class PhotographerController extends CustomController
{
    public function __construct(Mailer $mailer)
    {
        $this->mail = $mailer;
    }

    public function profile($id)
    {
        $stars = 0;
        $users = 0;
        $gigsByUser = '';
        $reviews = 0;

        /**
         * Pass the id of current photographer to suggested method
         * of Photographer model to some suggestions and make
         * sure that the suggested photographer is not
         * the same as the one we are visiting
         */

        $suggested = Photographer::suggested($id);

        /**
         * Pass the id of current photographer to photographerInformation
         * method of Photographer model and get details like photos
         * reviews, services, ratinglikes and category
         */

        $information = Photographer::photographerInformation($id);

        /**
         * store the categories of photographer in new array
         */

        if(isset($information->photographercateory)) {
            $photographercategories = $information->photographercategory;
        }

        /**
         * Store the reviews of photographer into reviews array
         */
        if(isset($information->reviews)) {
            $reviews = $information->reviews;
            /**
             * fetch the rating of each review
             */

            foreach ($reviews as $review) {
                $ratinglike = $review->ratinglikes;
            }

            /**
             * Calculate total rating of photographer
             */

            foreach ($reviews as $review) {
                $stars += $review->stars;
                $users += 1;
            }

        }



        /**
         * if $information has services put it in $services array
         */

        if (isset($information->services)) {
            $services = $information->services;
        }


        $rating = 0;
        if ($stars != 0 && $users != 0) {
            $rating = $stars / $users;
        }

        /**
         *
         * get gigs posted by the logged in users
         *
         */

        if (Auth::check()) {
            $gigsByUser = Gig::where('user_id', Auth::user()->id)->get();
            if (Auth::user()->id != $id) {
                $recentViews = new Recentview();
                $recentViews->user_id = Auth::user()->id;
                $recentViews->photographer_id = $id;
                $recentViews->save();
            }
        }

        return view('photographers.profile', compact('information', 'reviews', 'photographercategories', 'services', 'rating', 'ratinglike', 'suggested', 'gigsByUser'))
            ->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    /**
     * #########################
     * Show manage function
     * #########################
     */

    public function showManage()
    {
        return view('photographers.manage')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);;
    }

    /**
     * #########################
     * Check function
     * #########################
     */

    public function check()
    {
        $name = Photographer::where('user_id', '=', Auth::user()->id)->first();
        $name = $name->company_name;
        return view('photographers.details', compact('name'))->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    /**
     * #########################
     * Do check function
     * #########################
     */

    public function docheck(Request $request)
    {
        $filename = '/uploads/photographers/default';
        $destinationPath = '';
        $extension = 'png';
        if (Input::hasFile('photo')) {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $photo = Input::file('photo');
            $destinationPath = public_path() . '/uploads/photographers/';
            $filename = str_random(12);
            $extension = $photo->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                $upload_success = $photo
                    ->move($destinationPath, $filename . '.' . $extension);
            }
        }
        $savedetails = Photographer::where('user_id', \Auth::user()->id)->first();
        $savedetails->name = Auth::user()->name;
        $savedetails->website = $request->website;
        $savedetails->phone_number = $request->phone;
        $savedetails->city = $request->city;
        $savedetails->established = $request->established;
        $savedetails->studio = $request->studio;
        $savedetails->price = $request->price;
        $savedetails->under500 = $request->under500;
        $savedetails->under1000 = $request->under1000;
        $savedetails->facebook = $request->facebook;
        $savedetails->gplus = $request->gplus;
        $savedetails->twitter = $request->twitter;
        $savedetails->pinterest = $request->pinterest;
        $savedetails->instagram = $request->instagarm;
        $savedetails->certified = '0';
        $savedetails->brand = $request->brand;
        $savedetails->photo = $filename . '.' . $extension;
        $savedetails->logo = '';
        $savedetails->featured = 'No';
        $savedetails->subscribe = $request->tips;
        $savedetails->notification = $request->notification;
        $savedetails->save();

        if(count($request->field) >0) {
            foreach ($request->field as $field) {
                $saveCategory = new Photographercategories();
                $saveCategory->name = $field;
                $saveCategory->photographer_id = \Auth::user()->id;
                $saveCategory->save();
            }
        }

        return redirect()->to('lastpage');
    }

    /**
     *
     * Post Review Function
     *
     */

    public function postReview(Request $request)
    {
        $postReview = new Review;
        $postReview->subject = $request->subject;
        $postReview->user_id = Auth::user()->id;
        $postReview->user_name = Auth::user()->name;
        $postReview->stars = $request->stars;
        $postReview->review = $request->review_review;
        $postReview->photographer_id = $request->photographer_id;
        $postReview->save();

        $photographer = User::where('id', '=', $request->photographer_id)->first();

        $this->mail->send('emails.review', [
            'username' => $photographer->name,
            'reviewed_by' => Auth::user()->name,
            'subject' => $request->subject,
            'body' => $request->review_review,
            'photographer_id' => $photographer->id,
            'photographer_name' => $photographer->name,
        ], function ($message) use ($photographer) {
            $message->to($photographer->email, $photographer->name)->subject('Someone has reviewed your skills');
        });

        $getAdmin = \App\Models\Role::where('role', '=', 'admin')->with('user')->get();

        foreach ($getAdmin as $admin) {
            $this->mail->send('emails.adminReview', [
                'username' => $admin->user->name,
                'reviewed_by' => Auth::user()->name,
                'photographer_name' => $photographer->name,
                'subject' => $request->subject,
                'body' => $request->review_review,
                'photographer_id' => $photographer->id
            ], function ($message) use ($admin, $photographer) {
                $message->to($admin->user->email, $admin->user->name)->subject('Someone has reviewed the skills of' . $photographer->name);
            });
        }

        echo json_encode('success');
    }


    public function likeReview(Request $request)
    {
        $checkExistense = RatingLike::where('review_id', $request->like)->first();
        if (count($checkExistense) < 1) {
            $ratinglike = new RatingLike();
            $ratinglike->likes = '1';
            $ratinglike->review_id = $request->like;
            $ratinglike->save();
        } else {
            $reviewId = RatingLike::where('review_id', $request->like)->first();
            $reviewId->likes += 1;
            $reviewId->review_id = $request->like;
            $reviewId->save();
        }

        echo json_encode('success');
    }

    public function favphotographer($userId, $photographerId)
    {
	$ratinglike = new FavoritePhotographer();
	$ratinglike->user_id = $userId;
	$ratinglike->photographer_id = $photographerId;
	$ratinglike->save();
        echo json_encode('Marked as favorites successfully');
    }
    
	public function myfavphotographer ($userId){
		 $ratinglike = new FavoritePhotographer();
		$getAllFavPhotographer = RatingLike::where('user_id', $userId)->all();

}

	/**
     *
     * Dislike a review function
     *
     */
    public function dislikeReview(Request $request)
    {
        $checkExistence = RatingLike::where('review_id', $request->id)->first();
        if (count($checkExistence) < 1) {
            $reviewId = new RatingLike;
            $reviewId->dislikes += 1;
            $reviewId->review_id = $request->dislike;
            $reviewId->save();
        } else {
            $reviewId = RatingLike::where('review_id', $request->id)->first();
            $reviewId->dislikes += 1;
            $reviewId->save();
        }

        echo json_encode('success');
    }

    /**
     *
     * Function to display photographer dashboard
     *
     */
    public function dashboard()
    {
        $countries = \App\Country::all();
        $getProfile = Photographer::where('user_id', Auth::user()->id)
            ->with('user')
            ->with('reviews')
            ->with('messages')
            ->with('photos')
            ->with('reviews.ratinglikes')
            ->get();
        return view('photographers.dashboard', compact('countries', 'getProfile'))->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    /**
     *
     * Function to save updated data posted by photgrapher dashboard
     *
     */

    public function doDashboard(Request $request)
    {
        if (Input::hasFile('photo')) {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $photo = Input::file('photo');
            $destinationPath = public_path() . '/uploads/photographers/';
            $filename = str_random(12);
            $extension = $photo->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                $upload_success = $photo
                    ->move($destinationPath, $filename . '.' . $extension);
            }

            $data = [
                'name' => $request->name,
                'company_name' => $request->company_name,
                'phone_number' => $request->phone,
                'website' => $request->website,
                'city' => $request->city,
                'established' => $request->established,
                'studio' => $request->studio,
                'price' => $request->price,
                'under500' => $request->under500,
                'under1000' => $request->under1000,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'pinterest' => $request->pinterest,
                'gplus' => $request->gplus,
                'instagram' => $request->instagram,
                'brand' => $request->brand,
                'description' => $request->description,
                'photo' => '/uploads/photographers/'.$filename . '.' .$extension
            ];
        }
        else
        {
            $data = [
                'name' => $request->name,
                'company_name' => $request->company_name,
                'phone_number' => $request->phone,
                'website' => $request->website,
                'city' => $request->city,
                'established' => $request->established,
                'studio' => $request->studio,
                'price' => $request->price,
                'under500' => $request->under500,
                'under1000' => $request->under1000,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'pinterest' => $request->pinterest,
                'gplus' => $request->gplus,
                'instagram' => $request->instagram,
                'brand' => $request->brand,
                'description' => $request->description,
            ];
        }

        $updatePhotographer = Photographer::where('user_id',$request->userid)->first();
        $updatePhotographer->unguard();
        $updatePhotographer->fill($data);
        $updatePhotographer->reguard();
        $updatePhotographer->save();

        return redirect()->to('/photographer/dashboard');

    }

    /**
     *
     * Function to show photographer upload page
     *
     */

    public function showUpload()
    {
        $photos = Photo::where('photographer_id', Auth::user()->id)->take(16)->get();
        return view('photographers.upload', compact('photos'))->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    /**
     *
     * Function to process the upload
     *
     */

    public function doUpload(Request $request)
    {
        $files = Input::file('images');
        $file_count = count($files);
        $upload_count = 0;
        foreach($files as $file)
        {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                Storage::put('uploads/' . $file->getClientOriginalName(), File::get($file));
            }
            $upload_count++;
            $imageSuccess = new Photo();
            $imageSuccess->photo_url = $filename;
            $imageSuccess->photographer_id = Auth::user()->id;
            $imageSuccess->save();
        }
        if($upload_count == $file_count){
            Session::flash('success', 'Photo uploaded successfully');
            return redirect()->to('/photographer/dashboard#photos');
        }

    }

    /**
     * Function to show last page
     */

    public function showLastPage()
    {
        /*$name = Photographer::where('user_id', '=', Auth::user()->id)->first();
        $name = $name->company_name;*/
        return view('photographers.lastpage')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    public function processLastPage(Request $request)
    {
        $updateProfile = Photographer::where('user_id', Auth::user()->id)->first();
        $updateProfile->description = $request->description;
        $updateProfile->interests = $request->interests;
        $updateProfile->save();

        if(count($request->photographystyle) > 0)
        {
            foreach($request->photographystyle as $style)
            {
                $photographerstyle = new PhotographerStyle();
                $photographerstyle->photographer_id = Auth::user()->id;
                $photographerstyle->style_id = $style;
                $photographerstyle->save();
            }
        }

        if(count($request->photographyservices) > 0)
        {
            foreach($request->photographyservices as $service)
            {
                $photographerservices = new PhotographerService();
                $photographerservices->photographer_id = Auth::user()->id;
                $photographerservices->service_id = $service;
                $photographerservices->save();
            }
        }

        return redirect()->to('unconfirmed');
    }

}
