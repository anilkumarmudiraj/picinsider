<?php namespace App\Http\Controllers\Admin;
use \App\Http\Requests;
use \App\Http\Controllers\Controller;
use App\Models\Generalfunction;
use \App\User;
use \App\Models\Role;
use \App\Country;
use \App\Models\Profile;
use \App\Models\Faq;
use App\Photographer;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Slider;
use Illuminate\Filesystem;
use Illuminate\Support\Facades\File;
use DB;

class AdminController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */

    public function login()
    {
        return view('admin.login');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $totalUsers = User::count();
        $totalPhotographers = Role::where('role','photographer')->count();
        $generalUsers = Role::where('role','user')->count();
        return view('admin.dashboard', compact('totalUsers', 'totalPhotographers','generalUsers'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function users()
    {
        $users = User::with('role')->paginate(10);
        return view('admin.users', compact('users'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editUser($id)
    {
        $info = User::where('id', $id)->with('profile')->first();
        $countries = Country::all();
        return view('admin.edit-user', compact('info', 'countries'));
    }

    /**
     * @param Requests\UserEditRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processEdit(Requests\UserEditRequest $request)
    {
        $id = $request->id;

        $editusersTable = User::find($id);
        $editusersTable->name = $request->name;
        $editusersTable->email = $request->email;
        $editusersTable->save();

        $findUser = Profile::where('user_id', '=', $id)->count();
        if($findUser > 0) {
            $findUser = Profile::where('user_id', '=', $id)->first();
            $findUser->phone = $request->phone;
            $findUser->address = $request->address;
            $findUser->city = $request->city;
            $findUser->state = $request->state;
            $findUser->zip = $request->zip;
            $findUser->country = $request->country;
            $findUser->user_id = $request->id;
            $findUser->save();
        }
        else
        {
            $findUser = new Profile();
            $findUser->phone = $request->phone;
            $findUser->address = $request->address;
            $findUser->city = $request->city;
            $findUser->state = $request->state;
            $findUser->zip = $request->zip;
            $findUser->country = $request->country;
            $findUser->user_id = $request->id;
            $findUser->save();
        }
        return redirect()->to('administrator/all-users')->with('message', 'User updated successfully');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function photographers()
    {
        $users = User::whereHas('role', function($q){
                $q->where('role', '=','photographer');
            })
            ->with('photographer')
            ->paginate(10);
        //return $users;
        return view('admin.users', compact('users'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function generalUsers()
    {
        $users = User::with('role')
            ->whereHas('role', function($q)
            {
                $q->where('role', '=', 'user');

            })->paginate(10);

        return view('admin.users', compact('users'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function editPhotographer($id)
    {
        $info = User::where('id', $id)->with('photographer')->first();
        $countries = Country::all();
        //return $info;
        return view('admin.edit-photographers', compact('info', 'countries'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processPhotographerEdit(Request $request)
    {
        $id = $request->userid;
        $findPhotographer = Photographer::where('user_id', '=', $id)->count();
        if($findPhotographer > 0)
        {
            $filename = "";
            $destinationPath = '';
            if (Input::hasFile('photo')) {
                $allowedext = ["png", "jpg", "jpeg", "gif"];
                $photo = Input::file('photo');
                $destinationPath = public_path() . '/uploads/photographers/';
                $filename = str_random(12);
                $extension = $photo->getClientOriginalExtension();
                if (in_array(strtolower($extension), $allowedext)) {
                    $upload_success = $photo
                        ->move($destinationPath, $filename . '.' . $extension);
                }
                $findPhotographer = Photographer::where('user_id', '=', $id)->first();
                $findPhotographer->name = $request->name;
                $findPhotographer->company_name = $request->company_name;
                $findPhotographer->phone_number = $request->phone;
                $findPhotographer->website = $request->website;
                $findPhotographer->city = $request->city;
                $findPhotographer->established = $request->established;
                $findPhotographer->studio = $request->studio;
                $findPhotographer->price = $request->price;
                $findPhotographer->under500 = $request->under500;
                $findPhotographer->under1000 = $request->under1000;
                $findPhotographer->facebook = $request->facebook;
                $findPhotographer->twitter = $request->twitter;
                $findPhotographer->pinterest = $request->pinterest;
                $findPhotographer->gplus = $request->gplus;
                $findPhotographer->instagram = $request->instagram;
                $findPhotographer->certified = $request->certified;
                $findPhotographer->brand = $request->brand;
                $findPhotographer->description = $request->description;
                $findPhotographer->photo = $filename.'.'.$extension;
                $findPhotographer->logo = $request->logo;
                $findPhotographer->featured = $request->featured;
                $findPhotographer->subscribe = $request->subscribe;
                $findPhotographer->notification = $request->notification;
                $findPhotographer->save();
            }
            else
            {
                $findPhotographer = Photographer::where('user_id', '=', $id)->first();
                $findPhotographer->name = $request->name;
                $findPhotographer->company_name = $request->company_name;
                $findPhotographer->phone_number = $request->phone;
                $findPhotographer->website = $request->website;
                $findPhotographer->city = $request->city;
                $findPhotographer->established = $request->established;
                $findPhotographer->studio = $request->studio;
                $findPhotographer->price = $request->price;
                $findPhotographer->under500 = $request->under500;
                $findPhotographer->under1000 = $request->under1000;
                $findPhotographer->facebook = $request->facebook;
                $findPhotographer->twitter = $request->twitter;
                $findPhotographer->pinterest = $request->pinterest;
                $findPhotographer->gplus = $request->gplus;
                $findPhotographer->instagram = $request->instagram;
                $findPhotographer->certified = $request->certified;
                $findPhotographer->brand = $request->brand;
                $findPhotographer->description = $request->description;
                $findPhotographer->logo = $request->logo;
                $findPhotographer->featured = $request->featured;
                $findPhotographer->subscribe = $request->subscribe;
                $findPhotographer->notification = $request->notification;
                $findPhotographer->save();
            }
        }

        else
        {
            $findPhotographer = new Photographer;
            $findPhotographer->name = $request->name;
            $findPhotographer->company_name = $request->company_name;
            $findPhotographer->phone_number = $request->phone;
            $findPhotographer->website = $request->website;
            $findPhotographer->city = $request->city;
            $findPhotographer->established = $request->established;
            $findPhotographer->studio = $request->studio;
            $findPhotographer->price = $request->price;
            $findPhotographer->under500 = $request->under500;
            $findPhotographer->under1000 = $request->under1000;
            $findPhotographer->facebook = $request->facebook;
            $findPhotographer->twitter = $request->twitter;
            $findPhotographer->pinterest = $request->pinterest;
            $findPhotographer->gplus = $request->gplus;
            $findPhotographer->instagram = $request->instagram;
            $findPhotographer->certified = $request->certified;
            $findPhotographer->brand = $request->brand;
            $findPhotographer->description = $request->description;
            $findPhotographer->photo = $request->photo;
            $findPhotographer->logo = $request->logo;
            $findPhotographer->featured = $request->featured;
            $findPhotographer->subscribe = $request->subscribe;
            $findPhotographer->notification = $request->notification;
            $findPhotographer->save();
        }

        $info = User::where('id', $id)->with('photographer')->first();
        $countries = Country::all();
        //return view('admin.edit-photographers', compact('info', 'countries'));
        return redirect()->to('/administrator/photographer/edit/'. $id);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showAddUser()
    {
        $countries = \App\Country::all();
        return view('admin.adduser', compact('countries'));
    }

    /**
     * @param Requests\AdminCreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processAddUser(Requests\AdminCreateUserRequest $request)
    {
        $createUser = new User;
        $createUser->name = $request->name;
        $createUser->email = $request->email;
        $createUser->password = bcrypt($request->password);
        $createUser->confirmed = '0';
        $createUser->save();

        $key = bcrypt(\Carbon\Carbon::now());

        $createUser->key()->create([
            'key' => $key,
            'type' => 'registration'
        ]);

        $createUser->role()->create([
            'role' => 'user'
        ]);

        $createUser->profile()->create([
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'country' => $request->country
        ]);
        //send mail to this user
        Generalfunction::customMail($key, $createUser->id, $request->email, $request->name, 'Welcome to Pic Insider');

        return redirect()->intended('administrator/general-users');

    }


 public function showFaq()
    {
        $countries = \App\Country::all();
        return view('admin.faq', compact('countries'));
    }

    /**
     * @param Requests\AdminCreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processFaq(Requests\AdminFaqRequest $request)
    {
       // print_r($_POST);
        $faq = new Faq;
        $faq->question = $_POST['question'];
        $faq->answer = $_POST['answer'];
        $faq->save();

        return redirect()->to('/administrator/faq');

    }

    /**
     * @return \Illuminate\View\View
     */

    public function showAddPhotographer()
    {
        $countries = \App\Country::all();
        return view('admin.addphotographer', compact('countries'));
    }

    /**
     * @param Requests\AdminCreatePhotographerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function processAddPhotographer(Requests\AdminCreatePhotographerRequest $request)
    {
        $createUser = new User;
        $createUser->name = $request->name;
        $createUser->email = $request->email;
        $createUser->password = bcrypt($request->password);
        $createUser->confirmed = '1';
        $createUser->save();

        $key = bcrypt(\Carbon\Carbon::now());

        $createUser->key()->create([
            'key' => $key,
            'type' => 'registration'
        ]);

        $createUser->role()->create([
            'role' => 'photographer'
        ]);

        $filename = "";
        $destinationPath = '';
        $extension = "";
        if (Input::hasFile('photo')) {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $photo = Input::file('photo');
            $destinationPath = public_path() . '/uploads/photographers/';
            $filename = str_random(12);
            $extension = $photo->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                $upload_success = $photo
                    ->move($destinationPath, $filename . '.' . $extension);
            }
        }

        $createUser->photographer()->create([
            'name' => $request->name,
            'company_name' => $request->company_name,
            'phone' => $request->phone,
            'website' => $request->website,
            'city' => $request->city,
            'established' => $request->established,
            'studio' => $request->studio,
            'price' => $request->price,
            'under500' => $request->under500,
            'under1000' => $request->under1000,
            'facebook' => $request->facebook,
            'gplus' => $request->gplus,
            'twitter' => $request->twitter,
            'pinterest' => $request->pinterest,
            'instagram' => $request->instagarm,
            'certified' => '0',
            'brand' => $request->brand,
            'description' => $request->description,
            'photo' => $filename. '.' . $extension,
            'logo' => '',
            'featured' => $request->featured,
            'subscribe' => $request->subscribe,
            'notification' => $request->notification,
        ]);
        //send mail to this user
        Generalfunction::customMail($key, $createUser->id, $request->email, $request->name, 'Welcome to Pic Insider');

        return redirect()->intended('administrator/photographers');

    }

    /**
     * @return \Illuminate\View\View
     */
    public function showLogin()
    {
        return view('admin.login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogin(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            //check role of the user
            $role = \App\Models\Role::where('user_id', Auth::user()->id)->first();
            if($role->role == 'admin')
            {
                return redirect()->to('administrator');
            }
            else
            {
                Auth::logout();
                return redirect()->to('/');
            }
        }
        else
        {
            return redirect()->to('/administrator/login')->with('message', 'Invalid credentials provided');
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function showSliders()
    {
        $sliders = Slider::all();
        return view('admin/sliders', compact('sliders'));
    }

    /**
     * @param Request $request
     */
    public function deactivateSlider(Request $request)
    {
        $updateSlider = Slider::find($request->slider_id);
        $updateSlider->active = 'No';
        $updateSlider->save();

        echo json_encode('success');
    }

    /**
     * @param Request $request
     */
    public function activateSlider(Request $request)
    {
        $updateSlider = Slider::find($request->slider_id);
        $updateSlider->active = 'Yes';
        $updateSlider->save();

        echo json_encode('success');
    }

    /**
     * @param Request $request
     */
    public function deleteSlider(Request $request)
    {
        $getFileName = Slider::where('id', $request->slider_id)->first();
        $fileName = $getFileName->location;

        if(File::delete(public_path().$fileName)) {
            $deletFile = Slider::find($request->slider_id);
            $deletFile->delete();

            echo json_encode('success');
        }
        else
        {
            echo json_encode('failure');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sliderUpload(Request $request)
    {
        $filename = "";
        $destinationPath = '';
        if (Input::hasFile('slider')) {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $photo = Input::file('slider');
            $destinationPath = public_path() . '/images/slider/';
            $filename = str_random(12);
            $extension = $photo->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                $upload_success = $photo
                    ->move($destinationPath, $filename . '.' . $extension);

                $insertToSlider = new Slider();
                $insertToSlider->location = '/images/slider/'.$filename .'.'.$extension;
                $insertToSlider->active = 'No';
                $insertToSlider->save();
                return redirect()->to('/administrator/main-sliders')->with('message', 'Slider uploaded successfully');
            }
            else
            {
                return redirect()->to('/administrator/main-sliders')->with('message', 'Something went wrong while uploading the image.');
            }
        }
        else
        {
            return redirect()->to('/administrator/main-sliders')->with('message', 'You have not chosen any file');
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function aboutUs()
    {
        $aboutus = \App\Models\About::first();
        return view('admin.aboutus', compact('aboutus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processaboutUs(Request $request)
    {
        $aboutus = \App\Models\About::find(1);
        $aboutus->onslider = $request->onslide;
        $aboutus->heading1 = $request->heading1;
        $aboutus->para1 = $request->para1;
        $aboutus->person1_name = $request->person1_name;
        $aboutus->person1_position = $request->person1_position;
        $aboutus->person2_name = $request->person2_name;
        $aboutus->person2_position = $request->person2_position;
        $aboutus->person3_name = $request->person3_name;
        $aboutus->person3_position = $request->person3_position;
        $aboutus->heading2 = $request->heading2;
        $aboutus->para2 = $request->para2;
        $aboutus->facebook = $request->facebook;
        $aboutus->twitter = $request->twitter;
        $aboutus->pinterest = $request->pinterest;
        $aboutus->save();

        return redirect()->to('/administrator/about-us');

    }


    /**
     * @param Request $request
     */
    public function deleteUser(Request $request)
    {
        $user_id = $request->deleting;
        $deleteUser = User::find($user_id);
        $deleteUser->delete();

        echo json_encode('success');
    }

    /**
     * @param Request $request
     */
    public function deleteReview(Request $request)
    {
        $review_id = $request->deleting;
        $deleteReview = \App\Review::find($review_id);
        $deleteReview->delete();

        echo json_encode('success');
    }

    /**
     * @param Request $request
     */
    public function deleteFlag(Request $request)
    {
        $flag_id = $request->deleting;
        $deleteFlag = \App\Models\Flag::find($flag_id);
        $deleteFlag->delete();

        echo json_encode('success');
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function searchPhotographers(Request $request)
    {
        $query = $request->searchstring;

       // $users = User::whereRaw("name like '%".$query. "%'")->with(['role' => function($q){
	$users = User::whereRaw("name like '%".$query. "%' OR email like '%".$query. "%' ")->with(['role' => function($q){
            $q->where('role', '=', 'photographer');
        }])
            ->with('photographer')
            ->paginate(15);
        return view('admin.photographerSearch', compact('users'));

    }
}
