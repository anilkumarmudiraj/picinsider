<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Flag;

use Illuminate\Http\Request;

class AdminSecondController extends Controller {

	public function showFlagged()
    {
        $flagged = Flag::with('review')->paginate(20);
        return view('admin.flagged', compact('flagged'));
    }

    public function changeStatus(Request $request)
    {
        $status = $request->changeto;
        $id = $request->photographer_id;

        $change = \App\Photographer::where('user_id',$id)->first();
        $change->featured = $status;
        if($change->save()){
            echo json_encode('success');
        }else{
            echo json_encode('not success');
        }


    }
}
