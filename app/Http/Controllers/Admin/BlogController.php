<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class BlogController extends Controller {

    public function showEdit($id)
    {
        $blog = Blog::where('id', $id)->first();
        return view('admin.blog', compact('blog'));
    }

    public function doEdit(Request $request, $id)
    {
        if (Input::hasFile('image')) {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $photo = Input::file('image');
            $destinationPath = public_path() . '/images/blogs/';
            $filename = str_random(12);
            $extension = $photo->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                $upload_success = $photo
                    ->move($destinationPath, $filename . '.' . $extension);
            }
            $data = [
                'title' => $request->title,
                'author' => $request->author,
                'body' => $request->body,
                'photo' => '/images/blogs/' . $filename . '.'. $extension
            ];
        }
        else
        {
            $data = [
                'title' => $request->title,
                'author' => $request->author,
                'body' => $request->body
            ];
        }

        $updateBlog = Blog::find($id);
        $updateBlog->unguard();
        $updateBlog->fill($data);
        $updateBlog->reguard();
        $updateBlog->save();

        return redirect()->to('/administrator/edit/blog/'.$id);

    }

    public function blogs()
    {
        $blogs = Blog::paginate(15);
        return view('admin.blogs', compact('blogs'));
    }

    public function showWriteBlog()
    {
        return view('admin.write-blog');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function processWriteBlog(Requests\NewblogRequest $request)
    {
        $hasFile = $request->file();
        if(isset($hasFile))
        {
            $allowedext = ["png", "jpg", "jpeg", "gif"];
            $photo = Input::file('image');
            $destinationPath = public_path() . '/images/blogs/';
            $filename = str_random(12);
            $extension = $photo->getClientOriginalExtension();
            if (in_array(strtolower($extension), $allowedext)) {
                $upload_success = $photo
                    ->move($destinationPath, $filename . '.' . $extension);
            }
            $data = [
                'title' => $request->title,
                'author' => $request->author,
                'body' => $request->body,
                'photo' => '/images/blogs/' . $filename . '.'. $extension,
                'user_id' => $request->user_id
            ];
        }

        $updateBlog = new Blog();
        $updateBlog->unguard();
        $updateBlog->fill($data);
        $updateBlog->reguard();
        $updateBlog->save();

        return redirect()->to('administrator/blogs');
    }
}
