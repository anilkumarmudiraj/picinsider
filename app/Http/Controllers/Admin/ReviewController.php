<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller {

    /**
     * index function to display reviews about photographers
     */
    public function index($userid)
    {
        $reviews = Review::where('photographer_id', $userid)->with('user')->paginate(10);

        return view('admin.review', compact('reviews'));
    }

    /**
     * @param $reviewid
     * @return \Illuminate\View\View
     */
    public function showReview($reviewid)
    {
        $review = Review::where('id', $reviewid)->first();
        return view('admin.edit-review');
    }

    /**
     * @param Request $request
     */

    public function processReview(Request $request)
    {
        $review_id = $request->review_id;
        $body = $request->body;
        $subject = $request->review_subject;

        $updateReview = Review::find($review_id);
        $updateReview->subject = $subject;
        $updateReview->review = $body;
        $updateReview->save();

        echo json_encode(['status' => 'success', 'review' => $body, 'subject' => $subject]);
    }

}
