<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Generalfunction;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class PasswordController extends CustomController
{
    public function showForgotPassword()
    {
        return view('auth.reset')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }


    public function doForgotPassword(Request $request)
    {
        $email = $request->email;
        $checkExistence = User::where('email', $email)->first();
        if(count($checkExistence) > 0)
        {
            $key = md5(\Carbon\Carbon::now());

            $createKey = new \App\Models\Key;
            $createKey->key = $key;
            $createKey->type = 'reset';
            $createKey->user_id = $checkExistence->id;
            $createKey->save();

            Generalfunction::resetMail($key, $checkExistence->id, $email, $checkExistence->name, 'Instructions to reset your password');

            return redirect()->to('forgot-password')->with('message', 'We have sent you an email with instructions on how to reset your password');
        }
        else
        {
            return redirect()->to('forgot-password')->with('message','We do not have any user with that email in our database. Please try again');
        }
    }

    public function showReset(Request $request)
    {
        $id = $request->id;
        $key = $request->key;

        /**
         * Check if the user id and key matches
         */

        $checkKey = \App\Models\Key::where('user_id', $id)
            ->where('key', $key)
            ->get();
        if (count($checkKey) > 0) {
            $email = User::where('id', $id)->pluck('email');
            return view('reset', compact('email'))->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);;
        }
    }

    public function doReset(Requests\ResetRequest $request)
    {
        $getUser = User::where('email', $request->email)->get();
        if (count($getUser) > 0) {
            $updateRecord = User::where('email', $request->email)->first();
            $updateRecord->password = bcrypt($request->password);
            $updateRecord->save();

            return redirect()->intended('/login')->with('message', 'Thank you. Password reset has been successful');
        } else {
            return redirect()->intended('/login')->with('message', 'Something went wrong with processing your request');
        }
    }

    public function modalUpdate(Request $request)
    {
        $current_password = $request->current_password;
        //$current_password = bcrypt($current_password);
        $updateRequest = User::where('id', Auth::user()->id)->first();

        if(Hash::check($current_password, $updateRequest->password))
        {
            $updateRequest->password = bcrypt($request->new_password);
            $updateRequest->save();
            echo json_encode('1');
            // Congrats! your password updated successfully. Logging you out now
        }
        else
        {
            echo json_encode('0');
            // The password you have supplied does not match with the one in our records
        }
    }

}
