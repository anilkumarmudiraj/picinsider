<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends CustomController
{

    public function faq()
    {
        $faq = Faq::all();

        return view('faq', compact('faq'))->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }

}
