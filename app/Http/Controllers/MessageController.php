<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Message;
use Auth;

use Illuminate\Http\Request;

class MessageController extends Controller
{

	public function sendMessage(Request $request)
    {
        $sendMessage = new Message();
        $sendMessage->to_id = $request->to_id;
        if(Auth::check()) {
            $sendMessage->from_id = Auth::user()->id;
        }
        else
        {
            $sendMessage->from_id = 0;
        }
        $sendMessage->to_name = $request->to_name;
        $sendMessage->from_name = $request->name;
        $sendMessage->connect = $request->connect;
        $sendMessage->email = $request->email;
        $sendMessage->message = $request->comment;
        $sendMessage->save();

        echo json_encode('success');
    }

}
