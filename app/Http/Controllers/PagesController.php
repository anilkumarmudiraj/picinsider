<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\About;
use App\Models\Flag;
use Illuminate\Http\Request;

class PagesController extends CustomController
{

    public function aboutus()
    {
        $aboutus = About::first();

        return view('aboutus', compact('aboutus'))->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }

    public function markFlag(Request $request)
    {
        $markIt = new Flag();
        $markIt->review_id = $request->review_id;
        $markIt->save();

        echo json_encode('Flag has been marked successfully');
    }
}
