<?php namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Generalfunction;
use Auth;
use Greggilbert\Recaptcha\Facades\Recaptcha;
use Mail;
use Illuminate\Http\Request;
use \App\User;
use Illuminate\Contracts\Mail\Mailer;
use \App\Models\Role;

class LoginController extends CustomController
{

    public function showLogin()
    {
        return view('auth.login')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    public function dologin(Requests\LoginRequest $request)
    {
        if($request->remember == 'on') {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password], true)) {
                $role = Auth::user()->role->role;
                if ($role == 'admin') {
                    return redirect()->intended('administrator');
                } else {
                    return redirect()->intended('home');
                }
            } else {
                return redirect()->back()->withInput();
            }
        }

        else
        {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $role = Auth::user()->role->role;
                if ($role == 'admin') {
                    return redirect()->intended('administrator');
                } elseif ($role == 'photographer')
                {
                    return redirect()->intended('/photographer/dashboard');
                }
                else
                {
                    return redirect()->intended('/myprofile');
                }
            } else {
                return redirect()->back()->withInput();
            }
        }
    }

    public function showRegister()
    {
        return view('auth.register')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    public function doRegister(Requests\RegisterRequest $request)
    {
        //check if the user already exists
        $checkUser = User::where('email', '=', $request->email)->count();
        if ($checkUser < 1) {
            $register = new User();
            $register->name = $request->name;
            $register->email = $request->email;
            $register->password = bcrypt($request->password);
            $register->save();

            if($register->id == '1') {
                $register->role()
                    ->create([
                        'role' => 'admin'
                    ]);
            }
            else
            {
                $register->role()
                    ->create([
                        'role' => 'user'
                    ]);
            }

            $randomkey = bcrypt(\Carbon\Carbon::now());

            $register->key()->create([
                'key' => $randomkey
            ]);

            $email = $request->email;
            $username = $request->name;
            $subject = 'Account activation email';

            /**
             *
             * Send the key, just registered id, email, name of user
             * and subject to mail function of Generalfunction model
             *
             */

            $sendMail = GeneralFunction::customMail($randomkey, $register->id, $email, $username, $subject);

            Auth::loginUsingId($register->id);
            return redirect()->intended('/unconfirmed');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->intended('/');
    }

    public function facebook()
    {
        return \Socialize::with('facebook')->redirect();
    }

    public function callback()
    {
        $user = \Socialize::with('facebook')->user();

        //check if we already have a user

        $checkUser = User::where('email', '=', $user->email)->first();
        $count = count($checkUser);
        if ($count < 1) {
            $register = new User();
            $register->name = $user->name;
            $register->email = $user->email;
            $register->password = md5(\Carbon\Carbon::now());
            $register->save();

            $register->role()->create([
                'role' => 'user'
            ]);

            Auth::loginUsingId($register->id);
            return redirect()->intended('home');
        } else {
            Auth::loginUsingId($checkUser->id);
            return redirect()->intended('home');
        }
        // $user->token;
    }

    public function confirm(Request $request)
    {
        /**
         * get the id and key from the url
         */
        $key = $request->key;
        $id = $request->id;

        /**
         *
         * Send these details to confirmation function
         * of Key class and get teh response
         *
         */

        $result = \App\Models\Key::confirmation($id, $key);

        if ($result == 'confirmed') {
            return view('confirmed')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
        } else {
            return redirect()->intended('/')->with(['message' => 'Something went wrong']);
        }
    }

    public function resend($id)
    {
        /**
         *
         * check if the user account is confirmed or not
         *
         */

        $checkStatus = \App\User::where('id', '=', $id)->with('key')->first();

        $key = $checkStatus->key['key'];
        $email = $checkStatus->email;
        $username = $checkStatus->username;
        $subject = 'Confirmation Email';
        if ($checkStatus->confirmed == 0) {
            $sendMail = \App\Models\Generalfunction::customMail($key, $id, $email, $username, $subject);

            return view('resend')->with([
                'cities' => $this->cities(),
                'autocompletePhotographers' => $this->photographers(),
                'message' => 'Email sent successfully'
            ]);
        } else {
            return view('resend')->with([
                'cities' => $this->cities(),
                'autocompletePhotographers' => $this->photographers(),
                'message' => 'Your account is already active.'
            ]);
        }

    }

    public function unconfirmed()
    {
        return view('unconfirmed')->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    public function showPhotographerRegister()
    {
        $countries = Country::all();
        return view('photographers.register', compact('countries'))->with(['cities' => $this->cities(), 'autocompletePhotographers' => $this->photographers()]);
    }

    public function doPhotographerRegister(Requests\PhotographerRegisterRequest $request)
    {
        $name = $request->first_name .' ' . $request->last_name;
        $register = new User;
        $register->name = $name;
        $register->email = $request->email;
        $register->password = bcrypt($request->password);
        $register->confirmed = '0';
        $register->save();


        $register->photographer()->create([
            'name' => $name,
            'company_name' => $request->company_name
        ]);

        $register->role()->create([
            'role' => 'photographer'
        ]);

        $randomkey = bcrypt(\Carbon\Carbon::now());

        $register->key()->create([
            'key' => $randomkey
        ]);

        $email = $request->email;
        $username = $request->first_name . ' ' . $request->last_name;
        $subject = 'Account activation email';

        /**
         *
         * Send the key, just registered id, email, name of user
         * and subject to mail function of Generalfunction model
         *
         */

        $sendMail = GeneralFunction::customMail($randomkey, $register->id, $email, $username, $subject);

        Auth::loginUsingId($register->id);
        return redirect()->intended('/secondpage');
    }

    public function showPhotographerLogin()
    {
        return view('auth.photographer.login');
    }

    public function showPhotographerReset()
    {
        return view('auth.photographer.request');
    }

    public function doPhotographerReset(Requests\PhotographerPasswordRequest $request)
    {
        /**
         * Check if we have a user for the supplied email address
         * if yes then generate a key for this user and save
         * it in keys tabel and then send email to user
         */

        $checkUser = User::where('email', $request->email)->first();
        if (count($checkUser) > 0) {
            $key = md5(\Carbon\Carbon::now());

            $insertKey = new \App\Models\Key;
            $insertKey->key = $key;
            $insertKey->type = 'reset';
            $insertKey->user_id = $checkUser->id;
            $insertKey->save();

            $sendMail = Generalfunction::resetMail($key, $checkUser->id, $request->email, $checkUser->name, 'Password Reset');

            if ($sendMail == 'success') {
                return redirect()->intended('/photographer/reset')->with('message', 'We have sent you an email to recover your password. Please follow the instructions provided.');
            } else {
                return redirect()->intended('/photographer/reset')->with('message', 'We are not able to process your request to recover the password. If the problem persists please contact the site Admin on Admin@domain.com');
            }
        } else {
            return redirect()->intended('/photographer/reset')->with('message', 'We do not have an account with supplied email address in our database');
        }
    }

    public function modalLogin(Requests\LoginRequest $request)
    {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                echo json_encode('success');
            } else {
                echo json_encode('failure');
            }

    }
}
