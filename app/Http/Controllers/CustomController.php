<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \App\City;
use \App\Photographer;
use Illuminate\Http\Request;

class CustomController extends Controller
{

    public function cities()
    {
        /**
         *
         *  Get the list of cities from database for autocomplete
         */

        $response = json_decode(City::get(['id', 'name']));

        return $response;
    }

    public function photographers()
    {
        /**
         *
         * Get the list of photographers from database for auto-complete
         */

        $response = json_decode(Photographer::get(['id', 'name']));

        return $response;
    }

}