<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Models\Gig;
use App\Models\GigCategory;
use App\Models\Invite;
use App\Models\Role;
use Auth;
use Carbon\Carbon;
use Illuminate\Mail\Mailer;
use App\Photographer;
use Illuminate\Http\Request;

class UserController extends CustomController
{
    const RANDOM = 100;

    public function __construct(Mailer $mail)
    {
        $this->mail = $mail;
    }

    public function postProject()
    {
        $categories = Category::all();
        return view('post-project', compact('categories'))->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }

    public function processProject(Requests\ProjectRequest $request)
    {
        $date = date('d/m/Y',strtotime($request->date));

        /*$date = date_format($date, 'Y-m-d');
        $date = $date->format('Y-m-d');*/
        $budget = $request->budget;
        if ($budget == '1000-2000') {
            $budget = 'Above $1000';
        }

        $saveGig = new \App\Models\Gig();
        $saveGig->short = $request->short;
        $saveGig->city = $request->city;
        $saveGig->date = $date;
        $saveGig->requirements = $request->requirements;
        $saveGig->description = $request->description;
        $saveGig->budget = $request->budget;
        $saveGig->user_id = Auth::user()->id;
        $saveGig->save();

        //insert data into gig_categories table
        foreach ($request->category as $category) {
            $gigCat = new \App\Models\GigCategory();
            $gigCat->category = $category;
            $gigCat->gig_id = $saveGig->id;
            $gigCat->save();
        }
        if (isset($request->invitation)) {
            $getPhotographerId = \App\Photographer::where('name', $request->invitation)->pluck('user_id');

            $sendInvitation = new \App\Models\Invite();
            $sendInvitation->invited_by = Auth::user()->id;
            $sendInvitation->invite_to = $getPhotographerId;
            $sendInvitation->gig_id = $saveGig->id;
            $sendInvitation->save();
        }

        $getAdmin = Role::where('role', '=', 'admin')
            ->with('user')
            ->get();

        foreach ($getAdmin as $admin) {
            $this->mail->send('emails.notifyAdmin', [
                'username' => $admin->user->name,
                'gig_name' => $request->short,
                'posted_by' => Auth::user()->name,
                'gig_id' => $saveGig->id
            ],
                function ($message) use ($admin) {

                    $message->to($admin->user->email, $admin->user->name)->subject('A new project has been posted');
                });
        }
        return redirect()->to('gig-posted');

    }

    public function gigposted()
    {

        /* @var Collection $Songs */
        $totalCount = Photographer::count('id');

        if ($totalCount > self::RANDOM) {
            $skip = mt_rand(1, (int)($totalCount - 1));

            $Photographers = Photographer::take(self::RANDOM)
                ->whereNotNull('photo')
                ->skip($skip)
                ->get();

            $Photographers->shuffle();

            $randomPhotographers = $Photographers->slice(rand(0, self::RANDOM), 12);

        } else {
            $Photographers = Photographer::whereNotNull('photo')->get();

            $Photographers->shuffle();

            $randomPhotographers = $Photographers->take(8);

        }
        return view('gigposted', compact('randomPhotographers'))->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }

    public function inviteToGig(Request $request)
    {
        $gig_id = $request->gig_id;
        $photographer_id = $request->photographer_id;

        /**
         * Get the email address of user and photographer
         * from users table and send an email to
         * photographer informing about invite
         */

        $photographer = \App\User::where('id', $photographer_id)->first();

        $this->mail->send('emails.invite', ['username' => Auth::user()->name, 'photographer_name' => $photographer->name, 'gig_id' => $gig_id], function ($message) use ($photographer) {
            $message->to($photographer->email, $photographer->name)->subject('Someone invited you for a gig on Picinsider');
        });
        $sendInvite = new Invite;
        $sendInvite->invited_by = Auth::user()->id;
        $sendInvite->invite_to = $photographer_id;
        $sendInvite->gig_id = $gig_id;
        $sendInvite->save();

        echo json_encode('success');
    }

    public function showGigEdit($id)
    {
        $ownerId = Gig::where('id', $id)->first();
        if (Auth::id() != $ownerId->user_id)
        {
            return redirect()->to('/');
        }
        $gig = Gig::where('id', $id)->with('gigcategories')->first();
        $categories = Category::all();
        return view('edit-gig', compact('categories', 'gig'))
            ->with([
                'cities' => $this->cities(),
                'autocompletePhotographers' => $this->photographers()
            ]);
    }

    public function processGigEdit(Request $request, $id)
    {
        if(count($request->category) <1)
        {
            return redirect()->back()->with(['message' => 'Please choose at least one category', 'class' => 'alert alert-danger alert-dismissible']);
        }
        $data = [
            'short' => $request->short,
            'city' => $request->city,
            'date' => $request->date,
            'requirements' => $request->requirements,
            'description' => $request->description,
            'budget' => $request->budget
        ];
        $updateGig = Gig::find($id);
        $updateGig->unguard();
        $updateGig->fill($data);
        $updateGig->reguard();
        $updateGig->save();

        //delete old records from gig category table
        for($i=0;$i<=1;$i++) {
            $gig = GigCategory::where('gig_id', $id)->first();
            $gig->delete();
        }
        foreach($request->category as $category)
        {
            $updateGig = new GigCategory();
            $updateGig->category = $category;
            $updateGig->gig_id = $id;
            $updateGig->save();
        }
        return redirect()->to('/gig/edit/'.$id)->with(['message' => 'Gig updated successfully', 'class' => 'alert alert-success alert-dismissible']);
    }

    public function deleteGig($id)
    {
        \App\Models\Gig::where('id', $id)->delete();

        return redirect()->to('/myprofile'); 
    }
}
