<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Photographer;

use Illuminate\Http\Request;

class SearchController extends CustomController
{
    protected function index(Request $request)
    {
        $occasion = $request->occasion;
        $city = $request->city;
        $city = explode(',', $city);
        $city = $city[0];

        if ($occasion == "") {
            $results = Photographer::where('city', '=', $city)
                ->with('mainreview')
                ->paginate(10);
        } else if ($city == "") {
            $results = Photographer::with('mainreview')
                ->with(['photographercategory' => function ($q) use ($occasion) {
                    $q->where('name', $occasion);
                }])
                ->paginate(10);
        } else {
            $results = Photographer::where('city', '=', $city)
                ->with('mainreview')
                ->with(['photographercategory' => function ($q) use ($occasion) {
                    $q->where('name', $occasion);
                }])
                ->paginate(10);
        }
        return view('search', compact('results', 'photographercategories', 'occasion', 'city'))->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }

    public function topSearch(Request $request)
    {
        $name = $request->photographer;
        $city = explode(',',$request->city);
        $city = $city[0];
        $occasion = $name;


        if ($name == "") {
            $results = Photographer::where('city', $city)
                ->with('mainreview')
                ->with('reviews')
                ->with('photographercategory')
                ->paginate(10);
        } else if ($city == "") {
            $results = Photographer::where('name', $name)
                ->with('mainreview')
                ->with('reviews')
                ->with('photographercategory')
                ->paginate(10);
        } else if ($city == "" && $name == "") {
            $results = Photographer::with('mainreview')
                ->with('reviews')
                ->with('photographercategory')
                ->paginate(10);
        } else {
            $results = Photographer::where('city', $city)
                ->where('name', $name)
                ->with('mainreview')
                ->with('reviews')
                ->with('photographercategory')
                ->paginate(10);
        }
        return view('search', compact('results', 'occasion', 'city'))->with([
            'cities' => $this->cities(),
            'autocompletePhotographers' => $this->photographers()
        ]);
    }
}
